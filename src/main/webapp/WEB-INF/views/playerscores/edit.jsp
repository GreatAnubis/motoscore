<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><s:message code="page.title"/></title>
    <script type="text/javascript">
    </script>
</head>
<body role="document">
    <!-- Docs master nav -->
    <div class="container bs-docs-container">
        <div class="row">
            <div class="col-md-12"><span>&nbsp;</span></div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a href="<s:url value="/admin/racingevents/${eventid}/edit"/>" type="button" class="btn btn-default pull-left">Back</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12"><span>&nbsp;</span></div>
        </div>
        <div class="row">
            <s:url value="/admin/playerscores/${eventid}/${trackid}/assign" var="action" />
            <form:form id="vehicleAssignForm" action='${action}' modelAttribute="vehicleAssigningDTO" method="post" role="form">
            <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
            <div class="col-md-12">
                <div class="row">
                    <input type="hidden" id="trackInEvent" name="trackInEvent" value="${trackInEvent.id}"/>
                    <div class="panel panel-primary drop-shadow-down" style="position: relative;">
                        <div id="thisYearScoresLoader" class="ajax_loader" style="display: none;"></div>
                        <button id="updatedata" type="submit" class="btn btn-warning pull-right" style="margin-top: 3px;margin-right: 10px;"><span class="glyphicon glyphicon-save"></span>&nbsp;<s:message code="button.update"/></button>
                        <div class="panel-heading">Vehicles</div>
                        <div style="height: 100px;margin-bottom: 0;  border-bottom-right-radius: 4px;  border-bottom-left-radius: 4px">
                            <ul class="col-md-12" style=" display: block;white-space: nowrap;overflow: auto; margin-top: 5px;">
                                <c:forEach items="${availableVehicles}" var="vehicle">
                                    <li style="width: 80px;display: inline-block;">
                                    <div style="width: 100%;text-align: center;vertical-align: middle;">
                                        <img src="<s:url value="/resources/images/vehicles/icons/${vehicle.vehicle_code}.png"/>" width="80px" height="50px" title="${vehicle.vehicle_name}" />
                                    </div>
                                    <div style="width: 100%;text-align: center;vertical-align: middle;">
                                        <input type="checkbox" name="trackInEvent.vehiclesInRun" value="${vehicle.id}" label="${vehicle.vehicle_name}" <c:if test="${trackInEvent.containsVehicleInSet(vehicle)}" >checked="checked"</c:if> />
                                    </div>
                                    <div style="width: 100%;text-align: center;vertical-align: middle;">
                                    ${vehicle.vehicle_name}
                                    </div>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            </form:form>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#playerScoreAddDialog"><span class="glyphicon glyphicon-plus"></span>&nbsp;<s:message code="button.add-score"/></button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12"><span>&nbsp;</span></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#trackEditDialog" style="margin-top: 3px;margin-right: 10px;"><span class="glyphicon glyphicon-edit"></span>&nbsp;<s:message code="button.edit-track"/></button>
                    <div class="panel-heading">#${trackInEvent.ordering}&nbsp;${trackInEvent.track.track_name}</div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                            <th class="col-md-2">Miejsce</th>
                            <th class="col-md-2">Punkty</th>
                            <th class="col-md-4">Nick</th>
                            <th class="col-md-2"><div style="text-align: center;"><s:message code="button.edit"/></div></th>
                            <th class="col-md-2"><div style="text-align: center;"><s:message code="button.delete"/></div></th>
                            </thead>
                            <tbody>
                            <c:set var="index" value="1" scope="page"/>
                            <c:forEach var="playerScoreInRun" items="${trackInEvent.playerScoresInRun}">
                                <tr>
                                    <td class="col-md-2">${index}</td>
                                    <td class="col-md-2">${playerScoreInRun.score}</td>
                                    <td class="col-md-4">${playerScoreInRun.player.player_nick}</td>
                                    <td class="col-md-2">
                                        <button onclick="prepareToEdit(${playerScoreInRun.id},'${playerScoreInRun.player.player_nick}','${playerScoreInRun.score}');return false;"  style="float: right;" class="btn btn-sm btn-success btn-block" data-toggle="modal" data-target="#updateScoreDialog"><span class="glyphicon glyphicon-edit"></span>&nbsp;<s:message code="button.edit"/></button>
                                    </td>
                                    <td class="col-md-2">
                                        <button onclick="prepareToDelete(${playerScoreInRun.id},'${playerScoreInRun.player.player_nick} Score:  ${playerScoreInRun.score}');return false;" class="btn btn-sm btn-danger btn-block"><span class="glyphicon glyphicon-remove"></span>&nbsp;<s:message code="button.delete"/></button>
                                    </td>
                                </tr>
                            <c:set var="index" value="${index+1}" scope="page"/>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="playerScoreAddDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="deleteRacingEventLabel">Add a score</h4>
                    </div>
                    <div class="modal-body">
                        <div id="addError" style="display: none;" class="alert alert-danger"></div>
                        <s:url value="/admin/playerscores/${eventid}/${trackid}/add" var="action" />
                        <form:form id="addPlayerScoreForm" action='${action}' modelAttribute="playerScoreInRun" method="post" role="form">
                            <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                            <div class="row">
                                <div class="col-lg-12"><form:hidden path="trackInEvent" value="${trackInEvent.id}"/></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="input-group input-group-lg">
                                        <label class="input-group-addon" for="player">Player</label>
                                        <form:select path="player" items="${availablePlayers}" itemLabel="player_nick" itemValue="id" multiple="false" size="15" cssClass="form-control" cssStyle="height:auto;width:100%;"/>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="control-group">
                                        <label class="control-label" for="score">Score</label>
                                        <input id="score" name="score" placeholder="Type a player score" type="text" class="form-control" value="0" maxlength="4">
                                    </div>
                                </div>
                            </div>
                        </form:form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                        <button id="addPlayerScoreConfirm" type="button" class="btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.add"/></button>
                    </div>
                    <script type="text/javascript">
                        $('#addPlayerScoreConfirm').click(function () {
                            var btn = $(this);
                            btn.button('loading');
                            $.post( "${action}",  $('#addPlayerScoreForm').serialize())
                                    .fail(function(xhr,textstatus,error){$("#addError").html(error);$("#addError").show();btn.button('reset');})
                                    .success(function(data) {
                                        if(data == "OK")
                                        {
                                            $("#addError").html("");
                                            $("#addError").hide();
                                            //$('racingEventDeleteDialog').modal('hide')
                                            location.reload();
                                        }
                                        else if(data == "NOTEXISTS")
                                        {
                                            $("#addError").html("<s:message code="NotExists.racing-event" javaScriptEscape="true"/>");
                                            $("#addError").show();
                                        }
                                        else
                                        {
                                            $("#addError").html(data);
                                            $("#addError").show();
                                        }
                                    })
                                    .always(function() {/* btn.button('reset');*/});
                        });
                    </script>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="playerScoreEditDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="editPlayerScoreLabel">Edit a player score</h4>
                    </div>
                    <s:url value="/admin/playerscores/edit" var="editaction"/>
                    <form:form id="editplayerscoreform" onsubmit='$("#editPlayerScoreConfirm" ).trigger( "click" );return false;' action='${editaction}' commandName="playerScoreInRun" method="post" role="form">
                        <div class="modal-body">
                            <div id="editError" style="display: none;" class="alert alert-danger"></div>
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon" id="player">Player</span>
                                    <form:input path="score" type="text" class="form-control" placeholder="Score" maxlength="4"/>
                                    <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                                    <input id="edit_playerscore_id" name="id" type="hidden" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                            <button id="editPlayerScoreConfirm" type="button" class="btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.save"/></button>
                            <script type="text/javascript">
                                $('#playerScoreEditDialog').on('show.bs.modal', function (e) {$("#editError").html("");$("#editError").hide();$('#editPlayerScoreConfirm').button('reset');});
                                function prepareToEdit(id,player, playerScore)
                                {
                                    $("#editplayerscoreform #score").val(playerScore);
                                    $("#editplayerscoreform #player").text(player);
                                    $("#edit_playerscore_id").val(id);
                                    $("#playerScoreEditDialog").modal("show");
                                }
                                $('#editPlayerScoreConfirm').click(function () {
                                    if($("#editplayerscoreform #score").val().length == 0)
                                    {
                                        $("#editError").html("<s:message code='NotEmpty.player_score' javaScriptEscape='true' />");
                                        $("#editError").show();
                                        return;
                                    }
                                    var btn = $(this);
                                    btn.button('loading');
                                    $.post( "<c:url value="/admin/playerscores/${eventid}/${trackid}/edit" />", $('#editplayerscoreform').serialize())
                                            .fail(function(xhr,textstatus,error){$("#editError").html(error);$("#editError").show(); btn.button('reset');})
                                            .success(function(data) {
                                                if(data == "OK")
                                                {
                                                    $("#editError").html("");
                                                    $("#editError").hide();
                                                    $('#editplayerscoreform #score').val("");
                                                    location.reload();
                                                }
                                                else
                                                {
                                                    $("#editError").html(data);
                                                    $("#editError").show();
                                                    btn.button('reset');
                                                }
                                            })
                                            .always(function() { /*btn.button('reset');*/});
                                });
                            </script>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="trackEditDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="editTrackLabel">Edit a track</h4>
                    </div>
                    <c:url value="/admin/playerscores/${eventid}/${trackid}/edittrack" var="edittrackaction"/>
                    <form:form id="edittrackform" onsubmit='$("#editTrackConfirm" ).trigger( "click" );return false;' action='${edittrackaction}' modelAttribute="trackInEvent" role="form">
                        <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                        <div class="modal-body">
                            <div id="editTrackError" style="display: none;" class="alert alert-danger"></div>
                            <div class="input-group input-group-lg">
                                <span class="input-group-addon">Available tracks</span>
                                <form:select path="track" items="${availableTracks}" itemLabel="track_name" itemValue="id" multiple="false" size="10" cssClass="form-control" cssStyle="height:auto;width:100%;"/>
                                <form:hidden path="id"/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                            <button id="editTrackConfirm" type="button" class="btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.save"/></button>
                            <script type="text/javascript">
                                $('#trackEditDialog').on('show.bs.modal', function (e) {$("#editTrackError").html("");$("#editTrackError").hide();$('#editTrackConfirm').button('reset');});
                                $('#editTrackConfirm').click(function () {
                                    var btn = $(this);
                                    btn.button('loading');
                                    $.post( "${edittrackaction}", $('#edittrackform').serialize())
                                            .fail(function(xhr,textstatus,error){$("#editTrackError").html(error);$("#editTrackError").show();;$('#editTrackConfirm').button('reset');})
                                            .success(function(data) {
                                                if(data == "OK")
                                                {
                                                    $("#editTrackError").html("");
                                                    $("#editTrackError").hide();
                                                    location.reload();
                                                }
                                                else
                                                {
                                                    $("#editTrackError").html(data);
                                                    $("#editTrackError").show();
                                                    btn.button('reset');
                                                }
                                            })
                                            .always(function() { /*btn.button('reset');*/});
                                });
                            </script>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="playerScoreDeleteDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="deletePlayerScoreLabel">Delete a racing event</h4>
                    </div>
                    <div class="modal-body">
                        <div id="deleteError" style="display: none;" class="alert alert-danger"></div>
                        <form:form id="deleteplayerscoreform" action='' role="form">
                            <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                            <div class="row">
                                <div class="col-lg-12"><input id="player_score_id" name="player_score_id" type="hidden" value=""/><span><s:message code="deleting.player-score"/></span></div>
                            </div>
                            <div class="row">
                                <h3 class="text-center"><span id="del_player_score" class="text-danger"></span></h3>
                            </div>
                            <div class="row">
                                <div class="col-lg-12"><span><s:message code="really-delete.player-score"/>?</span></div>
                            </div>
                        </form:form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                        <button id="delPlayerScoreConfirm" type="button" class="btn-danger btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.delete"/></button>
                    </div>
                    <script type="text/javascript">
                        function prepareToDelete(id, text)
                        {
                            $("#del_player_score").text(text);
                            $("#deleteplayerscoreform #player_score_id").val(id);
                            $("#playerScoreDeleteDialog").modal("show");
                        }

                        $('#delPlayerScoreConfirm').click(function () {
                            var btn = $(this);
                            btn.button('loading');
                            $.post( "<c:url value="/admin/playerscores/${eventid}/${trackid}/delete" />",  $('#deleteplayerscoreform').serialize())
                                    .fail(function(xhr,textstatus,error){$("#deleteError").html(error);$("#deleteError").show();btn.button('reset');})
                                    .success(function(data) {
                                        if(data == "OK")
                                        {
                                            $("#deleteError").html("");
                                            $("#deleteError").hide();
                                            //$('playerScoreDeleteDialog').modal('hide')
                                            location.reload();
                                        }
                                        else if(data == "NOTEXISTS")
                                        {
                                            $("#deleteError").html("<s:message code="NotExists.player-score" javaScriptEscape="true"/>");
                                            $("#deleteError").show();
                                            btn.button('reset');
                                        }
                                        else
                                        {
                                            $("#deleteError").html(data);
                                            $("#deleteError").show();
                                            btn.button('reset');
                                        }
                                    })
                                    .always(function() { /*btn.button('reset');*/});
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</body>
</html>