<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><s:message code="page.title"/></title>
        <style type="text/css">
            div.row.not-first-margin-top-10 {
                border-bottom: 1px solid black;
                padding-bottom: 2px;
                padding-top: 0px;
            }
            div.row.not-first-margin-top-10:nth-child(odd) {
                background-color: #f9f9f9;
            }
            .not-first-margin-top-10:not(:first-child) {
                margin-top: 0px;
            }
            #tracklist .panel .panel-body .row {
                text-align: center;
            }
        </style>
    </head>
    <body role="document">
        <!-- Docs master nav -->
        <div class="container bs-docs-container">
            <div class="row">
                <div class="col-md-12"><button style="float: right;" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#trackAddDialog"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add a track</button></div>
            </div>
            <div class="row">
                <div class="col-md-12"><span>&nbsp;</span></div>
            </div>
            <div class="row" id="tracklist">
                <div class="col-md-12">
                    <c:choose>
                        <c:when test="${not empty tracks}">
                            <div class="panel panel-success drop-shadow-down">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span>Track list - (${tracks.size()})</span></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row not-first-margin-top-10">
                                        <div class="col-md-1 margins-top-bottom-10" ><strongId</strong></div>
                                        <div class="col-md-4 margins-top-bottom-10"><strong>Track name</strong></div>
                                        <div class="col-md-2 margins-top-bottom-10"><strong>Track code</strong></div>
                                        <div class="col-md-1 margins-top-bottom-10"><strong>Image</strong></div>
                                        <div class="col-md-2 margins-top-bottom-10"><strong><s:message code="button.edit"/></strong></div>
                                        <div class="col-md-2 margins-top-bottom-10"><strong><s:message code="button.delete"/></strong></div>
                                    </div>
                                <c:forEach var="track" items="${tracks}" varStatus="status">
                                    <div class="row not-first-margin-top-10">
                                        <div class="col-md-1 margins-top-bottom-10" >${track.id}</div>
                                        <div class="col-md-4 margins-top-bottom-10">${track.track_name}</div>
                                        <div class="col-md-2 margins-top-bottom-10">
                                            <c:if test="${track.track_code != null}">
                                                ${track.track_code}
                                            </c:if>
                                        </div>
                                        <div class="col-md-1 margins-top-bottom-2">
                                            <c:if test="${track.track_code != null}">
                                                <img src="<s:url value="/resources/images/maps/${track.track_code}.png" />" width="50px" height="38px" />
                                            </c:if>
                                        </div>
                                        <div class="col-md-2 margins-top-bottom-10"><button onclick="prepareToEdit(${track.id},'${track.track_name}','${track.track_code}');return false;" class="btn btn-xs btn-success btn-block"><span class="glyphicon glyphicon-edit"></span>&nbsp;<s:message code="button.edit"/></button></div>
                                        <div class="col-md-2 margins-top-bottom-10"><button onclick="prepareToDelete(${track.id},'${track.track_name}');return false;" class="btn btn-xs btn-danger btn-block"><span class="glyphicon glyphicon-remove"></span>&nbsp;<s:message code="button.delete"/></button></div>
                                    </div>
                                </c:forEach>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="panel panel-warning drop-shadow-down">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span>Track list</span></h3>
                                </div>
                                <div class="panel-body">
                                    No tracks :(
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="trackAddDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="addTrackLabel">Add a track</h4>
                    </div>
                    <s:url value="/admin/tracks/add" var="addaction"/>
                    <form:form id="addtrackform" onsubmit='$("#addTrackConfirm" ).trigger( "click" );return false;' action='${addaction}' commandName="track" method="post">
                        <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                        <div class="modal-body">
                            <div id="addTrackError" style="display: none;" class="alert alert-danger"></div>
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon">Track name</span>
                                    <form:input path="track_name" type="text" class="form-control" placeholder="Track name" maxlength="64"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon">Track code</span>
                                    <form:input path="track_code" type="text" class="form-control" placeholder="Track code" maxlength="32"/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                            <button id="addTrackConfirm" type="button" class="btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.add"/></button>
                            <script type="text/javascript">
                                $('#trackAddDialog').on('show.bs.modal', function (e) {$("#addTrackError").html("");$("#addTrackError").hide();});
                                $('#addTrackConfirm').click(function () {
                                    if($("#addtrackform #track_name").val().length == 0)
                                    {
                                        $("#addTrackError").html("<s:message code='NotEmpty.track_name' javaScriptEscape='true' />");
                                        $("#addTrackError").show();
                                        return;
                                    }
                                    var btn = $(this);
                                    btn.button('loading');
                                    $.post( "<s:url value="/admin/tracks/add" />", $('#addtrackform').serialize())
                                            .fail(function(xhr,textstatus,error){$("#addTrackError").html(error);$("#addTrackError").show();})
                                            .success(function(data) {
                                                if(data == "OK")
                                                {
                                                    $("#addTrackError").html("");
                                                    $("#addTrackError").hide();
                                                    //$('#trackAddDialog').modal('hide');
                                                    $('#track_name').val("");
                                                    location.reload();
                                                }
                                                else if(data == "EXISTS")
                                                {
                                                    $("#addTrackError").html("<s:message code="AlreadyExists.track" javaScriptEscape="true"/>");
                                                    $("#addTrackError").show();
                                                    btn.button('reset');
                                                }
                                                else
                                                {
                                                    $("#addTrackError").html(data);
                                                    $("#addTrackError").show();
                                                    btn.button('reset');
                                                }
                                            })
                                            .always(function() { /*btn.button('reset');*/});
                                });
                            </script>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="trackEditDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="editTrackLabel">Edit a track</h4>
                    </div>
                    <s:url value="/admin/tracks/edit" var="editaction"/>
                    <form:form id="edittrackform" onsubmit='$("#editTrackConfirm" ).trigger( "click" );return false;' action='${editaction}' commandName="track" method="post" role="form">
                        <div class="modal-body">
                            <div id="editError" style="display: none;" class="alert alert-danger"></div>
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon">Track name</span>
                                    <form:input path="track_name" type="text" class="form-control" placeholder="Track name" maxlength="64"/>
                                    <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                                    <input id="edit_track_id" name="id" type="hidden" value=""/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon">Track code</span>
                                    <form:input path="track_code" type="text" class="form-control" placeholder="Track code" maxlength="32"/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                            <button id="editTrackConfirm" type="button" class="btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.save"/></button>
                            <script type="text/javascript">
                                $('#trackEditDialog').on('show.bs.modal', function (e) {$("#editError").html("");$("#editError").hide();$('#editTrackConfirm').button('reset');});
                                function prepareToEdit(id, track, code)
                                {
                                    $("#edittrackform #track_name").val(track);
                                    $("#edit_track_id").val(id);
                                    $("#edittrackform #track_code").val(code);
                                    $("#trackEditDialog").modal("show");
                                }
                                $('#editTrackConfirm').click(function () {
                                    if($("#edittrackform #track_name").val().length == 0)
                                    {
                                        $("#editError").html("<s:message code='NotEmpty.track_name' javaScriptEscape='true' />");
                                        $("#editError").show();
                                        return;
                                    }
                                    var btn = $(this);
                                    btn.button('loading');
                                    $.post( "${editaction}", $('#edittrackform').serialize())
                                            .fail(function(xhr,textstatus,error){$("#editError").html(error);$("#editError").show();;$('#editTrackConfirm').button('reset');})
                                            .success(function(data) {
                                                if(data == "OK")
                                                {
                                                    $("#editError").html("");
                                                    $("#editError").hide();
                                                    $('#edittrackform #track_name').val("");
                                                    location.reload();
                                                }
                                                else if(data == "EXISTS")
                                                {
                                                    $("#editError").html("<s:message code="AlreadyExists.track" javaScriptEscape="true"/>");
                                                    $("#editError").show();
                                                    btn.button('reset');
                                                }
                                                else
                                                {
                                                    $("#editError").html(data);
                                                    $("#editError").show();
                                                    btn.button('reset');
                                                }
                                            })
                                            .always(function() { /*btn.button('reset');*/});
                                });
                            </script>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="trackDeleteDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="deleteTrackLabel">Delete a track</h4>
                    </div>
                    <div class="modal-body">
                        <div id="deleteError" style="display: none;" class="alert alert-danger"></div>
                        <s:url value="/admin/tracks/delete" var="delaction"/>
                        <form:form id="deletetrackform" action='${delaction}'>
                            <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                            <div class="row">
                                <div class="col-lg-12"><input id="track_id" name="track_id" type="hidden" value=""/><span><s:message code="deleting.track-name"/></span></div>
                            </div>
                            <div class="row">
                                <h3 class="text-center"><span id="del_track_name" class="text-danger"></span></h3>
                            </div>
                            <div class="row">
                                <div class="col-lg-12"><span><s:message code="really-delete.track"/>?</span></div>
                            </div>
                        </form:form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                        <button id="deleteTrackConfirm" type="button" class="btn-danger btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.delete"/></button>
                    </div>
                    <script type="text/javascript">
                        function prepareToDelete(id, name)
                        {
                            $("#del_track_name").text(name);
                            $("#track_id").val(id);
                            $("#trackDeleteDialog").modal("show");
                        }

                        $('#deleteTrackConfirm').click(function () {
                            var btn = $(this);
                            btn.button('loading');
                            $.post( "${delaction}",  $('#deletetrackform').serialize())
                                    .fail(function(xhr,textstatus,error){$("#deleteError").html(error);$("#deleteError").show();btn.button('reset');})
                                    .success(function(data) {
                                        if(data == "OK")
                                        {
                                            $("#deleteError").html("");
                                            $("#deleteError").hide();
                                            location.reload();
                                        }
                                        else if(data == "NOTEXISTS")
                                        {
                                            $("#deleteError").html("<s:message code="NotExists.track" javaScriptEscape="true"/>");
                                            $("#deleteError").show();
                                            btn.button('reset');
                                        }
                                        else
                                        {
                                            $("#deleteError").html(data);
                                            $("#deleteError").show();
                                            btn.button('reset');
                                        }
                                    })
                                    .always(function() { /*btn.button('reset');*/});
                        });
                    </script>
                </div>
            </div>
        </div>
    </body>
</html>