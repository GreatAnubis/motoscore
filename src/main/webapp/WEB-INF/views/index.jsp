<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><s:message code="page.title"/></title>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/plugins/jui_dropdown/jquery.jui_dropdown.css"/>"/>
        <style type="text/css">
            .container4 {display: inline-block;}
            .menu4 {position: absolute;border: 1px solid rgb(0, 0, 0);background-color: rgb(66, 139, 202);list-style-type: none;color: white;z-index: 100;width: 100px !important;margin-top: 3px !important;}
            #menu4 .ui-menu-item:hover {background-color: #357ebd;}
            /* fix for jquery-ui-bootstrap theme */
            #launcher4 span {display: inline-block;}
        </style>
        <script type="text/javascript" src="<c:url value="/resources/bootstrap-3.1.1/dist/js/bootstrap.youtubepopup.min.js"/>" ></script>
        <script type="text/javascript" src="<c:url value="/resources/plugins/jui_dropdown/jquery.jui_dropdown.min.js"/>"></script>
        <script type="text/javascript">
            $(function() {

                $("#demo_drop4").jui_dropdown({
                    launcher_id: 'launcher4',
                    launcher_container_id: 'launcher4_container',
                    menu_id: 'menu4',
                    launcherUIShowText: false,
                    launcherUISecondaryIconClass: 'ui-icon-gear',
                    menuClass: 'menu4',
                    containerClass: 'container4',
                    my_position: 'right top',
                    at_position: 'right bottom',
                    onSelect: function(event, data) {
                        $("#result").text('index: ' + data.index + ' (id: ' + data.id + ')');
                    }
                });
                $("#month_selection").change(function() {
                    var temp = $("#scorePerMonth ul").html();
                    $("#scorePerMonth ul").empty();
                    $(this).hide();
                    $("#lastMonthScoresLoader").show();
                    $.getJSON('<s:url value="/lastMonthScore"/>', {
                        month: this.value
                    }).done(function (data) {
                                var obj = data.scores;  // get entry object (array) from JSON data
                                var range = data.range;
                                $("#monthWinner").text(range);
                                var parent = $("#scorePerMonth ul");
                                // iterate over the array and build the list
                                for (var i = 0, l = obj.length; i < l; ++i) {
                                    parent.append('<li class="list-group-item"><span class="pull-left badge">'+obj[i].place+'</span>&nbsp;'+obj[i].player.player_nick+'<span class="pull-right label label-success">'+obj[i].score+'</span></li>');
                                }
                                blinkTag($("#scorePerMonth ul li.list-group-item span.badge:contains('1')").parent());
                            }).fail(function() {$("#scorePerMonth ul").html(temp);}).always(function() {$("#lastMonthScoresLoader").hide();$("#month_selection").show();});
                });

                $(".years *").on("click", function(e){
                    var thisClass = $(this).attr("class");
                    if(thisClass.match(/month_/))
                    {
                        $("a[class^='month_']").removeClass("selected");
                        var month = $(this).attr("class").replace(/month_/,'');
                        var yearTag = $(this).parent().prev();//.closest("[class^='year_']");
                        $(yearTag).removeClass("selected");
                        var year = $(yearTag).removeClass("selected").attr("class").replace(/year_/,'');
                        //select new
                        //$(yearTag).addClass("selected");
                        //$(this).addClass("selected");
                        loadEvents(year, month);

                    }
                    else if(thisClass.match(/year_/))
                    {
                        $("a[class^='month_']").removeClass("selected");
                        $("a[class^='year_']").removeClass("selected");
                        var year = $(this).attr("class").replace(/year_/,'');
                        loadEvents(year, null);
                    }
                });

                //ChangeIt();
                loadThisYearScores();
                loadLastMonthScores();
                loadLastEventScores();
                var d = new Date();
                loadEvents(d.getFullYear(), d.getMonth()+1);

            });

            var blinkTag = function blink(e){
                $(e).fadeOut('slow', function(){
                    $(this).fadeIn('slow', function(){
                        if(!$(this).is("[blink-data]"))
                        {
                            $(this).attr("blink-data", 0);
                        }
                        var blinkTimes =  parseInt($(this).attr("blink-data"), 10) + 1;
                        $(this).attr("blink-data", blinkTimes);
                        if(blinkTimes <= 3)
                        {
                            blink(this);
                        }
                    });
                });
            }

            function changeMonth(month)
            {
                var temp = $("#scorePerMonth ul").html();
                $("#scorePerMonth ul").empty();
                //$(this).hide();
                $("#lastMonthScoresLoader").show();
                $.getJSON('<s:url value="/lastMonthScore"/>', {
                    month: month
                }).done(function (data) {
                            var obj = data.scores;  // get entry object (array) from JSON data
                            var range = data.range;
                            $("#monthWinner").text(range);
                            var parent = $("#scorePerMonth ul");
                            // iterate over the array and build the list
                            for (var i = 0, l = obj.length; i < l; ++i) {
                                parent.append('<li class="list-group-item"><span class="pull-left badge">'+obj[i].place+'</span>&nbsp;'+obj[i].player.player_nick+'<span class="pull-right label label-success">'+obj[i].score+'</span></li>');
                            }
                            blinkTag($("#scorePerMonth ul li.list-group-item span.badge:contains('1')").parent());
                        }).fail(function() {$("#scorePerMonth ul").html(temp);}).always(function() {$("#lastMonthScoresLoader").hide();/*$("#month_selection").show();*/});
            }

            function loadEvents(iyear, imonth)
            {
                var tagLoading = "";
                $(".year_"+iyear).addClass("selected");
                if(imonth != null)
                {
                    imonth = imonth.toString();
                    $(".month_"+imonth).addClass("selected");
                    tagLoading = $(".month_"+imonth).find("span");
                }
                else
                {
                    tagLoading = $(".year_"+iyear).find("span");
                }

                $(tagLoading).html('<img src="<s:url value="/resources/images/ajax_loader_1.gif"/>"  style="width: 48px;height: 21px;"/>');

                    var temp = $("#events").html();
                    $("#events").html("");
                    //$("#thisYearScoresLoader").show();
                    var jqxhr = $.ajax({
                        url: '<s:url value="/index/events"/>',
                        data: {
                            "year": iyear,
                            "month": imonth
                        },
                    success: function (data) {
                            var obj = data;  // get entry object (array) from JSON data
                            //$("#yearWinner").text(range);
                            $("#events").html(obj);
                    },
                    error: function() {
                        $("events").html(temp);
                    }});
                jqxhr.always(function() {
                    $(tagLoading).html("");
                    reApplyEvents();
                });
            }

            function reApplyEvents()
            {
                $("span[data-event-visible]:first").closest(".panel-heading").next(".panel-body").toggleClass("content-score");
                <security:authorize ifAnyGranted="ROLE_MODERATOR">
                $("span[data-event-visible]:first").closest(".panel-heading").find(".btnSave").toggleClass("content-score");
                </security:authorize>
                $('span[data-event-visible]').click(function() {
                    $(this).closest(".panel-heading").next(".panel-body").toggleClass("content-score");
                    <security:authorize ifAnyGranted="ROLE_MODERATOR">
                    $(this).closest(".panel-heading").find(".btnSave").toggleClass("content-score");
                    </security:authorize>
                });
                $('.fancybox-media')
                        .attr('rel', 'media-gallery')
                        .fancybox({
                            openEffect : 'none',
                            closeEffect : 'none',
                            prevEffect : 'none',
                            nextEffect : 'none',

                            arrows : false,
                            helpers : {
                                media : {},
                                buttons : {}
                            }
                        });

                $('.popver').on({
                    mousemove: function(e) {
                        $(this).next('img').css({
                            top: e.pageY - 260,
                            left: e.pageX - ($(this).next('img').width() / 2)
                        });
                    },
                    mouseenter: function() {
                        var big = $('<img />', {'class': 'big_img', src: $(this).attr("data-image")});
                        $(this).after(big);
                    },
                    mouseleave: function() {
                        $('.big_img').remove();
                    }
                });

                <security:authorize ifAnyGranted="ROLE_MODERATOR">
                $(".btnSave").click(function() {
                    $(this).hide();
                    var btn = $(this);
                    html2canvas( $(this).closest("[data-print]"), {
                        onrendered: function(canvas) {
                            btn.show();
                            theCanvas = canvas;
                            $("#printing_area").show();
                            $(".img-out").append(canvas);

                            // Convert and download as image
                            Canvas2Image.saveAsPNG(canvas);
                            $("#printing_area").hide();
                            //$("#img-out")
                            //$(".btnSave").closest("[data-print]")[0].append(canvas);
                            // Clean up
                            $(".img-out").html("");
                        }
                    });
                });
                </security:authorize>
            }

            function loadLastEventScores()
            {
                var temp = $("#scoreLastEvent ul").html();
                $("#scoreLastEvent ul").html("");
                $("#scoreLastEventLoader").show();
                $.getJSON('<s:url value="/lastEventScore"/>', {}).done(function (data) {
                            var obj = data.scores;  // get entry object (array) from JSON data
                            var range = data.range;
                            //$("#monthWinner").text(range);
                            var parent = $("#scoreLastEvent ul");
                            // iterate over the array and build the list
                            for (var i = 0, l = obj.length; i < l; ++i) {
                                parent.append('<li class="list-group-item"><span class="pull-left badge">'+obj[i].place+'</span>&nbsp;'+obj[i].player.player_nick+'<span class="pull-right label label-success">'+obj[i].score+'</span></li>');
                            }
                            blinkTag($("#scoreLastEvent ul li.list-group-item span.badge:contains('1')").parent());
                        }).fail(function() {$("#scoreLastEvent ul").html(temp);}).always(function() {$("#scoreLastEventLoader").hide();});
            }

            function loadLastMonthScores()
            {
                var temp = $("#scorePerMonth ul").html();
                $("#scorePerMonth ul").html("");
                $("#month_selection").hide();
                $("#lastMonthScoresLoader").show();
                $.getJSON('<s:url value="/lastMonthScore"/>', { }).done(function (data) {
                            var obj = data.scores;  // get entry object (array) from JSON data
                            var range = data.range;
                            $("#monthWinner").text(range);
                            var parent = $("#scorePerMonth ul");
                            // iterate over the array and build the list
                            for (var i = 0, l = obj.length; i < l; ++i) {
                                parent.append('<li class="list-group-item"><span class="pull-left badge">'+obj[i].place+'</span>&nbsp;'+obj[i].player.player_nick+'<span class="pull-right label label-success">'+obj[i].score+'</span></li>');
                            }
                            blinkTag($("#scorePerMonth ul li.list-group-item span.badge:contains('1')").parent());
                        }).fail(function() {$("#scorePerMonth ul").html(temp);}).always(function() {$("#lastMonthScoresLoader").hide();$("#month_selection").show();});
            }


            function loadThisYearScores()
            {
                var temp = $("#scorePerYear ul").html();
                $("#scorePerYear ul").html("");
                $("#thisYearScoresLoader").show();
                $.getJSON('<s:url value="/thisYearScore"/>', {
                    ajax: 'true'
                }).done(function (data) {
                            var obj = data.scores;  // get entry object (array) from JSON data
                            var range = data.range;
                            $("#yearWinner").text(range);
                            var parent = $("#scorePerYear ul");
                            // iterate over the array and build the list
                            for (var i = 0, l = obj.length; i < l; ++i) {
                                parent.append('<li class="list-group-item"><span class="pull-left badge">'+obj[i].place+'</span>&nbsp;'+obj[i].player.player_nick+'<span class="pull-right label label-success">'+obj[i].score+'</span></li>');
                            }
                            blinkTag($("#scorePerYear ul li.list-group-item span.badge:contains('1')").parent());
                        }).fail(function() {$("#scorePerYear ul").html(temp);}).always(function() {$("#thisYearScoresLoader").hide();});
            }
            var totalCount = 3;
            function ChangeIt() 
            {
                var num = Math.ceil( Math.random() * totalCount );
                $("body").css("background","../../../images/background_"+num+".jpg");
            }
        </script>
    </head>
    <body role="document">
        <div class="container-fluid">
            <div class="row row-offcanvas row-offcanvas-left">
                <div class="col-xs-6 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
                    <div class="years">
                        <a class="year_2014" rel="filter">
                            2014
                            <span></span>
                        </a>
                        <div class="months">
                            <c:forEach var="monthvar" items="${monthvars}">
                            <a class="month_${monthvar}" rel="filter">
                                <s:message code="month.${monthvar}"/>
                                <span></span>
                            </a>
                            </c:forEach>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <p class="pull-left visible-xs">
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle navigation</button>
                    </p>
                    <div class="row">
                        <div class="alert alert-warning">
                            <strong>Ostatnia aktualizacja:</strong> 2014-10-05 . Site v2 - under construction (60% completed): <a href="/angular/">https://greatanubis-motoscore.rhcloud.com/angular/</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="panel panel-primary drop-shadow-down" style="position: relative;">
                                <div id="thisYearScoresLoader" class="ajax_loader" style="display: none;"></div>
                                <div class="panel-heading">Punktacja roku <span id="yearWinner">-</span></div>
                                <div  id="scorePerYear" style="max-height: 165px;min-height: 165px;height: 165px;overflow-y: auto;margin-bottom: 0;  border-bottom-right-radius: 4px;  border-bottom-left-radius: 4px;">
                                    <ul class="list-group">
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="panel panel-primary drop-shadow-down" style="position: relative;">
                                <div id="lastMonthScoresLoader" class="ajax_loader" style="display: none;"></div>
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 pull-left">Punktacja miesiąca: <span id="monthWinner">-</span>
                                            <div id="demo_drop4" style="float: right;">
                                                <div id="launcher4_container">
                                                    <button id="launcher4"><span style="color: #428bca;" class="glyphicon glyphicon-list"></span></button>
                                                </div>
                                                <ul id="menu4">
                                                    <!--select id="month_selection" size="1" style="color: black;" class="pull-right"-->
                                                        <c:forEach var="monthvar" items="${monthvars}" varStatus="statusMonth">
                                                            <li style="margin-left: -40px;"><a href="javascript:changeMonth(${monthvar});" style="color: white;margin-left: 14px;text-decoration: none;"><s:message code="month.${monthvar}"/></a></li>
                                                            <%--option label="<s:message code="month.${monthvar}"/>" value="${monthvar}"  <c:if test="${monthvar == (monthvars.size() - 1)}">selected</c:if>><s:message code="month.${monthvar}" /></OPTION--%>
                                                        </c:forEach>
                                                    <!--/select-->
                                                </ul>
                                            </div>

                                            <%--select id="month_selection" size="1" style="color: black;" class="pull-right">
                                                <c:forEach var="monthvar" items="${monthvars}" varStatus="statusMonth">
                                                    <option label="<s:message code="month.${monthvar}"/>" value="${monthvar}"  <c:if test="${monthvar == (monthvars.size() - 1)}">selected</c:if>><s:message code="month.${monthvar}" /></OPTION>
                                                </c:forEach>
                                            </select--%>
                                        </div>
                                    </div>
                                </div>
                                <div id="scorePerMonth" style="max-height: 165px;min-height: 165px;height: 165px;overflow-y: auto;margin-bottom: 0;  border-bottom-right-radius: 4px;  border-bottom-left-radius: 4px;">
                                    <ul class="list-group">
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="panel panel-primary drop-shadow-down" style="position: relative;">
                                <div id="scoreLastEventLoader" class="ajax_loader" style="display: none;"></div>
                                <div class="panel-heading">Punktacja ostatniego eventu</div>
                                <div id="scoreLastEvent" style="max-height: 165px;min-height: 165px;height: 165px;overflow-y: auto;margin-bottom: 0;  border-bottom-right-radius: 4px;  border-bottom-left-radius: 4px;">
                                    <ul class="list-group">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="events" class="row">
                        <!-- events here -->
                    </div> <!-- /.row -->
                </div> <!-- /.row -->
            </div> <!-- /.row off-canvas-->
            <hr>
            <footer class="bs-docs-footer" role="contentinfo">
              <p>
                  ©2014 Anubis
              </p><!-- .container -->
            </footer><!-- footer -->
        </div><!-- /.container -->
    </body>
</html>