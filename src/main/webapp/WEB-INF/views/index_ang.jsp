<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <link rel="shortcut icon" type="image/png" href='<c:url value="/resources/ico/favicon.png"/>' />
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href='<c:url value="/resources/bower_components/bootstrap/dist/css/bootstrap.min.css"/>' >
        <link rel="stylesheet" href="<c:url value="/resources/css/offcanvas.css"/>">
        <!-- Optional theme -->
        <link rel="stylesheet" href='<c:url value="/resources/bower_components/bootstrap/dist/css/bootstrap-theme.min.css"/>'>
        <link rel="stylesheet" href='<c:url value="/resources/css/moto.css"/>'>
        <!-- Fancy Box -->
        <link rel="stylesheet" href='<c:url value="/resources/bower_components/font-awesome/css/font-awesome.min.css"/>'>
        <link rel="stylesheet" href='<c:url value="/resources/css/famfamfam-flags.css"/>'>

        <script type="text/javascript">var _contextPath = "${contextPath}"</script>
        <script type="text/javascript" src='<c:url value="/resources/bower_components/angular/angular.min.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/bower_components/angular-route/angular-route.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/bower_components/angular-resource/angular-resource.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/bower_components/angular-cookies/angular-cookies.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/bower_components/angular-translate/angular-translate.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/bower_components/angular-dynamic-locale/tmhDynamicLocale.min.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/bower_components/angular-i18n/angular-locale_pl-pl.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/bower_components/angular-animate/angular-animate.min.js"/>'></script>
        <!-- Custom implementation -->
        <script type="text/javascript" src='<c:url value="/resources/angularjs/app/utils.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/angularjs/app/config.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/angularjs/app/motoscoreapp.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/angularjs/app/motoservices.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/angularjs/app/motodirectives.js"/>'></script>

    </head>
    <body ng-app="motoscoreApp" >
        <!--div ng-if="loading" ng-class="{loading: loading}"><i class="fa fa-refresh fa-5x fa-spin" style="z-index: 1001;top: 50%;left: 50%;position: absolute;"></i></div-->
        <!--[if lt IE 10]>
        <p class="browsehappy" translate="global.browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <header class="navbar navbar-default navbar-fixed-top navbar-inverse">
            <div class="navbar-inner">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" ng-click="isCollapsed = !isCollapsed">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="javascript:void(0)"><span translate="global.title">Moto-score</span></a>
                    </div>
                    <nav class="hidden-xs" >
                        <ul class="nav navbar-nav" ng-controller="HeaderController">
                            <li ng-class="{ active: isActive('/') || isActive('/index')}"><a href="#" translate="global.menu.home"><i class="fa fa-home fa-fw"></i>Home</a></li>
                            <li ng-class="{ active: isActive('/admin/playerlist')}" ng-if="hasRole('ROLE_MODERATOR')"><a href="#admin/playerlist" translate="global.menu.admin.playerlist"><i class="fa fa-users fa-fw"></i>Player list</a></li>
                            <li ng-class="{ active: isActive('/admin/tracklist')}" ng-if="hasRole('ROLE_MODERATOR')"><a href="#admin/tracklist" translate="global.menu.admin.tracklist"><i class="fa fa-stumbleupon fa-fw"></i>Track list</a></li>
                            <li ng-class="{ active: isActive('/admin/vehiclelist')}"ng-if="hasRole('ROLE_MODERATOR')"><a href="#admin/vehiclelist" translate="global.menu.admin.vehiclelist"><i class="fa fa-car fa-fw"></i>Vehicle list</a></li>
                            <li ng-class="{ active: isActive('/admin/racinglist')}" ng-if="hasRole('ROLE_MODERATOR')"><a href="#admin/racinglist" translate="global.menu.admin.racingevents"><i class="fa fa-globe fa-fw"></i>Racing events</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li ng-if="user">
                                <a ng-controller="loggedUserDropDownCtrl">
                                    <span class="dropdown" on-toggle="toggled(open)">
                                        <span class="dropdown-toggle" translate="global.menu.logged-as" translate-values="{username: user.name }"><i class="fa fa-user fa-fw"></i>Logged-in as {{username}}</span>
                                        <ul class="dropdown-menu navbar-inverse">
                                            <li>
                                                <span style="text-align: center;width: 100%;float: left;" ng-click="logout()" translate="global.menu.logout"><i class="fa fa-power-off fa-fw"></i>Logout</span>
                                            </li>
                                        </ul>
                                    </span>
                                </a>
                            </li>
                            <li><a id="login" ng-hide="user" ng-href="#login" translate="global.menu.login"><i class="fa fa-lock fa-fw"></i>Log in</a><!--a id="logout" ng-show="user" href="javascript:void(0)" ng-click="logout()">Log out</a--></li>
                            <li class="dropdown pointer" ng-controller="LanguageController">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                                    <span>
                                        <span class="glyphicon glyphicon-flag"></span>
                                        <span class="hidden-tablet" translate="global.menu.language">Language</span>
                                        <b class="caret"></b>
                                    </span>
                                </a>
                                <ul class="dropdown-menu inverse-dropdown">
                                    <li active-menu="en">
                                        <a ng-href="" ng-click="changeLanguage($event,&#x27;en&#x27;)"><span class="famfamfam-flag-gb"></span>
                                            &#xA0;<span translate="global.language.en">English</span></a>
                                    </li>
                                    <li active-menu="pl">
                                        <a ng-href="" ng-click="changeLanguage($event,&#x27;pl&#x27;)"><span class="famfamfam-flag-pl"></span>
                                            &#xA0;<span translate="global.language.pl">Polish</span></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!--li class="dropdown pointer" ng-controller="LanguageController">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                                                            <span>
                                                                <span class="glyphicon glyphicon-flag"></span>
                                                                <span class="hidden-tablet" translate="global.menu.language">Language</span>
                                                                <b class="caret"></b>
                                                            </span>
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li active-menu="en">
                                                                <a href="#" ng-click="changeLanguage(&#x27;en&#x27;)"><span class="famfamfam-flag-gb"></span>
                                                                    &#xA0;<span translate="global.language.en">English</span></a>
                                                            </li>
                                                            <li active-menu="pl">
                                                                <a href="#" ng-click="changeLanguage(&#x27;pl&#x27;)"><span class="famfamfam-flag-pl"></span>
                                                                    &#xA0;<span translate="global.language.pl">Polish</span></a>
                                                            </li>
                                                        </ul>
                                                    </li-->
                    </nav>
                    <nav class="visible-xs collapse" collapse="!isCollapsed" style="height: 0px;">
                        <ul class="nav navbar-nav">
                            <li><a href="#" ng-click="isCollapsed = !isCollapsed" class="" translate="global.home">Home</a></li>
                            <li><a ng-hide="user" ng-href="#login" ng-click="isCollapsed = !isCollapsed" class="" translate="global.menu.login">Log-in</a><a ng-show="user" href="javascript:void(0)" ng-click="isCollapsed = !isCollapsed;logout();" class="" translate="global.menu.logout"><i class="fa fa-power-off"></i>&nbsp;Log-out</a></li>

                        </ul>
                    </nav>

        <!--div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="navbar-inner">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" ng-click="isCollapsed = !isCollapsed">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="javascript:void(0)"><span translate="global.title">Moto-score</span></a>
                    </div>
                    <nav class="hidden-xs">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="javascript:void(0)">Home</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                           <li><a id="login" href="javascript:void(0)">Log in</a></li>
                        </ul>
                    </nav>
                    <nav class="visible-xs" collapse="!isCollapsed">
                        <ul class="nav navbar-nav">
                            <li><a href="#index" ng-click="isCollapsed = !isCollapsed">Home2</a></li>
                            <li><a href="javascript:void(0)" ng-click="isCollapsed = !isCollapsed">Log-in2</a></li>
                        </ul>
                    </nav-->
                    <%--div class="collapse navbar-collapse" id="navbar-collapse" ng-switch="authenticated">
                        <ul class="nav navbar-nav nav-pills navbar-right" ng-controller="AdminController">
                            <li>
                                <a href="javascript:void(0)">
                                    <span class="glyphicon glyphicon-home"></span>
                                    <span translate="global.menu.home">Home</span>
                                </a>
                            </li>
                            <li ng-switch-when="true" ng-show="isAuthorized(userRoles.admin)" class="dropdown pointer">
                                <a href="<s:url value='/admin/players/list'/>">
                                    <span class="glyphicon glyphicon-home"></span>
                                    <span translate="global.menu.home">Players</span>
                                </a>
                            </li>
                            <li ng-switch-when="true" ng-show="isAuthorized(userRoles.admin)" class="dropdown pointer">
                                <a href="<s:url value='/admin/tracks/list'/>">
                                    <span class="glyphicon glyphicon-home"></span>
                                    <span translate="global.menu.home">Tracks</span>
                                </a>
                            </li>
                            <li ng-switch-when="true" ng-show="isAuthorized(userRoles.admin)" class="dropdown pointer">
                                <a href="<s:url value='/admin/vehicles/list'/>">
                                    <span class="glyphicon glyphicon-home"></span>
                                    <span translate="global.menu.home">Vehicles</span>
                                </a>
                            </li>
                            <li ng-switch-when="true" ng-show="isAuthorized(userRoles.admin)" class="dropdown pointer">
                                <a href="<s:url value='/admin/racingevents/list'/>">
                                    <span class="glyphicon glyphicon-home"></span>
                                    <span translate="global.menu.home">Racing events</span>
                                </a>
                            </li>
                            <li class="dropdown pointer">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
                                    <span>
                                        <span class="glyphicon glyphicon-user"></span>
                                        <span class="hidden-tablet" translate="global.menu.account.main">
                                            Account
                                        </span>
                                        <b class="caret"></b>
                                    </span>
                                </a>
                                <ul class="dropdown-menu" ng-controller="MenuController">
                                    <!--li ng-switch-when="true"><a href="#/settings"><span class="glyphicon glyphicon-wrench"></span>
                                        &#xA0;<span translate="global.menu.account.settings">Settings</span></a></li>
                                    <li ng-switch-when="true"><a href="#/password"><span class="glyphicon glyphicon-lock"></span>
                                        &#xA0;<span translate="global.menu.account.password">Password</span></a></li>
                                    <li ng-switch-when="true"><a href="#/sessions"><span class="glyphicon glyphicon-cloud"></span>
                                        &#xA0;<span translate="global.menu.account.sessions">Sessions</span></a></li-->
                                    <li ng-switch-when="true"><a href="#/logout"><span class="glyphicon glyphicon-log-out"></span>
                                        &#xA0;<span translate="global.menu.account.logout">Log out</span></a></li>
                                    <li ng-switch-when="false"><a href="#/login"><span class="glyphicon glyphicon-log-in"></span>
                                        &#xA0;<span translate="global.menu.account.login">Log-in</span></a></li>
                                </ul>
                            </li>

                            <!--li ng-switch-when="true" ng-show="isAuthorized(userRoles.admin)" class="dropdown pointer" ng-controller="AdminController">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
                                    <span>
                                        <span class="glyphicon glyphicon-tower"></span>
                                        <span class="hidden-tablet" translate="global.menu.admin">Admin</span>
                                        <b class="caret"></b>
                                    </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="#/metrics"><span class="glyphicon glyphicon-dashboard"></span>
                                        &#xA0;<span translate="global.menu.account.metrics">Metrics</span></a></li>
                                    <li><a href="#/audits"><span class="glyphicon glyphicon-bell"></span>
                                        &#xA0;<span translate="global.menu.account.audits">Audits</span></a></li>
                                    <li><a href="#/logs"><span class="glyphicon glyphicon-tasks"></span>
                                        &#xA0;<span translate="global.menu.account.logs">Logs</span></a></li>
                                    <li><a href="#/docs"><span class="glyphicon glyphicon-book"></span>
                                        &#xA0;<span translate="global.menu.account.apidocs">API Docs</span></a></li>
                                </ul>
                            </li-->
                            <!--li class="dropdown pointer" ng-controller="LanguageController">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                                    <span>
                                        <span class="glyphicon glyphicon-flag"></span>
                                        <span class="hidden-tablet" translate="global.menu.language">Language</span>
                                        <b class="caret"></b>
                                    </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li active-menu="en">
                                        <a href="#" ng-click="changeLanguage(&#x27;en&#x27;)"><span class="famfamfam-flag-gb"></span>
                                            &#xA0;<span translate="global.language.en">English</span></a>
                                    </li>
                                    <li active-menu="pl">
                                        <a href="#" ng-click="changeLanguage(&#x27;pl&#x27;)"><span class="famfamfam-flag-pl"></span>
                                            &#xA0;<span translate="global.language.pl">Polish</span></a>
                                    </li>
                                </ul>
                            </li-->
                        </ul>
                    </div--%>
                </div><!-- /.container -->
            </div><!-- /.navbar -->
        </header>
        <div class="container-fluid" ng-view>
        </div><!-- /.container -->
        <footer class="bs-docs-footer">
            <div class="col-xs-9 col-xs-offset-2">
                <a href="https://angularjs.org/" target="_blank">
                    <img clas="pull-right" src="<c:url value="/resources/images/logos/AngularJS-small.png"/>"/>
                </a>
            </div>
        </footer><!-- footer -->
    </body>
</html>