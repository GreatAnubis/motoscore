<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="javascript:void(0)">Moto-score</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li ><a href="<c:url value="/index"/>">Home</a></li>
                <security:authorize ifAnyGranted="ROLE_MODERATOR">
                    <li><a href="<s:url value='/admin/players/list'/>">Players</a></li>
                    <li><a href="<s:url value='/admin/tracks/list'/>">Tracks</a></li>
                    <li><a href="<s:url value='/admin/vehicles/list'/>">Vehicles</a></li>
                    <li><a href="<s:url value='/admin/racingevents/list'/>">Racing events</a></li>
                </security:authorize>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <security:authorize ifAnyGranted="ROLE_ANONYMOUS">
                    <li><a id="login" href="javascript:void(0)">Log in</a></li>
                </security:authorize>
                <security:authorize ifAnyGranted="ROLE_MODERATOR">
                    <li><a href="javascript:void(0)">Logged-in as <%= request.getUserPrincipal().getName() %></a></li>
                    <li><a id="logout" href="<s:url value='/logout'/>">Log out</a></li>
                </security:authorize>
            </ul>
        </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
</div><!-- /.navbar -->
