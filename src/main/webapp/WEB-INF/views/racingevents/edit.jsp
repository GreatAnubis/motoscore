<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><s:message code="page.title"/></title>
        <script type="text/javascript">
            $(function() {
                $('#datetimepicker').datetimepicker({
                    //format: 'YYYY-MM-DD hh:mm',
                    weekStart: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    //startView: 2,
                    //forceParse: 1,
                    pick12HourFormat: false
                });
                $(".modal").draggable({
                    handle: ".modal-header"
                });
                $("#panel-scores-edit").sortable({
                            axis: "y",
                            handle: ".glyphicon-resize-vertical",
                            items: ".panel",
                            start: function(e, info) {

                            },
                            stop: function(e, info) {
                                var selector = $("#panel-scores-edit > .panel");
                                var dirtydata = false;
                                selector.each(function(index )
                                {
                                    var oneBasedIndex = index+1;
                                    var trackid = $(this).attr("data-track-id");
                                    var trackordering = $(this).attr("data-track-ordering");
                                    if(oneBasedIndex != trackordering)
                                    {
                                        var orderingInput = $(this).find(".panel-heading").find("input[id*=ordering]");
                                        $(this).attr("data-track-ordering", oneBasedIndex);
                                        orderingInput.val(oneBasedIndex);
                                        dirtydata = true;
                                    }
                                    if(oneBasedIndex == selector.length)
                                    {
                                        if(dirtydata == true && $("#updatedata").hasClass('btn-success') == true)
                                        {
                                            $("#updatedata").removeClass("btn-success").addClass("btn-warning");
                                        }
                                    }
                                });
                                //reenumerate
                                //send to controller
                            }
                        });
            });
        </script>
    </head>
    <body role="document">
        <!-- Docs master nav -->
        <div class="container bs-docs-container">
            <div class="row">
                <div class="col-md-12"><span>&nbsp;</span></div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <a href="<s:url value="/admin/racingevents/"/>" type="button" class="btn btn-default pull-left">Back</a>
                </div>
            </div>
            <div class="row"></div>
            <div class="row">
                <div class="col-md-12">
                <s:url value="/admin/racingevents/${racingevent.id}/updateall" var="action" />
                <form:form id="editracingeventform" action='${action}' modelAttribute="racingevent" method="post">
                <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                <div class="row">
                    <form:hidden path="id"/>
                    <div class="col-md-4">Data i godzina:&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="input-group input-group-lg date" id="datetimepicker" data-format="yyyy-MM-dd hh:mm">
                                <form:input path="timestamp" class="form-control" placeholder="Racing event timestamp"  />
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">Data i godzina:&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">Movie link</span>
                            <form:input id="movieLink" path="movieLink" placeholder="youtube link" type="text" class="form-control" maxlength="255"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">
                                <form:checkbox path="visible"/>
                            </span>
                            <span type="text" class="form-control">Visible</span>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-10">
                        <button onclick="javascript:void(0);return false;" style="float: right;" class="btn btn-primary btn-lg pull-left" data-toggle="modal" data-target="#trackInEventAddDialog"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add a track to event</button>
                        <button id="updatedata" type="submit" class="btn btn-success pull-right" ><span class="glyphicon glyphicon-save"></span>&nbsp;<s:message code="button.save"/></button>
                    </div>
                </div>
                <div class="row" style="margin-top: 40px;">
                    <div class="col-md-10">
                        <div id="panel-scores-edit" >
                            <c:set var="index" value="0" scope="page"/>
                            <c:forEach var="trackInEvent" items="${racingevent.tracks}" varStatus="i">
                            <div class="panel panel-default" data-track-id="${trackInEvent.id}" data-track-ordering="${trackInEvent.ordering}">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-6" style="text-align: center;">
                                        <s:bind path="tracks[${i.index}]">
                                            <input id="tracks${i.index}" type="hidden" name="${status.expression}" value="${trackInEvent.id}" />
                                        </s:bind>
                                        <form:hidden path="tracks[${i.index}].ordering"/>
                                        <span class="glyphicon glyphicon-resize-vertical pull-left" style="cursor: pointer;margin-top: 7px;"></span>
                                        <span class="pull-left" style="margin-right: 10px;margin-top: 5px;">
                                            <strong>#${trackInEvent.ordering}</strong>&nbsp;${trackInEvent.track.track_name}
                                        </span>
                                        <c:if test="${trackInEvent.track.track_code != null}">
                                            <img src="<s:url value="/resources/images/maps/${trackInEvent.track.track_code}.png" />" width="45px" height="33px" />
                                        </c:if>
                                        </div>
                                        <div class="col-md-4" style="text-align: center;">
                                            <ul style="display: block; white-space: nowrap; overflow: auto; margin-bottom: 0;">
                                            <c:forEach items="${trackInEvent.vehiclesInRun}" var="vehicle">
                                                <c:if test="${not empty vehicle.vehicle_code}">
                                                <li style="width: 60px;display: inline-block;">
                                                    <div style="width: 100%;text-align: center;vertical-align: middle;">
                                                        <img src="<s:url value="/resources/images/vehicles/icons/${vehicle.vehicle_code}.png"/>" width="45px" height="33px" title="${vehicle.vehicle_name}" />
                                                    </div>
                                                </li>
                                                </c:if>
                                            </c:forEach>
                                            </ul>
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn btn-block btn-danger pull-right" onclick="prepareToDelete('${racingevent.id}','${trackInEvent.id}');return false;"><span class="glyphicon glyphicon-remove"></span>&nbsp;<s:message code="button.delete"/></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table class="table">
                                        <thead>
                                            <th>Miejsce</th>
                                            <th>Punkty</th>
                                            <th>Nick</th>
                                        </thead>
                                        <tbody>
                                        <c:set var="indexscores" value="1" scope="page"/>
                                        <c:forEach var="playerScoreInRun" items="${trackInEvent.playerScoresInRun}">
                                            <tr>
                                                <td>${indexscores}</td>
                                                <td>${playerScoreInRun.score}</td>
                                                <td>${playerScoreInRun.player.player_nick}</td>
                                            </tr>
                                            <c:set var="indexscores" value="${indexscores+1}" scope="page"/>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                    <a href="<s:url value="/admin/playerscores/${racingevent.id}/${trackInEvent.id}/edit"/>" type="button" class="btn btn-sm btn-info pull-right">Edit run details</a>
                                </div>
                            </div>
                            <c:set var="index" value="${index+1}" scope="page"/>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                </form:form>
            </div>
        </div>
        <!-- Modal add track to event -->
        <div class="modal fade" id="trackInEventAddDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="addTrackLabel">Add a track to event</h4>
                    </div>
                    <s:url value="/admin/racingevents/${racingevent.id}/addtrack" var="addtracktoracingevent"/>
                    <form:form id="addtrackform" onsubmit='$("#addTrackConfirm" ).trigger( "click" );return false;' action='${addtracktoracingevent}' modelAttribute="trackInEvent" method="post">
                        <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                        <div class="modal-body">
                            <div id="addError" style="display: none;" class="alert alert-danger"></div>
                            <form:hidden path="id"/>
                            <input id="ordering" type="hidden" name="ordering" value="${index+1}" />
                            <div class="input-group input-group-lg">
                                <span class="input-group-addon">Available tracks</span>
                                <form:select path="track" items="${availableTracks}" itemLabel="track_name" itemValue="id" multiple="false" size="10" cssClass="form-control" cssStyle="height:auto;width:100%;"/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                            <button id="addTrackConfirm" type="button" class="btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.add"/></button>
                            <script type="text/javascript">
                                $('#trackAddDialog').on('show.bs.modal', function (e) {$("#addError").html("");$("#addError").hide();});
                                $('#addTrackConfirm').click(function (e) {
                                    if($("#track").val().length == 0)
                                    {
                                        $("#addError").html("<s:message code='NotEmpty.track_name' javaScriptEscape='true' />");
                                        $("#addError").show();
                                        return;
                                    }
                                    var btn = $(this);
                                    btn.button('loading');
                                    $.post( "${addtracktoracingevent}", $('#addtrackform').serialize())
                                            .fail(function(xhr,textstatus,error){$("#addError").html(error);$("#addError").show();btn.button('reset');})
                                            .success(function(data) {
                                                if(data == "OK")
                                                {
                                                    $("#addError").html("");
                                                    $("#addError").hide();
                                                    //$('#trackAddDialog').modal('hide');
                                                    $('#track_name').val("");
                                                    location.reload();
                                                }
                                                else
                                                {
                                                    $("#addError").html(data);
                                                    $("#addError").show();
                                                }
                                            })
                                            .always(function() { /*btn.button('reset');*/});
                                });
                            </script>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="trackInEventDeleteDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="trackInEventLabel">Delete a track in event</h4>
                    </div>
                    <div class="modal-body">
                        <c:url value="/admin/racingevents/${racingevent.id}/deletetrack" var="deleteAction"/>
                        <form:form id="deleteracingeventform" action='${deleteAction}'>
                            <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                            <div id="deleteError" style="display: none;" class="alert alert-danger"></div>
                            <div class="row">
                                <div class="col-lg-12"><input id="trackInEvent_id" name="trackInEvent_id" type="hidden" value=""/><span><s:message code="deleting.track-in-event"/></span></div>
                            </div>
                            <div class="row">
                                <h3 class="text-center"><span id="del_trackInEvent" class="text-danger"></span></h3>
                            </div>
                            <div class="row">
                                <div class="col-lg-12"><span><s:message code="really-delete.track-in-event"/>?</span></div>
                            </div>
                        </form:form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                        <button id="trackInEventConfirm" type="button" class="btn-danger btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.delete"/></button>
                    </div>
                    <script type="text/javascript">
                        function prepareToDelete(id, trackInEvent)
                        {
                            $("#del_trackInEvent").text(id);
                            $("#trackInEvent_id").val(trackInEvent);
                            $("#trackInEventDeleteDialog").modal("show");
                        }

                        $('#trackInEventConfirm').click(function () {
                            var btn = $(this);
                            btn.button('loading');
                            $.post( "${deleteAction}",  $('#deleteracingeventform').serialize())
                                    .fail(function(xhr,textstatus,error){$("#deleteError").html(error);$("#deleteError").show();btn.button('reset');})
                                    .success(function(data) {
                                        if(data == "OK")
                                        {
                                            $("#deleteError").html("");
                                            $("#deleteError").hide();
                                            //$('racingEventDeleteDialog').modal('hide')
                                            location.reload();
                                        }
                                        else if(data == "NOTEXISTS")
                                        {
                                            $("#deleteError").html("<s:message code="NotExists.track-in-event" javaScriptEscape="true"/>");
                                            $("#deleteError").show();
                                            btn.button('reset');
                                        }
                                        else
                                        {
                                            $("#deleteError").html(data);
                                            $("#deleteError").show();
                                            btn.button('reset');
                                        }
                                    })
                                    .always(function() { /*btn.button('reset');*/});
                        });
                    </script>
                </div>
            </div>
        </div>
    </body>
</html>
