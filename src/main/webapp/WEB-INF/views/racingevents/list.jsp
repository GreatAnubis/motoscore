<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><s:message code="page.title"/></title>
        <style type="text/css">
            div.row.not-first-margin-top-10 {
                border-bottom: 1px solid black;
                padding-bottom: 2px;
                padding-top: 0px;
            }
            div.row.not-first-margin-top-10:nth-child(odd) {
                background-color: #f9f9f9;
            }
            .not-first-margin-top-10:not(:first-child) {
                margin-top: 0px;
            }
            #racingeventslist .panel .panel-body .row {
                text-align: center;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#datetimepicker').datetimepicker({
                    //format: 'YYYY-MM-DD hh:mm',
                    weekStart: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    //startView: 2,
                    //forceParse: 1,
                    pick12HourFormat: false
                });

                $('.fancybox-media')
                        .attr('rel', 'media-gallery')
                        .fancybox({
                            openEffect : 'none',
                            closeEffect : 'none',
                            prevEffect : 'none',
                            nextEffect : 'none',

                            arrows : false,
                            helpers : {
                                media : {},
                                buttons : {}
                            }
                        });
            });
        </script>
    </head>
    <body role="document">
        <!-- Docs master nav -->
        <div class="container bs-docs-container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1"><button class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#racingEventAddDialog"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add a racing event</button></div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1"><span>&nbsp;</span></div>
            </div>
            <div class="row" id="racingeventslist">
                <div class="col-md-10 col-md-offset-1">
                    <c:choose>
                        <c:when test="${not empty racingevents}">
                            <div class="panel panel-success drop-shadow-down">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span>Racing events list - (${racingevents.size()})</span></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row not-first-margin-top-10">
                                        <div class="col-md-1 margins-top-bottom-10" ><strong>Id</strong></div>
                                        <div class="col-md-3 margins-top-bottom-10"><strong>Event date</strong></div>
                                        <div class="col-md-1 margins-top-bottom-10"><strong>Trasy</strong></div>
                                        <div class="col-md-1 margins-top-bottom-10"><strong>Film</strong></div>
                                        <div class="col-md-1 margins-top-bottom-10"><strong>Widoczny</strong></div>
                                        <div class="col-md-1 margins-top-bottom-10"></div>
                                        <div class="col-md-2 margins-top-bottom-10"><strong><s:message code="button.edit"/></strong></div>
                                        <div class="col-md-2 margins-top-bottom-10"><strong><s:message code="button.delete"/></strong></div>
                                    </div>
                                <c:forEach var="racingevent" items="${racingevents}" varStatus="status">
                                    <div class="row not-first-margin-top-10">
                                        <div class="col-md-1 margins-top-bottom-10">${racingevent.id}</div>
                                        <div class="col-md-3 margins-top-bottom-10"><s:eval expression="racingevent.timestamp"/></div>
                                        <div class="col-md-1 margins-top-bottom-10">
                                            <c:if test="${racingevent.tracks!=null}">
                                                ${racingevent.tracks.size()}
                                            </c:if>
                                        </div>
                                        <div class="col-md-1 margins-top-bottom-10">
                                            <c:if test="${not empty racingevent.movieLink}">
                                                <a class="fancybox-media" href="${racingevent.movieLink}"><span class="glyphicon glyphicon-facetime-video"></span></a>
                                            </c:if>
                                        </div>
                                        <div class="col-md-1 margins-top-bottom-10">
                                            <c:choose>
                                                <c:when test="${racingevent.visible}">
                                                    <span class="glyphicon glyphicon-eye-open" style="color: #47a447"></span>
                                                </c:when>
                                                <c:otherwise>
                                                    <span class="glyphicon glyphicon-eye-close" style="color: red"></span>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                        <div class="col-md-1 margins-top-bottom-10"></div>
                                        <div class="col-md-2 margins-top-bottom-10"><a href='<s:url value="/admin/racingevents/${racingevent.id}/edit"/>' class="btn btn-xs btn-success btn-block"><span class="glyphicon glyphicon-edit"></span>&nbsp;<s:message code="button.edit"/></a></div>
                                        <div class="col-md-2 margins-top-bottom-10"><button onclick="prepareToDelete(${racingevent.id},'<s:eval expression="racingevent.timestamp"/>');return false;" class="btn btn-xs btn-danger btn-block"><span class="glyphicon glyphicon-remove"></span>&nbsp;<s:message code="button.delete"/></button></div>
                                    </div>
                                </c:forEach>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="panel panel-warning drop-shadow-down">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span>Racing events list</span></h3>
                                </div>
                                <div class="panel-body">
                                    No racing events :(
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="racingEventAddDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="addRacinEventLabel">Add a racing event</h4>
                    </div>
                    <form:form id="addracingeventform" onsubmit='$("#addRacingEventConfirm" ).trigger( "click" );return false;' action='' commandName="racingevent" method="post">
                        <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                        <div class="modal-body">
                            <div id="addError" style="display: none;" class="alert alert-danger"></div>
                            <span class="input-group-addon">Racing event date and time</span>
                            <div class="form-group">
                                <div class='input-group input-group-lg date' id='datetimepicker' data-format="yyyy-MM-dd hh:mm">
                                    <form:input path="timestamp" class="form-control" placeholder="Racing event timestamp"  />
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                            <button id="addRacingEventConfirm" type="button" class="btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.add"/></button>
                            <script type="text/javascript">
                                $('#racingEventAddDialog').on('show.bs.modal', function (e) {$("#addError").html("");$("#addError").hide();});
                                $('#addRacingEventConfirm').click(function () {
                                    if($("#timestamp").val().length == 0)
                                    {
                                        $("#addError").html("<s:message code='NotEmpty.racing_event_timestamp' javaScriptEscape='true' />");
                                        $("#addError").show();
                                        return;
                                    }
                                    var btn = $(this);
                                    btn.button('loading');
                                    $.post( "<c:url value="/admin/racingevents/add" />", $('#addracingeventform').serialize())
                                            .fail(function(xhr,textstatus,error){$("#addError").html(error);$("#addError").show();btn.button('reset');})
                                            .success(function(data) {
                                                if(data == "OK")
                                                {
                                                    $("#addError").html("");
                                                    $("#addError").hide();
                                                    //$('#racingEventAddDialog').modal('hide');
                                                    $('#timestamp').val("");
                                                    location.reload();
                                                }
                                                else if(data == "EXISTS")
                                                {
                                                    $("#addError").html("<s:message code="AlreadyExists.racingEvent" javaScriptEscape="true"/>");
                                                    $("#addError").show();
                                                    btn.button('reset');
                                                }
                                                else
                                                {
                                                    $("#addError").html(data);
                                                    $("#addError").show();
                                                    btn.button('reset');
                                                }
                                            })
                                            .always(function() { /*btn.button('reset');*/});
                                });
                            </script>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="racingEventDeleteDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="deleteRacingEventLabel">Delete a racing event</h4>
                    </div>
                    <div class="modal-body">
                        <c:url value="/admin/racingevents/delete" var="deleteAction"/>
                        <form:form id="deleteracingeventform" action='${deleteAction}'>
                            <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                            <div class="row">
                                <div class="col-lg-12"><input id="racing_event_id" name="racing_event_id" type="hidden" value=""/><span><s:message code="deleting.racing-event"/></span></div>
                            </div>
                            <div class="row">
                                <h3 class="text-center"><span id="del_racing_event_timestamp" class="text-danger"></span></h3>
                            </div>
                            <div class="row">
                                <div class="col-lg-12"><span><s:message code="really-delete.racing-event"/>?</span></div>
                            </div>
                        </form:form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                        <button id="deleteRacingEventConfirm" type="button" class="btn-danger btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.delete"/></button>
                    </div>
                    <script type="text/javascript">
                        function prepareToDelete(id, timestamp)
                        {
                            $("#del_racing_event_timestamp").text(timestamp);
                            $("#racing_event_id").val(id);
                            $("#racingEventDeleteDialog").modal("show");
                        }

                        $('#deleteRacingEventConfirm').click(function () {
                            var btn = $(this);
                            btn.button('loading');
                            $.post( "${deleteAction}",  $('#deleteracingeventform').serialize())
                                    .fail(function(xhr,textstatus,error){$("#deleteError").html(error);$("#deleteError").show();btn.button('reset');})
                                    .success(function(data) {
                                        if(data == "OK")
                                        {
                                            $("#deleteError").html("");
                                            $("#deleteError").hide();
                                            //$('racingEventDeleteDialog').modal('hide')
                                            location.reload();
                                        }
                                        else if(data == "NOTEXISTS")
                                        {
                                            $("#deleteError").html("<s:message code="NotExists.racing-event" javaScriptEscape="true"/>");
                                            $("#deleteError").show();
                                            btn.button('reset');
                                        }
                                        else
                                        {
                                            $("#deleteError").html(data);
                                            $("#deleteError").show();
                                            btn.button('reset');
                                        }
                                    })
                                    .always(function() { /*btn.button('reset');*/});
                        });
                    </script>
                </div>
            </div>
        </div>
    </body>
</html>