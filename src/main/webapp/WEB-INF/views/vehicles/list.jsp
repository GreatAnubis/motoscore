<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><s:message code="page.title"/></title>
        <style type="text/css">
            div.row.not-first-margin-top-10 {
                border-bottom: 1px solid black;
                padding-bottom: 2px;
                padding-top: 0px;
            }
            div.row.not-first-margin-top-10:nth-child(odd) {
                background-color: #f9f9f9;
            }
            .not-first-margin-top-10:not(:first-child) {
                margin-top: 0px;
            }
            #vehiclelist .panel .panel-body .row {
                text-align: center;
            }
        </style>
    </head>
    <body role="document">
        <!-- Docs master nav -->
        <div class="container bs-docs-container">
            <div class="row">
                <div class="col-md-12"><button style="float: right;" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#vehicleAddDialog"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add a vehicle</button></div>
            </div>
            <div class="row">
                <div class="col-md-12"><span>&nbsp;</span></div>
            </div>
            <div class="row" id="vehiclelist">
                <div class="col-md-12">
                    <c:choose>
                        <c:when test="${not empty vehicles}">
                            <div class="panel panel-success drop-shadow-down">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span>Vehicle list - (${vehicles.size()})</span></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row not-first-margin-top-10">
                                        <div class="col-md-1 margins-top-bottom-10" ><strong>Id</strong></div>
                                        <div class="col-md-3 margins-top-bottom-10"><strong>Vehicle name</strong></div>
                                        <div class="col-md-2 margins-top-bottom-10"><strong>Vehicle code</strong></div>
                                        <div class="col-md-1 margins-top-bottom-10"><strong>Image</strong></div>
                                        <div class="col-md-1 margins-top-bottom-10"></div>
                                        <div class="col-md-2 margins-top-bottom-10"><strong><s:message code="button.edit"/></strong></div>
                                        <div class="col-md-2 margins-top-bottom-10"><strong><s:message code="button.delete"/></strong></div>
                                    </div>
                                <c:forEach var="vehicle" items="${vehicles}" varStatus="status">
                                    <div class="row not-first-margin-top-10">
                                        <div class="col-md-1 margins-top-bottom-10">${vehicle.id}</div>
                                        <div class="col-md-3 margins-top-bottom-10">${vehicle.vehicle_name}</div>
                                        <div class="col-md-2 margins-top-bottom-10">
                                            <c:if test="${vehicle.vehicle_code != null}">
                                                ${vehicle.vehicle_code}
                                            </c:if>
                                        </div>
                                        <div class="col-md-1 margins-top-bottom-2" style="margin-top: 7px;">
                                            <c:if test="${vehicle.vehicle_code != null}">
                                                <img src="<s:url value="/resources/images/vehicles/icons/${vehicle.vehicle_code}.png" />" width="50px" height="30px" />
                                            </c:if>
                                        </div>
                                        <div class="col-md-1 margins-top-bottom-10"></div>
                                        <div class="col-md-2 margins-top-bottom-10"><button onclick="prepareToEdit(${vehicle.id},'${vehicle.vehicle_name}','${vehicle.vehicle_code}');return false;" class="btn btn-xs btn-success btn-block"><span class="glyphicon glyphicon-edit"></span>&nbsp;<s:message code="button.edit"/></button></div>
                                        <div class="col-md-2 margins-top-bottom-10"><button onclick="prepareToDelete(${vehicle.id},'${vehicle.vehicle_name}');return false;" class="btn btn-xs btn-danger btn-block"><span class="glyphicon glyphicon-remove"></span>&nbsp;<s:message code="button.delete"/></button></div>
                                    </div>
                                </c:forEach>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="panel panel-warning drop-shadow-down">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span>Vehicle list</span></h3>
                                </div>
                                <div class="panel-body">
                                    No vehicles :(
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="vehicleAddDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="addVehicleLabel">Add a vehicle</h4>
                    </div>
                    <s:url value="/admin/vehicles/add" var="addaction"/>
                    <form:form id="addvehicleform" onsubmit='$("#addVehicleConfirm" ).trigger( "click" );return false;' action='${addaction}' commandName="vehicle" method="post">
                        <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                        <div class="modal-body">
                            <div id="addError" style="display: none;" class="alert alert-danger"></div>
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon">Vehicle name</span>
                                    <form:input path="vehicle_name" type="text" class="form-control" placeholder="Vehicle name" maxlength="64"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon">Vehicle code</span>
                                    <form:input path="vehicle_code" type="text" class="form-control" placeholder="Vehicle code" maxlength="64"/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                            <button id="addVehicleConfirm" type="button" class="btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.add"/></button>
                            <script type="text/javascript">
                                $('#vehicleAddDialog').on('show.bs.modal', function (e) {$("#addError").html("");$("#addError").hide();});
                                $('#addVehicleConfirm').click(function () {
                                    if($("#vehicle_name").val().length == 0)
                                    {
                                        $("#addError").html("<s:message code='NotEmpty.vehicle_name' javaScriptEscape='true' />");
                                        $("#addError").show();
                                        return;
                                    }
                                    var btn = $(this);
                                    btn.button('loading');
                                    $.post( "<s:url value="/admin/vehicles/add" />", $('#addvehicleform').serialize())
                                            .fail(function(xhr,textstatus,error){$("#addError").html(error);$("#addError").show();})
                                            .success(function(data) {
                                                if(data == "OK")
                                                {
                                                    $("#addError").html("");
                                                    $("#addError").hide();
                                                    //$('#vehicleAddDialog').modal('hide');
                                                    $('#vehicle_name').val("");
                                                    location.reload();
                                                }
                                                else if(data == "EXISTS")
                                                {
                                                    $("#addError").html("<s:message code="AlreadyExists.vehicle" javaScriptEscape="true"/>");
                                                    $("#addError").show();
                                                    btn.button('reset');
                                                }
                                                else
                                                {
                                                    $("#addError").html(data);
                                                    $("#addError").show();
                                                    btn.button('reset');
                                                }
                                            })
                                            .always(function() { /*btn.button('reset');*/});
                                });
                            </script>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="vehicleEditDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="editVehicleLabel">Edit a vehicle</h4>
                    </div>
                    <s:url value="/admin/vehicles/edit" var="editaction"/>
                    <form:form id="editvehicleform" onsubmit='$("#editVehicleConfirm" ).trigger( "click" );return false;' action='${editaction}' commandName="vehicle" method="post">
                        <div class="modal-body">
                            <div id="editError" style="display: none;" class="alert alert-danger"></div>
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon">Vehicle name</span>
                                    <form:input path="vehicle_name" type="text" class="form-control" placeholder="Vehicle name" maxlength="64"/>
                                    <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                                    <input id="edit_vehicle_id" name="id" type="hidden" value=""/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon">Vehicle code</span>
                                    <form:input path="vehicle_code" type="text" class="form-control" placeholder="Vehicle code" maxlength="32"/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                            <button id="editVehicleConfirm" type="button" class="btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.edit"/></button>
                            <script type="text/javascript">
                                $('#vehicleEditDialog').on('show.bs.modal', function (e) {$("#editError").html("");$("#editError").hide();$('#editVehicleConfirm').button('reset');});
                                function prepareToEdit(id, vehicle, code)
                                {
                                    $("#editvehicleform #vehicle_name").val(vehicle);
                                    $("#edit_vehicle_id").val(id);
                                    $("#editvehicleform #vehicle_code").val(code);
                                    $("#vehicleEditDialog").modal("show");
                                }
                                $('#editVehicleConfirm').click(function () {
                                    if($("#editvehicleform #vehicle_name").val().length == 0)
                                    {
                                        $("#editError").html("<s:message code='NotEmpty.vehicle_name' javaScriptEscape='true' />");
                                        $("#editError").show();
                                        return;
                                    }
                                    var btn = $(this);
                                    btn.button('loading');
                                    $.post( "${editaction}", $('#editvehicleform').serialize())
                                            .fail(function(xhr,textstatus,error){$("#editError").html(error);$("#editError").show();})
                                            .success(function(data) {
                                                if(data == "OK")
                                                {
                                                    $("#editError").html("");
                                                    $("#editError").hide();
                                                    $('#editvehicleform #vehicle_name').val("");
                                                    location.reload();
                                                }
                                                else if(data == "EXISTS")
                                                {
                                                    $("#editError").html("<s:message code="AlreadyExists.vehicle" javaScriptEscape="true"/>");
                                                    $("#editError").show();
                                                    btn.button('reset');
                                                }
                                                else
                                                {
                                                    $("#editError").html(data);
                                                    $("#editError").show();
                                                    btn.button('reset');
                                                }
                                            })
                                            .always(function() { /*btn.button('reset');*/});
                                });
                            </script>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="vehicleDeleteDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="deleteVehicleLabel">Delete a vehicle</h4>
                    </div>
                    <div class="modal-body">
                        <div id="deleteError" style="display: none;" class="alert alert-danger"></div>
                        <s:url value="/admin/vehicles/delete" var="delaction"/>
                        <form:form id="deletevehicleform" action='${delaction}'>
                            <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                            <div class="row">
                                <div class="col-lg-12"><input id="vehicle_id" name="vehicle_id" type="hidden" value=""/><span><s:message code="deleting.vehicle-name"/></span></div>
                            </div>
                            <div class="row">
                                <h3 class="text-center"><span id="del_vehicle_name" class="text-danger"></span></h3>
                            </div>
                            <div class="row">
                                <div class="col-lg-12"><span><s:message code="really-delete.vehicle"/>?</span></div>
                            </div>
                        </form:form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                        <button id="deleteVehicleConfirm" type="button" class="btn-danger btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.delete"/></button>
                    </div>
                    <script type="text/javascript">
                        function prepareToDelete(id, name)
                        {
                            $("#del_vehicle_name").text(name);
                            $("#vehicle_id").val(id);
                            $("#vehicleDeleteDialog").modal("show");
                        }

                        $('#deleteVehicleConfirm').click(function () {
                            var btn = $(this);
                            btn.button('loading');
                            $.post( "${delaction}",  $('#deletevehicleform').serialize())
                                    .fail(function(xhr,textstatus,error){$("#deleteError").html(error);$("#deleteError").show();btn.button('reset');})
                                    .success(function(data) {
                                        if(data == "OK")
                                        {
                                            $("#deleteError").html("");
                                            $("#deleteError").hide();
                                            //$('#vehicleDeleteDialog').modal('hide')
                                            location.reload();
                                        }
                                        else if(data == "NOTEXISTS")
                                        {
                                            $("#deleteError").html("<s:message code="NotExists.vehicle" javaScriptEscape="true"/>");
                                            $("#deleteError").show();
                                            btn.button('reset');
                                        }
                                        else
                                        {
                                            $("#deleteError").html(data);
                                            $("#deleteError").show();
                                            btn.button('reset');
                                        }
                                    })
                                    .always(function() { /*btn.button('reset');*/});
                        });
                    </script>
                </div>
            </div>
        </div>
    </body>
</html>