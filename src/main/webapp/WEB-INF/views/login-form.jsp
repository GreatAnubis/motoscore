<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><s:message code="page.title"/></title>
        <style type="text/css">
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #eee;
            }

            .form-signin {
                max-width: 330px;
                padding: 15px;
                margin: 0 auto;
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            .form-signin .checkbox {
                font-weight: normal;
            }
            .form-signin .form-control {
                position: relative;
                height: auto;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                padding: 10px;
                font-size: 16px;
            }
            .form-signin .form-control:focus {
                z-index: 2;
            }
            .form-signin input[type="email"] {
                margin-bottom: -1px;
                border-bottom-right-radius: 0;
                border-bottom-left-radius: 0;
            }
            .form-signin input[type="password"] {
                margin-bottom: 10px;
                border-top-left-radius: 0;
                border-top-right-radius: 0;
            }
        </style>
        <script type="text/javascript">
            $(function() {
            $("#login").remove();
            $("#loginDialog").remove();
            });

            function delay() {
                $("#signin").button('loading');
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="well" style="border-radius: 30px;margin-top: 20px;">
                        <form method="post" onsubmit="delay()" action="<s:url value='/j_spring_security_check'/>" class="form-signin" role="form">
                            <legend><h2 class="form-signin-heading">Please sign in</h2></legend>
                            <c:if test="${error==true}">
                            <div class="form-group">
                                <div id="addErrorLogin" class="alert alert-danger">Invalid login or password</div>
                            </div>
                            </c:if>
                            <div class="form-group">
                            	<input class="form-control" type="text" name="username" id="username"  maxlength="32" placeholder="Username" required autofocus/>
                            </div>
                            <div class="form-group">
                            	<input type="password" class="form-control" name="password" placeholder="Password"  maxlength="32" required>
                            </div>
                            <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                            <div class="form-group">
                            	<button id="signin"  data-loading-text="Please wait..."  class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> <!-- /container -->
    </body>
</html>