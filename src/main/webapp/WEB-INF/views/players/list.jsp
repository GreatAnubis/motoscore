<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><s:message code="page.title"/></title>
        <style type="text/css">
            div.row.not-first-margin-top-10 {
                border-bottom: 1px solid black;
                padding-bottom: 2px;
                padding-top: 0px;
            }
            div.row.not-first-margin-top-10:nth-child(odd) {
                background-color: #f9f9f9;
            }
            .not-first-margin-top-10:not(:first-child) {
                margin-top: 0px;
            }
            #playerList .panel .panel-body .row {
                text-align: center;
            }
            #playerList .panel .panel-body button {
                width: 100%;
            }
        </style>
    </head>
    <body role="document">
        <!-- Docs master nav -->
        <div class="container bs-docs-container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2"><button style="float: right;" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#playerAddDialog"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add a player</button></div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2"><span>&nbsp;</span></div>
            </div>
            <div class="row" id="playerList">
                <div class="col-md-8 col-md-offset-2">
                    <c:choose>
                        <c:when test="${not empty players}">
                            <div class="panel panel-success drop-shadow-down">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span>Player list - (${players.size()})</span></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row not-first-margin-top-10">
                                        <div class="col-md-1 margins-top-bottom-10"><strong>Id</strong></div>
                                        <div class="col-md-4 margins-top-bottom-10"><strong>Nick</strong></div>
                                        <div class="col-md-3"></div>
                                        <div class="col-md-2 margins-top-bottom-10"><strong><s:message code="button.edit"/></strong></div>
                                        <div class="col-md-2 margins-top-bottom-10"><strong><s:message code="button.delete"/></strong></div>
                                    </div>
                                <c:forEach var="player" items="${players}" varStatus="status">
                                    <div class="row not-first-margin-top-10">
                                        <div class="col-md-1 margins-top-bottom-2">${player.id}</div>
                                        <div class="col-md-4 margins-top-bottom-2">${player.player_nick}</div>
                                        <div class="col-md-3 margins-top-bottom-2"></div>
                                        <div class="col-md-2 margins-top-bottom-2"><button onclick="prepareToEdit(${player.id},'${player.player_nick}');return false;" class="btn btn-xs btn-success pull-right"><span class="glyphicon glyphicon-edit"></span>&nbsp;<s:message code="button.edit"/></button></div>
                                        <div class="col-md-2 margins-top-bottom-2"><button onclick="prepareToDelete(${player.id},'${player.player_nick}');return false;" class="btn btn-xs btn-danger pull-right"><span class="glyphicon glyphicon-remove"></span>&nbsp;<s:message code="button.delete"/></button></div>
                                    </div>
                                </c:forEach>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="panel panel-warning drop-shadow-down">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span>Player list</span></h3>
                                </div>
                                <div class="panel-body">
                                    No players :(
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="playerEditDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="editPlayerLabel">Edit a player</h4>
                    </div>
                    <s:url value="/admin/players/edit" var="editaction"/>
                    <form:form id="editplayerform" action="${editaction}" onsubmit='$("#editPlayerConfirm" ).trigger( "click" );return false;' commandName="player" method="post">
                        <div class="modal-body">
                            <div id="addErrorEdit" style="display: none;" class="alert alert-danger"></div>
                            <div class="input-group input-group-lg">
                                <span class="input-group-addon">Player nick</span>
                                <form:input path="player_nick" type="text" class="form-control" placeholder="Player nick" maxlength="32"/>
                                <input id="edit_player_id" name="id" type="hidden" value=""/>
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                            <button id="editPlayerConfirm" type="button" class="btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.edit"/></button>
                            <script type="text/javascript">
                                $('#playerEditDialog').on('show.bs.modal', function (e) {$("#addErrorEdit").html("");$("#addErrorEdit").hide();$('#editPlayerConfirm').button('reset');});
                                function prepareToEdit(id, nick)
                                {
                                    $("#playerEditDialog #player_nick").val(nick);
                                    $("#edit_player_id").val(id);
                                    $("#playerEditDialog").modal("show");
                                }
                                $('#editPlayerConfirm').click(function () {
                                    if($("#player_nick").val().length == 0)
                                    {
                                        $("#addErrorEdit").html("<s:message code='NotEmpty.player_nick' javaScriptEscape='true' />");
                                        $("#addErrorEdit").show();
                                        return;
                                    }
                                    var btn = $(this);
                                    btn.button('loading');
                                    $.post( "${editaction}", $('#editplayerform').serialize())
                                            .fail(function(xhr,textstatus,error){$("#addErrorEdit").html(error);$("#addErrorEdit").show();btn.button('reset');})
                                            .success(function(data) {
                                                if(data == "OK")
                                                {
                                                    $("#addErrorEdit").html("");
                                                    $("#addErrorEdit").hide();
                                                    $('#editplayerform #player_nick').val("");
                                                    location.reload();
                                                }
                                                else if(data == "EXISTS")
                                                {
                                                    $("#addErrorEdit").html("<s:message code="AlreadyExists.player" javaScriptEscape="true"/>");
                                                    $("#addErrorEdit").show();
                                                    btn.button('reset');
                                                }
                                                else
                                                {
                                                    $("#addErrorEdit").html(data);
                                                    $("#addErrorEdit").show();
                                                    btn.button('reset');
                                                }
                                            })
                                            .always(function() { /*btn.button('reset');*/});
                                });
                            </script>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="playerAddDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="addPlayerLabel">Add a player</h4>
                    </div>
                    <form:form id="addplayerform" onsubmit='$("#addPlayerConfirm" ).trigger( "click" );return false;' commandName="player" method="post">
                        <div class="modal-body">
                            <div id="addErrorAdd" style="display: none;" class="alert alert-danger"></div>
                            <div class="input-group input-group-lg">
                                <span class="input-group-addon">Player nick</span>
                                <form:input path="player_nick" type="text" class="form-control" placeholder="Username" maxlength="32"/>
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                            <button id="addPlayerConfirm" type="button" class="btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.add"/></button>
                            <script type="text/javascript">
                                $('#playerAddDialog').on('show.bs.modal', function (e) {$("#addError").html("");$("#addError").hide();});
                                $('#addPlayerConfirm').click(function () {
                                    if($("#addplayerform #player_nick").val().length == 0)
                                    {
                                        $("#addErrorAdd").html("<s:message code='NotEmpty.player_nick' javaScriptEscape='true' />");
                                        $("#addErrorAdd").show();
                                        return;
                                    }
                                    var btn = $(this);
                                    btn.button('loading');
                                    $.post( "<c:url value="/admin/players/add" />", $('#addplayerform').serialize())
                                            .fail(function(xhr,textstatus,error){$("#addErrorAdd").html(error);$("#addErrorAdd").show();btn.button('reset');btn.button('reset');})
                                            .success(function(data) {
                                                if(data == "OK")
                                                {
                                                    $("#addErrorAdd").html("");
                                                    $("#addErrorAdd").hide();
                                                    //$('#playerAddDialog').modal('hide');
                                                    $('#addplayerform #player_nick').val("");
                                                    location.reload();
                                                }
                                                else if(data == "EXISTS")
                                                {
                                                    $("#addErrorAdd").html("<s:message code="AlreadyExists.player" javaScriptEscape="true"/>");
                                                    $("#addErrorAdd").show();
                                                }
                                                else
                                                {
                                                    $("#addErrorAdd").html(data);
                                                    $("#addErrorAdd").show();
                                                }
                                            })
                                            .always(function() { /*btn.button('reset');*/});
                                });
                            </script>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="playerDeleteDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="deletePlayerLabel">Delete a player</h4>
                    </div>
                    <div class="modal-body">
                        <div id="deleteError" style="display: none;" class="alert alert-danger"></div>
                        <form:form id="deleteplayerform" action='<c:url value="/admin/players/delete" />'>
                            <div class="row">
                                <div class="col-lg-12">
                                    <input id="del_player_id" name="player_id" type="hidden" value=""/><span><s:message code="deleting.player-nick"/></span>
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                </div>
                            </div>
                            <div class="row">
                                <h3 class="text-center"><span id="del_player_nick" class="text-danger"></span></h3>
                            </div>
                            <div class="row">
                                <div class="col-lg-12"><span><s:message code="really-delete.player"/>?</span></div>
                            </div>
                        </form:form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><s:message code="button.cancel"/></button>
                        <button id="deletePlayerConfirm" type="button" class="btn-danger btn btn-primary" data-loading-text="Please wait..." ><s:message code="button.delete"/></button>
                    </div>
                    <script type="text/javascript">
                        function prepareToDelete(id, nick)
                        {
                            $("#del_player_nick").text(nick);
                            $("#del_player_id").val(id);
                            $("#playerDeleteDialog").modal("show");
                        }

                        $('#deletePlayerConfirm').click(function () {
                            var btn = $(this);
                            btn.button('loading');
                            $.post( "<c:url value="/admin/players/delete" />",  $('#deleteplayerform').serialize())
                                    .fail(function(xhr,textstatus,error){$("#deleteError").html(error);$("#deleteError").show();btn.button('reset');btn.button('reset');})
                                    .success(function(data) {
                                        if(data == "OK")
                                        {
                                            $("#deleteError").html("");
                                            $("#deleteError").hide();
                                            //$('#playerDeleteDialog').modal('hide')
                                            location.reload();
                                        }
                                        else if(data == "NOTEXISTS")
                                        {
                                            $("#deleteError").html("<s:message code="NotExists.player" javaScriptEscape="true"/>");
                                            $("#deleteError").show();
                                        }
                                        else
                                        {
                                            $("#deleteError").html(data);
                                            $("#deleteError").show();
                                        }
                                    })
                                    .always(function() { /*btn.button('reset');*/});
                        });
                    </script>
                </div>
            </div>
        </div>
    </body>
</html>