<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<security:authorize ifAnyGranted="ROLE_MODERATOR">
<div class="row" id="printing_area" style="display: none;">
    <div class="col-sm-12">
        <div class="panel panel-success img-out"></div>
    </div>
</div>
</security:authorize>
<c:choose>
    <c:when test="${not empty racingevents}">
        <c:forEach var="racingevent" items="${racingevents}" varStatus="status">
            <div class="row" >
                <div class="col-sm-12">
                    <div class="panel panel-success drop-shadow-down" data-print="print_canvas">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="glyphicon glyphicon-eye-open" style="cursor: cell;" data-event-visible="0"></span>&nbsp;<span>Event: <s:eval expression="racingevent.timestamp"/><c:if test="${not empty racingevent.movieLink}"><span><a class="fancybox-media" href="${racingevent.movieLink}" style="float: right;">Film</a></span></c:if></span><security:authorize ifAnyGranted="ROLE_MODERATOR"><input type="button" class="btnSave content-score" value="Save PNG"/></security:authorize></h3>
                        </div>
                        <div class="panel-body content-score">
                            <div class="table-responsive">
                                <c:choose>
                                    <c:when test="${not empty racingevent.tracks}">
                                        <table class="table table-striped small">
                                            <thead>
                                            <tr>
                                                <th style="text-align: center;vertical-align: bottom;">Gracz</th>
                                                <c:forEach var="trackInEvent" items="${racingevent.tracks}" varStatus="statusTracksInEvent">
                                                    <th style="line-height: 20px;text-align: center;vertical-align: middle;">
                                                        <c:choose>
                                                            <c:when test="${trackInEvent.track.id == 1}">
                                                                <div>${trackInEvent.track.track_name}&nbsp;[${statusTracksInEvent.index+1}]</div>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:choose>
                                                                    <c:when test="${trackInEvent.track.track_code != null}">
                                                                        <div class="popver" rel="popover" data-image="<s:url value="/resources/images/maps/${trackInEvent.track.track_code}.png" />">${trackInEvent.track.track_name}</div>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <div>${trackInEvent.track.track_name}</div>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </th>
                                                </c:forEach>
                                                <th style="line-height: 20px;text-align: center;vertical-align: bottom;">Suma punktów</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:set var="tableRacingEventRow" value="${resultMap[status.index]}" scope="page"/>
                                            <c:set var="vehicleRacingInfo" value="${vehRacingEvents[status.index]}" scope="page"/>
                                            <tr>
                                                <td></td>
                                                <c:forEach var="trackInEvent" items="${racingevent.tracks}" varStatus="statusTracksInEvent">
                                                <td>
                                                     <c:forEach var="vehicle" items="${vehicleRacingInfo.vehicles[trackInEvent.id]}" varStatus="statustableRacingEventRow">
                                                        <div style="width: 100%;text-align: center;vertical-align: middle;"
                                                            <c:choose>
                                                                <c:when test="${vehicle.vehicle_code != null}">
                                                            class="popver" rel="popover" data-image="<s:url value="/resources/images/vehicles/big/${vehicle.vehicle_code}.png" />">
                                                                    <img src="<s:url value="/resources/images/vehicles/icons/${vehicle.vehicle_code}.png"  />" width="50px" height="30px" title="${vehicle.vehicle_name}"/>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    >
                                                                </c:otherwise>
                                                            </c:choose>

                                                        </div>
                                                     </c:forEach>
                                                     </td>
                                                 </c:forEach>
                                                <td></td>
                                            </tr>
                                            <c:forEach var="players" items="${tableRacingEventRow}" varStatus="statustableRacingEventRow">
                                                <tr>
                                                    <td style="font-weight: bold;">${playerdata[players.id].player_nick}</td>
                                                    <c:forEach var="trackInEvent" items="${racingevent.tracks}" varStatus="statusTracksInEvent">
                                                        <c:choose>
                                                            <c:when test="${not empty players.playerScores[trackInEvent.id]}">
                                                                <td style="text-align: center;">${players.playerScores[trackInEvent.id]}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td style="text-align: center;">-</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                    <td class="alert alert-success" style="text-align: center;"><strong>${players.totalScore}</strong></td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </c:when>
                                    <c:otherwise>
                                        No scores defined :(
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>
    </c:when>
</c:choose>

