<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href='<c:url value="/resources/bower_components/bootstrap/dist/css/bootstrap.min.css"/>' >
        <link rel="stylesheet" href="<c:url value="/resources/css/offcanvas.css"/>">
        <!-- Optional theme -->
        <link rel="stylesheet" href='<c:url value="/resources/bower_components/bootstrap/dist/css/bootstrap-theme.min.css"/>'>

        <link rel="stylesheet" href='<c:url value="/resources/css/moto.css"/>'>
        <link rel="stylesheet" rel="stylesheet" media="screen" href='<c:url value="/resources/bootstrap-3.1.1/dist/css/bootstrap-datetimepicker.min.css"/>'>

        <!-- Fancy Box -->
        <link rel="stylesheet" href='<c:url value="/resources/bower_components/font-awesome/css/font-awesome.min.css"/>'>>
        <link rel="stylesheet" href='<c:url value="/resources/plugins/fancybox/jquery.fancybox.css?v=2.1.5"/>'>
        <link rel="stylesheet" href='<c:url value="/resources/plugins/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5"/>'>
        <link rel="stylesheet" href='<c:url value="/resources/plugins/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7"/>'>
        <link rel="stylesheet" href='<c:url value="/resources/plugins/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7"/>'>

        <link rel="shortcut icon" type="image/png" href='<c:url value="/resources/ico/favicon.png"/>'/>
        <!-- Latest compiled and minified JavaScript -->
        <script type="text/javascript" src='<c:url value="/resources/bower_components/jquery/dist/jquery.min.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/bower_components/jquery-ui/jquery-ui.min.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/bootstrap-3.1.1/dist/js/bindWithDelay.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/bootstrap-3.1.1/dist/js/bootstrap-datetimepicker.js"/>' charset="UTF-8"></script>
        <script type="text/javascript" src='<c:url value="/resources/bootstrap-3.1.1/dist/js/locales/bootstrap-datetimepicker.pl.js"/>' charset="UTF-8"></script>
        <!-- Off Canvas -->
        <script type="text/javascript" src='<c:url value="/resources/bootstrap-3.1.1/dist/js/offcanvas.js"/>'></script>
        <!-- Fancy Box -->
        <script type="text/javascript" src='<c:url value="/resources/plugins/fancybox/jquery.fancybox.js?v=2.1.5"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/plugins/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/plugins/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/plugins/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"/>'></script>


        <title><sitemesh:write property='title'/></title>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src='<c:url value="/resources/js/html5shiv/html5shiv.js"/>'></script>
        <script src='<c:url value="/resources/js/respond/respond.min.js"/>'></script>
        <![endif]-->

        <security:authorize ifAnyGranted="ROLE_MODERATOR">
            <script type="text/javascript" src='<c:url value="/resources/scripts/base64.js"/>'></script>
            <script type="text/javascript" src='<c:url value="/resources/scripts/html2canvas.js"/>'></script>
            <script type="text/javascript" src='<c:url value="/resources/scripts/canvas2image.js"/>'></script>
            <script type="text/javascript" src='<c:url value="/resources/scripts/canvas2image.js"/>'></script>
        </security:authorize>

        <script type="text/javascript">
            $(function() {
                $('.navbar-nav li a[href="' + this.location.pathname + '"]').parent().addClass('active');
                var paramVal = window.location.search.substr(1);
                if(paramVal.match("^loginerror")){$("#addErrorLogin").html('<span style="color: red;"><s:message code="invalid.login" javaScriptEscape='true' /></span>');$("#addErrorLogin").show();}
                if(paramVal.match("^login")){$('#loginDialog').modal('show');}
                $("#login").on("click", function(e){
                    e.preventDefault();
                    $("#loginDialog").modal("show");
                }
                );
            });
        </script>
        <sitemesh:write property='head'/>
    </head>
<body>
    <%@ include file="../views/include/navigation.jsp" %>
    <sitemesh:write property='body'/>

    <security:authorize ifAnyGranted="ROLE_ANONYMOUS">
        <!-- Modal -->
        <div class="modal fade" id="loginDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h1 class="text-center">Login</h1>
                    </div>
                    <div class="modal-body">
                        <s:url value='/j_spring_security_check' var="action" />
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="addErrorLogin" style="display: none;" class="alert alert-danger"></div>
                            </div>
                        </div>
                        <form method="post" id="loginform" action='${action}' class="form form-signin col-md-12 center-block" role="form">
                            <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
                            <div class="form-group">
                                <input id="username" name="username" type="text" class="form-control input-lg" placeholder="Username" maxlength="10" value="">
                            </div>
                            <div class="form-group">
                                <input id="password" name="password" type="password" class="form-control input-lg" placeholder="Password" maxlength="15" value="" >
                            </div>
                            <div class="form-group">
                                <button id="loginConfirm" type="button" class="btn btn-primary btn-lg btn-block" data-loading-text="Please wait..." ><s:message code="button.login"/></button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="border-top: 0px;">
                        <div class="col-md-12">
                            <button class="btn" data-dismiss="modal" aria-hidden="true" >Cancel</button>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $('#loginConfirm').click(function () {
                            var btn = $(this);
                            btn.button('loading');
                            $.post( "${action}",  $('#loginform').serialize())
                            .success(function(data, textStatus, jqXHR){
                                        if(jqXHR.getResponseHeader("TM-finalURL"))
                                        {
                                            window.location = jqXHR.getResponseHeader("TM-finalURL");
                                        }
                                        else
                                        {
                                            location.reload();
                                        }

                                    })
                            .fail(function(data, textStatus, jqXHR){
                                        location.reload();
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </security:authorize>
</body>
</html>
