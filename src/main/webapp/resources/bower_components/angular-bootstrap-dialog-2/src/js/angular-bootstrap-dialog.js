(function(){
    'use strict';
    /**
     * Module
     */
    angular.module( 'angularBootstrapDialog', [ ] );
    /**
     * Defaults
     */
    angular.module( 'angularBootstrapDialog' )
        .value( 'angularBootstrapDialogDefaults', {
            modal   : true,
            closable: true,
            movable : false
        } );

    angular.module( 'angularBootstrapDialog' )
        .value( 'angularBootstrapDialogMemoryValues', {
            _memory: {
                open: false
            }
        } );

    angular.module( 'angularBootstrapDialog' )
        .factory( 'abDialog', function( angularBootstrapDialogDefaults, angularBootstrapDialogMemoryValues, $document, $window, $timeout ){

            var abDialog = function( params ){
                var data = {};
                angular.extend( data, angularBootstrapDialogDefaults );
                angular.extend( data, params );
                angular.extend( data, angularBootstrapDialogMemoryValues );
                angular.copy( data, this );
            };

            abDialog.prototype._init = function( element ){
                var my = this;
                my.element = element;


                my.element
                    .addClass( 'hidden out' );

                my.duration = this.element.hasClass( 'fast' ) ? 800 : this.element.hasClass( 'slow' ) ? 2000 : 1000;

                my.closable && $document.on( 'keyup', function( e ){
                    if( e.keyCode === 27 ){
                        if( my.element.hasClass( 'open' ) ){
                            my.close();
                        }
                    }
                } );

                my.closable && my.element.find( '.close' ).on( 'click', function(){
                    my.close();
                } );

                my.movable && my._initMovePosition();

                my._memory.open && my.open();

            };

            abDialog.prototype.open = function(){
                var my = this;

                if( my.element ){
                    // removes memory commands
                    if( my.hasOwnProperty( '_memory' ) ){
                        delete my._memory;
                    }
                    // Resets the inner pos
                    var $inner = my.element.find( '.inner' );
                    if( $inner.offset().top !== 85 && $inner.offset().left !== 0 ){
                        $inner.offset( {
                            top : 85,
                            left: $inner.offset().left
                        } );
                    }
                    // Reset move pos
                    my.movable && my._resetMovePos();
                    // Modal
                    my.modal && my._showBackdrop();
                    // Opening
                    my.element
                        .removeClass( 'out hidden' )
                        .addClass( 'in opening' );
                    // Opened
                    $timeout( function(){
                        my.movable && my._setMoveEvent();
                        my.element
                            .removeClass( 'opening' )
                            .addClass( 'open' );

                    }, my.duration );

                }else{
                    my._memory.open = true;
                }
            };

            abDialog.prototype.close = function(){
                var my = this;

                if( my.element ){
                    // Kill Movable
                    my.movable && my._killMoveEvent();
                    // Modal
                    this.modal && my._hideBackdrop();
                    // Closing
                    my.element
                        .removeClass( 'open' )
                        .addClass( 'out closing' );
                    // Closed
                    $timeout( function(){
                        my.element
                            .removeClass( 'in closing' )
                            .addClass( 'hidden' );
                    }, my.duration );
                }
            };

            abDialog.prototype._getBackdrop = function(){
                var b = $document.find( 'body div.ab-dialog-backdrop' );
                return ( b.length === 0 ) ? false : b;
            };

            abDialog.prototype._showBackdrop = function(){
                var speed = this.element.hasClass( 'fast' ) ? 'fast' : this.element.hasClass( 'slow' ) ? 'slow' : '';
                if( !this._getBackdrop() ){
                    $document.find( 'body' )
                        .addClass( 'no-scrolling' )
                        .append( '<div class="ab-dialog-backdrop ' + speed + ' in"></div>' );
                }else{
                    !this._getBackdrop().hasClass( 'in' ) && this._getBackdrop().removeClass( 'slow fast out' ).addClass( speed + ' in' );
                }
            };

            abDialog.prototype._hideBackdrop = function(){
                var my = this;
                var $BackDrop = this._getBackdrop();
                if( $BackDrop ){
                    $BackDrop
                        .addClass( 'out' )
                        .removeClass( 'in' );
                    $timeout( function(){
                        $BackDrop.remove();
                        $document.find( 'body' )
                            .removeClass( 'no-scrolling' );
                    }, my.duration );
                }
            };

            abDialog.prototype._initMovePosition = function(){
                var my = this;
                my.element.addClass( 'moveable' );

                my._move = {};
                my._move.startX = parseInt( my.element.css( 'margin-left' ), 10 );
                my._move.startY = parseInt( my.element.css( 'top' ), 10 );
                my._move.x = my._move.startX;
                my._move.y = my._move.startY;
                my._move.width = my.element.width();
            };

            abDialog.prototype._killMoveEvent = function(){
                this.element.find( '.header' ).off( 'mousedown' );
            };

            abDialog.prototype._resetMovePos = function(){
                var my = this;
                my.element.css( 'margin-left', my._move.startX + 'px' );
                my.element.css( 'top', my._move.startY + 'px' );
            };

            abDialog.prototype._setMoveEvent = function(){
                var my = this;
                var opt = angular.copy( my._move );
                var screenHeight, screenWidth, xLeftDangerZone, xRightDangerZone, yTopDangerZone, yBottomDangerZone;

                // On mousemove
                function mousemove( event ){
                    opt.y = event.pageY - opt.startY;
                    opt.x = event.pageX - opt.startX;
                    my.element.css( {
                        top       : opt.y + 'px',
                        marginLeft: opt.x + 'px'
                    } );
                }

                // On mouseup
                function mouseup(){
                    $document.off( 'mousemove', mousemove );
                    $document.off( 'mouseup', mouseup );

                    // Checks the X danger zone
                    if( xLeftDangerZone > opt.x || opt.x > xRightDangerZone ){
                        opt.x = ( xLeftDangerZone > opt.x ) ? xLeftDangerZone : xRightDangerZone;
                        my.element.css( {
                            marginLeft: opt.x + 'px'
                        } );
                    }

                    // Checks the Y danger zone
                    if( yTopDangerZone > opt.y || opt.y > yBottomDangerZone ){
                        opt.y = ( yTopDangerZone > opt.y ) ? yTopDangerZone : yBottomDangerZone;
                        my.element.css( {
                            top: opt.y + 'px'
                        } );
                    }
                }

                // Sets the event on the title of the dialog
                my.element.find( '.header' ).on( 'mousedown', function( event ){
                    // Prevent default dragging of selected content
                    event.preventDefault();

                    // Gets the start position
                    opt.startX = event.pageX - opt.x;
                    opt.startY = event.pageY - opt.y;

                    // Gets the screen sizing
                    screenHeight = $window.innerHeight;
                    screenWidth = $window.innerWidth;

                    // Danger-zones
                    xLeftDangerZone = ( ( screenWidth / 2 ) - 15 ) * ( -1 );
                    xRightDangerZone = ( opt.width - ( ( screenWidth / 2 ) - 15 ) ) * ( -1 );
                    yTopDangerZone = 15;
                    yBottomDangerZone = screenHeight - my.element.height() - 15;

                    $document.on( 'mousemove', mousemove );
                    $document.on( 'mouseup', mouseup );
                } );
            };


            return abDialog;
        } );
    /**
     * Directive
     */
    angular.module( 'angularBootstrapDialog' )
        .directive( 'abDialog', function(){
            return {
                restrict  : 'A',
                priority  : 1001,
                scope     : {
                    dialog: '=abDialog'
                },
                template  : '<div class="ab-dialog hidden">' +
                    '<div class="inner" ng-transclude></div>' +
                    '</div>',
                replace   : true,
                transclude: true,
                link      : function( scope, element, attrs ){
                    attrs.$set( 'role', 'dialog' );
                    scope.dialog && scope.dialog._init( element );
                }
            };
        } );
}());