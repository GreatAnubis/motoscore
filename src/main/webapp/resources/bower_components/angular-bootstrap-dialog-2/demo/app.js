(function(){
    'use strict';
    /**
     * ******************************************************************************************************
     *
     *
     *
     *
     *  @author     Gery Hirschfeld
     *  @date       September, 2014
     *
     * ******************************************************************************************************
     */
    angular
        .module( 'app', [
            'angularBootstrapDialog'
        ] );

    angular
        .module( 'app' )
        .directive( 'exampleTitle', [ function(){
            return {
                restrict: 'E',
                scope   : {
                    text: '@'
                },
                template: '<div class="header">\
                                <h1>{{ text }}</h1>\
                                <button class="close">X</button>\
                           </div>',
                replace : true
            }
        } ] );

    angular
        .module( 'app' )
        .directive( 'exampleContent', [ function(){
            return {
                restrict: 'E',
                template: '<div class="content">\
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\
                           </div>',
                replace : true
            }
        } ] );

    angular
        .module( 'app' )
        .directive( 'exampleFooter', [ function(){
            return {
                restrict: 'E',
                scope   : {
                    dialog: '='
                },
                template: '<div class="footer">\
                                <button class="btn btn-default" ng-click="dialog.close()">Cancel</button>\
                                <button class="btn btn-primary" ng-click="dialog.close()">Ok</button>\
                           </div>',
                replace : true
            }
        } ] );

    angular
        .module( 'app' )
        .controller( 'DemoCtrl', DemoCtrl );

    DemoCtrl.$inject = [
        '$scope', 'abDialog'
    ];

    function DemoCtrl( $scope, abDialog ){
        $scope.dialogDefault = new abDialog(  );
        $scope.dialogPrimary = new abDialog( );
        $scope.dialogInfo = new abDialog();
        $scope.dialogSuccess = new abDialog();
        $scope.dialogWarning = new abDialog();
        $scope.dialogDanger = new abDialog();

        $scope.dialogDefaultInverse = new abDialog();
        $scope.dialogPrimaryInverse = new abDialog();
        $scope.dialogInfoInverse = new abDialog();
        $scope.dialogSuccessInverse = new abDialog();
        $scope.dialogWarningInverse = new abDialog();
        $scope.dialogDangerInverse = new abDialog();

        $scope.dialogSM = new abDialog();
        $scope.dialogMD = new abDialog();
        $scope.dialogLG = new abDialog();

        $scope.dialogFade = new abDialog();
        $scope.dialogFlip = new abDialog();
        $scope.dialogZoom = new abDialog();
        $scope.dialogSlide = new abDialog();
        $scope.dialogSlideSlow = new abDialog();
        $scope.dialogSlideFast = new abDialog();

        $scope.dialogClosable = new abDialog( {
            closable: false
        } );

        $scope.dialogModal = new abDialog( {
            modal: false
        } );

        $scope.dialogMovable = new abDialog( {
            movable: true
        } );

        $scope.dialogPrimary.open();


    }

}());