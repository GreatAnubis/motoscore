Angular-Bootstrap-Dialog
===========

**Angular Bootstrap Dialog** is a module for the [AngularJS](http://angularjs.org/) framework and extends the [Bootstrap](http://getbootstrap.com/) framework.

This version contains a native AngularJS directive and factory based on the bootstrap-modal.
[AngularJS](http://angularjs.org/) and [Bootstrap](http://getbootstrap.com/) are required.

##Installation

Add the css:

```html
<!-- Bootstrap -->
<link href="../path/to/bootstrap.min.css" rel="stylesheet"/>

<!-- AB Dialog -->
<link href="../path/to/angular-bootstrap-dialog.min.css" rel="stylesheet"/>
```

Add the js files:

```html
<script src="../path/to/angular-bootstrap-dialog.min.js"></script>
```

Add angular module to your app

```html
angular.module( 'myApp', [ 'angularBootstrapDialog'  ] );
```

##Usage
Define your dialog:

```html
<div ab-dialog="dialog" class="ab-dialog">
    <div class="header">
    	<h1>Title</h1>
    </div>
    <div class="content">
    	<p>Text...</p>
    </div>
    <div class="footer">
        <button class="btn btn-default close">Cancel</button>
    </div>
</div>
```

Create new dialog object:

```html
angular.module( 'myApp').controller( 'MyCtrl', function( $scope ){
	$scope.dialog = new abDialog();
	
	// to open
	//$scope.dialog.open();
	
	// to close
	//$scope.dialog.close();
	
} );
```

