'use strict';

module.exports = function( grunt ){

    /**
     * Load required Grunt tasks. These are installed based on the versions listed
     * in `package.json` when you do `npm install` in this directory.
     */
    require( 'load-grunt-tasks' )( grunt );

    /**
     * Time how long tasks take. Can help when optimizing build times
     */
    require( 'time-grunt' )( grunt );

    /**
     * Configurable paths for the application
     */
    var userConfig = require( './Grunt.config.js' );

    /**
     * This is the configuration object Grunt uses to give each plugin its
     * instructions.
     * Define the configuration for all the tasks
     */
    var taskConfig = {

        /**
         * We read in our `package.json` file so we can access the package name and version. It's already there, so
         * we don't repeat ourselves here.
         */
        pkg  : grunt.file.readJSON( 'package.json' ),
        bower: grunt.file.readJSON( 'bower.json' ),

        /**
         * The banner is the comment that is placed at the top of our compiled source files. It is first processed
         * as a Grunt template, where the `<%=` pairs are evaluated based on this very configuration object.
         */
        meta: {
            banner: '/**\n' +
                ' * @appName    <%= bower.name %>\n' +
                ' * @version    <%= bower.version %>\n' +
                ' * @date       <%= grunt.template.today("yyyy-mm-dd") %>\n' +
                ' * @copyright  <%= grunt.template.today("yyyy") %> <%= bower.authors[0] %>\n' +
                ' * Licensed    <%= bower.license %>\n' +
                ' */\n'
        },

        /**
         * Settings for unit and e2e test with karma
         */
        karma: {
            unit: {
                configFile: '<%= testDir %>/karma.unit.config.js',
                singleRun : true
            },
            e2e : {
                configFile: '<%= testDir %>/karma.e2e.config.js',
                singleRun : true
            }
        },

        /**
         *
         */
        watch: {
            files  : [
                'Grunt.config.js',
                '<%= devDir %>/<%= appFiles.js %>',
                '<%= devDir %>/less/*.less'
            ],
            tasks  : [
                'build'
            ],
            options: {
                event: ['added', 'changed', 'deleted']
            }
        },

        /**
         * `jshint` defines the rules of our linter as well as which files we should check. This file, all javascript
         * sources, and all our unit tests are linted based on the policies listed in `options`. But we can also
         * specify exclusionary patterns by prefixing them with an exclamation point (!); this is useful when code comes
         * from a third party but is nonetheless inside `src/`.
         * Make sure code styles are up to par and there are no obvious mistakes
         */
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require( 'jshint-stylish' )
            },
            all    : {
                src: [
                    'Gruntfile.js',
                    '<%= devDir %>/<%= appFiles.js %>'
                ]
            },
            test   : {
                options: {
                    jshintrc: '<%= testDir %>/.jshintrc'
                },
                src    : ['<%= testDir %>/<%= appFiles.spec =>']
            }
        },

        /**
         * The 'clean' tasks cleans the mention directories
         */
        clean: {
            deploy: {
                src: ['<%= deployDir %>/**/*', '<%= deployDir %>']
            }
        },

        /**
         * The `copy` task just copies files from A to B. We use it here to copy our project assets
         * (images, fonts, etc.) and javascripts into `buildDir`, and then to copy the assets to `deployDir`.
         */
        //copy: {},

        /**
         * LESS
         */
        less: {
            deploy: {
                options: {
                    banner: '<%= meta.banner %>'
                },
                files: {
                    '<%= deployDir %>/css/<%= appName %>.css': '<%= devDir %>/<%= appFiles.less %>'
                }
            }
        },

        /**
         * The 'cssmin' task combines, minifies and adds a banner to our stylesheets
         */
        cssmin: {
            deploy: {
                options: {
                    banner: '<%= meta.banner %>'
                },
                files  : {
                    '<%= deployDir %>/css/<%= appName %>.min.css': [ '<%= deployDir %>/css/<%= appName %>.css' ]
                }
            }
        },

        /**
         *
         */
        concat: {
            deploy: {
                src : [ '<%= devDir %>/<%= appFiles.js %>' ],
                dest: '<%= deployDir %>/js/<%= appName %>.js'
            }
        },

        /**
         * ngmin tries to make the code safe for minification automatically by
         * using the Angular long form for dependency injection. It doesn't work on
         * things like resolve or inject so those have to be done manually.
         */
        ngmin: {
            deploy: {
                src : ['<%= deployDir %>/js/<%= appName %>.js'],
                dest: '<%= deployDir %>/js/<%= appName %>.js'
            }
        },

        /**
         * The 'uglify' tasks also like the cssmin combines, minifies and adds a banner to our javascript files. We also
         * generat a debug file with the 'beautify' option.
         */
        uglify: {
            options    : {
                banner: '<%= meta.banner %>'
            },
            deployDebug: {
                options: {
                    beautify: true,
                    mangle  : false
                },
                files  : {
                    '<%= deployDir %>/js/<%= appName %>.js': [ '<%= deployDir %>/js/<%= appName %>.js' ]
                }
            },
            deployMin  : {
                options: {
                    mangle: {
                        except: ['jQuery', 'Angular', 'angular', '$']
                    }
                },
                files  : {
                    '<%= deployDir %>/js/<%= appName %>.min.js': [ '<%= deployDir %>/js/<%= appName %>.js' ]
                }
            }
        },


        /**
         * The 'bump' task
         */
        bump: {
            options: {
                files        : ['package.json', 'bower.json'],
                updateConfigs: [ 'pkg' ],
                commit       : false,
                //commitMessage: 'Release v%VERSION%',
                //commitFiles: ['package.json'],
                createTag    : false,
                tagName      : '%VERSION%',
                tagMessage   : 'Version %VERSION%',
                push         : false
                //pushTo: 'upstream',
                //gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d'
            }
        }


    };
    grunt.initConfig( grunt.util._.extend( taskConfig, userConfig ) );

    /**
     * Build tasks for building the dev environment
     */
    grunt.registerTask( 'default', [
        'watch'
    ] );



    /**
     * This tasks are for the testing stuff with karma and jasmine
     */
    grunt.registerTask( 'check', [
        'jshint:all',
        'jshint:test'
    ] );

    grunt.registerTask( 'test', [
        'jshint:test',
        'karma:unit',
        'karma:e2e'
    ] );

    grunt.registerTask( 'test:unit', [
        'jshint:test',
        'karma:unit'
    ] );

    grunt.registerTask( 'test:e2e', [
        'jshint:test',
        'karma:e2e'
    ] );

    /**
     * The following tasks are for the deployments
     */
    grunt.registerTask( 'build', [
        'jshint:all',
        'clean:deploy',
        'less:deploy',
        'cssmin:deploy',
        'concat:deploy',
        'ngmin:deploy',

        'uglify:deployDebug',
        'uglify:deployMin'

    ] );


};
