/**
 * This file/module contains all configuration for the build process.
 */
module.exports = {

    appName: 'angular-bootstrap-dialog',

    devDir   : 'src',
    testDir  : 'test',
    deployDir: 'dist',

    libDir: 'lib',

    appFiles: {

        js: 'js/*.js',
        less: 'less/main.less',
        spec: '**/*.spec.js'
    }



};
