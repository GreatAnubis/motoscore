/**
 * Created by Anubis on 2014-08-05.
 */
'use strict';


App.factory('UserService', function ($resource) {

    return $resource(_contextPath + '/user/:action', {},
        {
            authenticate: {
                method: 'POST',
                params: {'action': 'authenticate'},
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            },
            logout: {
                method: 'POST',
                params: {'action': 'logoff'}
            }
        });
});


App.service('containerService', ['$http', function ($http) {
    var self = this;
    // sidebarMenuController
    self.yearList = [];
    self.yearMonthsList = [];
    self.selectedYear = 0;
    self.selectedMonth = 0;
    // eventsController
    self.eventsForAYear = {};

    this.setYearList = function (newyearList) {
        self.yearList = newyearList;
    }

    this.setYearMonthsList = function (newyearMonthsList) {
        self.yearMonthsList = newyearMonthsList;
    }

    var service = {
        downloadYearsWithMonths: function () {
            $http({method: 'GET', url: _contextPath + '/current/racingeventsyearmonths.json'}).
                success(function (data, status, headers, config) {
                    self.setYearMonthsList(data);
                    var tmp = [];
                    for (var prop in data) {
                        tmp.push(data[prop].year);
                    }
                    self.setYearList(tmp);
                }).
                error(function (data, status, headers, config) {
                });

        },
        getYearsMonthsList: function () {
            return self.yearMonthsList;
        },
        getylist: function () {
            return self.yearList;
        },
        clearAll: function () {
            self.yearMonthsList = [];
            return self.yearList = [];
        },
        getYears: function () {
            return self.years;
        },
        setYears: function (years) {
            self.years = years;
        }


    }
    return service;
}]);

App.service('eventsDownloadService', ['$http', function ($http) {
    var sharedService = {
        setsuccess: function (funForSuccess) {
            console.log("setsuccess");
            successFunc = funForSuccess;
        },
        setFinishLoadingFunc: function (funForFinishLoadingFunc) {
            console.log("funForFinishLoadingFunc");
            finishLoadingFunc = funForFinishLoadingFunc;
        },
        setBeginLoadingFunc: function (funForBeginLoadingFunc) {
            console.log("funForBeginLoadingFunc");
            beginLoadingFunc = funForBeginLoadingFunc;
        },
        downloadEvents: function (year, month) {
            beginLoadingFunc();
            console.log("eventsDownloadService:downloadEvents");
            $http({method: 'GET',
                params: {year: year, month: month},
                url: _contextPath + '/events/racingevents.json'}).
                success(function (data, status, headers, config) {
                    console.log("downloadEvents");
                    finishLoadingFunc(false);
                    successFunc(data, status, headers);
                }).
                error(function (data, status, headers, config) {
                    finishLoadingFunc(true);
                    console.log("downloadEvents");
                    successFunc(data, status, headers);
                });
        },
        downloadEventsForYear: function (year) {
            beginLoadingFunc();
            console.log("eventsDownloadService:downloadEvents");
            $http({method: 'GET',
                params: {year: year},
                url: _contextPath + '/events/racingevents.json'}).
                success(function (data, status, headers, config) {
                    console.log("downloadEventsForYear");
                    finishLoadingFunc(false);
                    successFunc(data, status, headers);
                }).
                error(function (data, status, headers, config) {
                    finishLoadingFunc(true);
                    console.log("downloadEventsForYear");
                    successFunc(data, status, headers);
                });
        }
    };

    var successFunc = function (data, status, headers) {
    };
    var beginLoadingFunc = function () {
    };
    var finishLoadingFunc = function (error) {
    };
    return sharedService;
}]);


App.service('downloadService', ['$http', function ($http) {
    var self = this;
    self.scoreForYear = [];
    self.scoreForYearAndMonth = {};
    self.scoreForLastEvent = {};

    var service = {
        downloadScoreForYear: function (year, handleSuccess, handleError) {
            $http({method: 'GET', url: _contextPath + '/current/currentYearScore.json'}).
                success(function (data, status, headers, config) {
                    handleSuccess(data, status, headers);
                }).
                error(function (data, status, headers, config) {
                    handleError(data, status, headers);
                });
        },
        downloadScoreForYearAndMonth: function (year, month, handleSuccess, handleError) {
            $http({method: 'GET',
                params: {year: year, month: month},
                url: _contextPath + '/current/monthScore.json'}).
                success(function (data, status, headers, config) {
                    handleSuccess(data, status, headers);
                }).
                error(function (data, status, headers, config) {
                    handleError(data, status, headers);
                });
        },
        downloadScoreForLastEvent: function (handleSuccess, handleError) {
            $http({method: 'GET',
                url: _contextPath + '/current/lastEventScore.json'}).
                success(function (data, status, headers, config) {
                    handleSuccess(data, status, headers);
                }).
                error(function (data, status, headers, config) {
                    handleError(data, status, headers);
                });
        }
    }
    return service;
}]);


App.service('playerListService', ['$http', function ($http) {
    var self = this;

    //main storage for players
    self.players = {};
    self.loading = false;

    var sharedService = {
        downloadPlayers: function () {
            self.loading = true;
            console.log("playerListService:downloadPlayers");
            $http({method: 'GET',
                url: _contextPath + '/admin/playerlist/list.json'}).
                success(function (data, status, headers, config) {
                    console.log("playerListService:success");
                    self.players = data;
                    self.loading = false;
                    self.infoText = "playerListService:loaded: " + data.length + " entries";
                }).
                error(function (data, status, headers, config) {
                    console.log("playerListService:ERROR");
                    self.players = {};
                    self.loading = false;
                    self.infoText = "playerListService:Error: " + status;
                });
        },
        addPlayer: function (player) {
            console.log("eventsDownloadService:downloadEvents");
            self.submitting = true;
            return $http.post(_contextPath + '/admin/playerlist/add', player);
        },
        deletePlayer: function (playerId) {
            console.log("eventsDownloadService:downloadEvents");
            self.submitting = true;
            return $http.delete(_contextPath + '/admin/playerlist/delete', {params: {player_id: playerId }});
        },
        updatePlayer: function (player) {
            console.log("eventsDownloadService:downloadEvents");
            self.submitting = true;
            return $http.post(_contextPath + '/admin/playerlist/edit', player);
        },
        getPlayerList: function () {
            return self.players;
        },
        addPlayerToList: function (player) {
            self.players.push(player);
        },
        removePlayerFromList: function (player) {
            var index = self.players.indexOf(player);
            if (index > -1) {
                self.players.splice(index, 1);
                return true;
            }
            return false;
        },
        existsInList: function (nick) {
            if (nick == null || nick.length == 0) {
                return false;
            }
            for (var i = 0; i < self.players.length; i++) {
                if (self.players[i].player_nick == nick) {
                    return true;
                }
            }
            return false;
        }
    };
    return sharedService;
}]);

App.service('trackListService', ['$http', function ($http) {
    var self = this;
    self.tracks = {};
    self.loading = false;

    self.servicePath = _contextPath + '/admin/tracklist/';

    var sharedService = {
        downloadTracks: function () {
            self.loading = true;
            console.log("trackListService:downloadTracks");
            $http({method: 'GET',
                url: self.servicePath + 'list.json'}).
                success(function (data, status, headers, config) {
                    console.log("trackListService:success");
                    self.tracks = data;
                    self.loading = false;
                    self.infoText = "trackListService:loaded: " + data.length + " entries";
                }).
                error(function (data, status, headers, config) {
                    console.log("trackListService:ERROR");
                    self.tracks = {};
                    self.loading = false;
                    self.infoText = "trackListService:Error: " + status;
                });
        },
        addTrack: function (track) {
            console.log("trackListService:addTrack");
            self.submitting = true;
            return $http.post(self.servicePath + 'add', track);
        },
        deleteTrack: function (trackId) {
            console.log("trackListService:deleteTrack");
            self.submitting = true;
            return $http.delete(self.servicePath + 'delete', {params: {track_id: trackId }});
        },
        updateTrack: function (track) {
            console.log("trackListService:updateTrack");
            self.submitting = true;
            return $http.post(self.servicePath + 'edit', track);
        },
        getTrackList: function () {
            return self.tracks;
        },
        addTrackToList: function (track) {
            self.tracks.push(track);
        },
        removeTrackFromList: function (track) {
            var index = self.tracks.indexOf(track);
            if (index > -1) {
                self.tracks.splice(index, 1);
                return true;
            }
            return false;
        },
        existsInList: function (trackName) {
            if (trackName == null || trackName.length == 0) {
                return false;
            }
            for (var i = 0; i < self.tracks.length; i++) {
                if (self.tracks[i].track_name == trackName) {
                    return true;
                }
            }
            return false;
        }
    };
    return sharedService;
}]);



