/**
 * Created by Anubis on 2014-08-05.
 */
'use strict';


App.directive('yearscore', ['downloadService', '$locale', 'containerService', function (downloadService, $locale, containerService) {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: _contextPath + '/layouts/summaryyearscores.html',
        link: function ($scope, element) {

        },
        controller: function ($scope) {
            $scope.scoresTitle = 'messages.loading.long';
            $scope.loading = true;

            $scope.range = new Date().getFullYear();
            $scope.items = function () {
                return containerService.getylist();
            }
            function handleError(data, status, headers) {
                $scope.loading = false;
                $scope.scoresTitle = 'messages.error';
            }

            function handleSuccess(data, status, headers) {
                $scope.loading = false;
                $scope.scoresTitle = 'summary.score.year';
                $scope.scores = data.scores;
                //$scope.scoresTitle = data.scoresTitle;
                $scope.range = data.range;

            }

            $scope.scoreForYear = function (year) {
                $scope.range = year;
                $scope.loading = true;
                downloadService.downloadScoreForYear(year, handleSuccess, handleError);
            };

            if ($scope.items() != null && $scope.items().length != 0) {
                $scope.scoreForYear($scope.range);
            }


            $scope.$watch(function () {
                return containerService.getylist();
            }, function (newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                } // AKA first run
                $scope.scoreForYear($scope.range);
            }, true);

            $scope.status = {
                isopen: false
            };

            $scope.toggled = function (open) {
                //console.log('Dropdown is now: ', open);
            };
        }
    };
}]);


App.directive('monthscore', ['downloadService', '$locale', 'containerService','$translate', function (downloadService, $locale, containerService,$translate) {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: _contextPath + '/layouts/summarymonthscores.html',
        link: function ($scope, element) {

        },
        controller: function ($scope) {
            $scope.scoresTitle = 'messages.loading.long';
            $scope.loading = true;

            $scope.currentlyVisiblemonth = new Date().getMonth();
            $scope.monthString = new Date().getMonth() - 1;
            $scope.range = 'months.'+$scope.currentlyVisiblemonth;

            $scope.chosenYear = new Date().getFullYear();
            $scope.itemyears = function () {
                return containerService.getYearsMonthsList();
            };
            $scope.monthlistForChosenYear = [];

            function handleError(data, status, headers) {
                $scope.loading = false;
                $scope.scoresTitle = 'messages.error';
            }

            function handleSuccess(data, status, headers) {
                $scope.loading = false;
                $scope.scoresTitle = 'summary.score.month';

                $scope.scores = data.scores;
                $scope.range = 'months.'+data.range;
                $scope.monthString = data.range;
            }

            $scope.scoreForYear = function (year) {
                //if($scope.chosenYear != year) {
                $scope.chosenYear = year;
                if ($scope.itemyears().length == 0) {
                    $scope.monthlistForChosenYear = {};
                }
                for (var prop in $scope.itemyears()) {
                    if (prop.year == year) {
                        $scope.monthlistForChosenYear = prop.months;
                        break;
                    }
                }
                $scope.loading = true;
                downloadService.downloadScoreForYearAndMonth($scope.chosenYear, $scope.currentlyVisiblemonth, handleSuccess, handleError);
                // }
            };

            $scope.scoreForMonth = function (month) {
                //if($scope.currentlyVisiblemonth != month) {
                $scope.currentlyVisiblemonth = month;
                $scope.loading = true;
                downloadService.downloadScoreForYearAndMonth($scope.chosenYear, $scope.currentlyVisiblemonth, handleSuccess, handleError);
                //}
            };


            function updateMonthListForChosenYear() {
                for (var prop in $scope.itemyears()) {
                    if ($scope.itemyears()[prop].year == $scope.chosenYear) {
                        $scope.monthlistForChosenYear = $scope.itemyears()[prop].months;
                        break;
                    }
                }
                $scope.scoreForMonth($scope.currentlyVisiblemonth);
            }

            if ($scope.itemyears() != null && $scope.itemyears().length != 0) {
                updateMonthListForChosenYear();
            }

            $scope.$watch(function () {
                return $scope.itemyears();
            }, function (newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                } // AKA first run
                updateMonthListForChosenYear();
            }, true);


            $scope.status = {
                isopen: false
            };

            $scope.toggled = function (open) {
                //console.log('Dropdown is now: ', open);
            };
        }
    };
}]);


App.directive('lasteventscore', ['downloadService', '$locale', function (downloadService, $locale) {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: _contextPath + '/layouts/summaryscoresnoparams.html',
        link: function ($scope, element) {

        },
        controller: function ($scope) {
            $scope.scoresTitle = 'messages.loading.long';
            $scope.loading = true;

            function handleError(data, status, headers) {
                $scope.loading = false;
                $scope.scoresTitle = 'messages.error';
            }

            function handleSuccess(data, status, headers) {
                $scope.loading = false;
                $scope.scoresTitle = 'summary.score.last-event';
                $scope.scores = data.scores;
            }

            $scope.scoreForLastEvent = function () {
                $scope.loading = true;
                downloadService.downloadScoreForLastEvent(handleSuccess, handleError);
            };

            $scope.scoreForLastEvent();
        }
    };
}]);

// -------------------------------------------------------------------

App.directive('updateplayer', ['downloadService', '$locale', 'playerListService', function (downloadService, $locale, playerListService) {
    return {
        restrict: 'AE',
        scope: {player: '=player'},
        templateUrl: _contextPath + '/layouts/updatePlayerForm.html',
        link: function ($scope, element) {
            $scope.updatePlayer = false;
        },
        controller: function ($scope) {
            $scope.view = {
                nick: $scope.player.player_nick,
                editorEnabled: false
            };

            $scope.enableEditor = function () {
                $scope.view.editorEnabled = true;
                $scope.view.nick = $scope.player.player_nick;
            };

            $scope.disableEditor = function () {
                $scope.view.editorEnabled = false;
            };

            $scope.save = function () {
                var savedPlayer = {};
                savedPlayer.player_nick = $scope.view.nick;
                savedPlayer.id = $scope.player.id;
                playerListService.updatePlayer(savedPlayer);

                $scope.player.player_nick = $scope.view.nick;
                $scope.disableEditor();
            };
        }

    };
}]);

App.directive('blink', function ($timeout, $log) {
    return {
        restrict: "A",
        scope: {blink: "="},
        transclude: true,
        controller: function ($scope, $element, $attrs) {
            $scope.$parent.blinkvis = true;
            $scope.blinked = $scope.blink.blink;
            $scope.condition = $scope.blink.condition;
            function showElement() {
                $scope.$parent.blinkvis = true;
                if ($scope.blinked <= 1) {
                    return;
                }
                $timeout(hideElement, 1000);
                $scope.blinked -= 1;
            }

            function hideElement() {
                $scope.$parent.blinkvis = false;
                $timeout(showElement, 1000);
            }

            if ($scope.condition == true) {
                hideElement();
            }
        },
        template: '<div ng-transclude></div>',
        replace: true
    };
});

App.directive("clickToEdit", function () {
    var editorTemplate = '<div class="click-to-edit">' +
        '<div ng-hide="view.editorEnabled">' +
        '{{value}} ' +
        '<a ng-click="enableEditor()">Edit</a>' +
        '</div>' +
        '<div ng-show="view.editorEnabled">' +
        '<input ng-model="view.editableValue">' +
        '<a href="#" ng-click="save()">Save</a>' +
        ' or ' +
        '<a ng-click="disableEditor()">cancel</a>.' +
        '</div>' +
        '</div>';

    return {
        restrict: "A",
        replace: true,
        template: editorTemplate,
        scope: {
            value: "=clickToEdit"
        },
        controller: function ($scope) {
            $scope.view = {
                editableValue: $scope.value,
                editorEnabled: false
            };

            $scope.enableEditor = function () {
                $scope.view.editorEnabled = true;
                $scope.view.editableValue = $scope.value;
            };

            $scope.disableEditor = function () {
                $scope.view.editorEnabled = false;
            };

            $scope.save = function () {
                $scope.value = $scope.view.editableValue;
                $scope.disableEditor();
            };
        }
    };
});


App.directive('nickAvailableValidator', ['$http', function ($http) {
    return {
        require: 'ngModel',
        link: function ($scope, element, attrs, ngModel) {
            // ngModel.$asyncValidators.nickAvailable = function(nick) {
            //  return $http.get(_contextPath + '/api/nick-exists?n='+ nick);
            // };
        }
    };
}]);

App.directive('playerscorerow', function () {
    return {
        scope: {
            playerscorerow: "="
        },
        restrict: 'AE',
        template: "<td style='font-weight: bold;'>{{player_nick}}</td>" +
            "<td ng-repeat='trackinevent in trackInRun track by trackinevent.id' style='text-align: center;' playerscores='trackinevent' ng-class='tableCellClass(score)'></td>" +
            "<td class='alert alert-success' style='text-align: center;'><strong>{{totalScore}}</strong></td>",
        link: function (scope, elm, attrs) {
            scope.players = scope.playerscorerow.players;
            scope.playerScores = scope.playerscorerow.scoresRow.playerScores;
            scope.trackInRun = scope.playerscorerow.tracks;
            scope.totalScore = scope.playerscorerow.scoresRow.totalScore;
            scope.player_nick = scope.players[scope.playerscorerow.scoresRow.playerId].player_nick;
        },
        controller: function ($scope) {
            this.getScore = function (trackId) {
                return $scope.playerScores[trackId];
            }
            $scope.tableCellClass = function (score) {
                if (score == "-") {
                    return "notinrace";
                }
                if (score == 12) {
                    return "firstplace";
                }
                if (score == 10) {
                    return "secondplace";
                }
                if (score == 8) {
                    return "thirdplace";
                }
                return "";
            };
        }
    }
});

App.directive('playerscores', function () {
    return {
        restrict: 'A',
        require: "^playerscorerow",
        template: '{{score}}',
        compile: function ($element, $attrs) {
            return {
                pre: function ($scope, $element, $attrs, topController) {
                    var tmpScore = topController.getScore($scope.trackinevent.id);
                    if (tmpScore !== undefined) {
                        $scope.score = tmpScore;
                    } else {
                        $scope.score = "-";
                    }
                }
            };
        }
    }
});


App.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function (file, uploadUrl) {
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .success(function () {
            })
            .error(function () {
            });
    }
}]);

App.controller('myCtrl', ['$scope', 'fileUpload', function ($scope, fileUpload) {

    $scope.uploadFile = function () {
        var file = $scope.myFile;
        console.log('file is ' + JSON.stringify(file));
        var uploadUrl = "/fileUpload";
        fileUpload.uploadFileToUrl(file, uploadUrl);
    };

}]).directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);


App.service('$youtube', ['$window', '$rootScope', function ($window, $rootScope) {
    // adapted from http://stackoverflow.com/a/5831191/1614967
    var youtubeRegexp = /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*[^\w\s-])([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig;
    var timeRegexp = /t=(\d+)[ms]?(\d+)?s?/;

    function contains(str, substr) {
        return (str.indexOf(substr) > -1);
    }

    var service = {
        // Frame is ready
        ready: false,

        // Element id for player
        playerId: null,

        // Player currently in use
        player: null,

        // Current video id
        videoId: null,

        // Size
        playerHeight: '300px',
        playerWidth: '100%',

        currentState: null,

        setURL: function (url) {
            service.videoId = service.getIdFromURL(url);
            console.log("setURL: " + url);
        },

        getIdFromURL: function (url) {
            var id = String(url).replace(youtubeRegexp, '$1');
            console.log("getIdFromURL: " + url);
            if (contains(id, ';')) {
                var pieces = id.split(';');

                if (contains(pieces[1], '%')) {
                    // links like this:
                    // "http://www.youtube.com/attribution_link?a=pxa6goHqzaA&amp;u=%2Fwatch%3Fv%3DdPdgx30w9sU%26feature%3Dshare"
                    // have the real query string URI encoded behind a ';'.
                    // at this point, `id is 'pxa6goHqzaA;u=%2Fwatch%3Fv%3DdPdgx30w9sU%26feature%3Dshare'
                    var uriComponent = decodeURIComponent(id.split(';')[1]);
                    id = ('http://youtube.com' + uriComponent)
                        .replace(youtubeRegexp, '$1');
                } else {
                    // https://www.youtube.com/watch?v=VbNF9X1waSc&amp;feature=youtu.be
                    // `id` looks like 'VbNF9X1waSc;feature=youtu.be' currently.
                    // strip the ';feature=youtu.be'
                    id = pieces[0]
                }
            } else if (contains(id, '#')) {
                // id might look like '93LvTKF_jW0#t=1'
                // and we want '93LvTKF_jW0'
                id = id.split('#')[0];
            }

            return id;
        },

        getTimeFromURL: function (url) {
            url || (url = '');
            console.log("getTimeFromURL: " + url);

            // t=4m20s
            // returns ['t=4m20s', '4', '20']
            // t=46s
            // returns ['t=46s', '46']
            // t=46
            // returns ['t=46', '46']
            var times = url.match(timeRegexp);

            if (!times) {
                // zero seconds
                return 0;
            }

            // assume the first
            var full = times[0],
                minutes = times[1],
                seconds = times[2];

            // t=4m20s
            if (typeof seconds !== 'undefined') {
                seconds = parseInt(seconds, 10);
                minutes = parseInt(minutes, 10);

                // t=4m
            } else if (contains(full, 'm')) {
                minutes = parseInt(minutes, 10);
                seconds = 0;

                // t=4s
                // t=4
            } else {
                seconds = parseInt(minutes, 10);
                minutes = 0;
            }

            // in seconds
            return seconds + (minutes * 60);
        },

        createPlayer: function (playerVars) {
            //console.log("playerHeight: " + playerVars.playerHeight);
            //console.log("playerWidth: " + playerVars.playerWidth);
            // console.log("videoId: " + playerVars.videoId);
            return new YT.Player(this.playerId, {
                height: this.playerHeight,
                width: this.playerWidth,
                videoId: this.videoId,
                playerVars: playerVars,
                events: {
                    onReady: onPlayerReady,
                    onStateChange: onPlayerStateChange
                }
            });
        },

        loadPlayer: function (playerVars) {
            console.log("loadPlayer: " + playerVars);
            if (this.ready && this.playerId && this.videoId) {
                if (this.player && typeof this.player.destroy === 'function') {
                    this.player.destroy();
                }

                this.player = this.createPlayer(playerVars);
            }
        }
    };

    // YT calls callbacks outside of digest cycle
    function applyBroadcast(event) {
        $rootScope.$apply(function () {
            $rootScope.$broadcast(event);
        });
    }

    // from YT.PlayerState
    var stateNames = {
        0: 'ended',
        1: 'playing',
        2: 'paused',
        3: 'buffering',
        5: 'queued'
    };

    var eventPrefix = 'youtube.player.';

    function onPlayerReady(event) {
        console.log("onPlayerReady");
        applyBroadcast(eventPrefix + 'ready');
    }

    function onPlayerStateChange(event) {
        console.log("onPlayerStateChange");
        var state = stateNames[event.data];
        if (typeof state !== 'undefined') {
            applyBroadcast(eventPrefix + state);
        }
        service.currentState = state;
    }

    // Inject YouTube's iFrame API
    (function () {
        var tag = document.createElement('script');
        tag.src = "//www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }());

    // Youtube callback when API is ready
    $window.onYouTubeIframeAPIReady = function () {
        $rootScope.$apply(function () {
            service.ready = true;
        });
    };

    return service;
}])
    .directive('youtubeVideo', ['$youtube', function ($youtube) {
        return {
            restrict: 'EA',
            scope: {
                videoId: '=',
                videoUrl: '=',
                playerVars: '='
            },
            link: function (scope, element, attrs) {
                // Attach to element
                $youtube.playerId = element[0].id;

                var stopWatchingReady = scope.$watch(
                    function () {
                        return $youtube.ready
                            // Wait until one of them is defined...
                            && (typeof scope.videoUrl !== 'undefined'
                                || typeof scope.videoId !== 'undefined');
                    },
                    function (ready) {
                        if (ready) {
                            stopWatchingReady();

                            // use URL if you've got it
                            if (typeof scope.videoUrl !== 'undefined') {
                                scope.$watch('videoUrl', function (url) {
                                    $youtube.setURL(url);
                                    $youtube.loadPlayer(scope.playerVars);
                                });

                                // otherwise, watch the id
                            } else {
                                scope.$watch('videoId', function (id) {
                                    $youtube.videoId = id;
                                    $youtube.loadPlayer(scope.playerVars);
                                });
                            }
                        }
                    });
            }
        };
    }]);


