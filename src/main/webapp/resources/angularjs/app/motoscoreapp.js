'use strict';

var App = angular.module('motoscoreApp', [ 'ui.bootstrap', 'ngResource', 'ngRoute', 'ngCookies', 'tmh.dynamicLocale', 'pascalprecht.translate', 'ngResource', 'ngAnimate' ]).run(function ($http, $cookies, $rootScope) {
    $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
    $rootScope.loading = true;
});

App.controller('MainController', ['$scope', '$translate', '$rootScope',
    function ($scope, $translate, $rootScope) {
        $rootScope.viewNavigation = false;
    }]);


App.controller('HeaderController', ['$scope', '$location', function ($scope, $location) {
    $scope.isActive = function (viewLocation) {
        if (viewLocation == "/") {
            if ($location.path() == "" || $location.path() == "/" || $location.path() == "/index") {
                return true;
            }
            else {
                return false;
            }
        }
        return viewLocation === $location.path() || $location.path().indexOf(viewLocation) == 0;
    };
}]);

App.controller('LanguageController', ['$scope', '$translate',
    function ($scope, $translate) {
        $scope.changeLanguage = function ($event, languageKey) {
            $translate.use(languageKey);
        };
    }]);

App.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);

App.config(['$routeProvider', '$httpProvider', '$translateProvider', 'tmhDynamicLocaleProvider', '$locationProvider',
        function ($routeProvider, $httpProvider, $translateProvider, tmhDynamicLocaleProvider, $locationProvider) {
            $routeProvider.
                when('/index', {
                    templateUrl: _contextPath + '/layouts/dashboard.html',
                    controller: function ($scope, $location) {
                    }
                }).
                when('/login', {
                    templateUrl: _contextPath + '/layouts/login.html',
                    controller: 'LoginController'
                }).
                when('/admin/playerlist', {
                    templateUrl: _contextPath + '/layouts/playerlist.html',
                    controller: 'PlayerListController'
                }).
                when('/admin/tracklist', {
                    templateUrl: _contextPath + '/layouts/tracklist.html',
                    controller: 'TrackListController'
                }).otherwise({
                    templateUrl: _contextPath + '/layouts/dashboard.html',
                    controller: 'MainController'
                });
            //$locationProvider.html5Mode(true);
            //$locationProvider.hashPrefix('!');
            // $locationProvider.html5Mode(true);

            var getTokenData = function () {
                var defaultCsrfTokenHeader = 'X-CSRF-TOKEN';
                var csrfTokenHeaderName = 'X-CSRF-HEADER';
                var xhr = new XMLHttpRequest();
                xhr.open('head', _contextPath, false);
                xhr.send();
                var csrfTokenHeader = xhr.getResponseHeader(csrfTokenHeaderName);
                csrfTokenHeader = csrfTokenHeader ? csrfTokenHeader : defaultCsrfTokenHeader;
                return { headerName: csrfTokenHeader, token: xhr.getResponseHeader(csrfTokenHeader) };
            };
            var csrfTokenData = getTokenData();
            var numRetries = 0;
            var MAX_RETRIES = 5;

            $httpProvider.interceptors.push(function ($q, $rootScope, $location, $injector) {
                    return {
                       request: function (config) {
                            config.headers[csrfTokenData.headerName] = csrfTokenData.token;
                            return config || $q.when(config);
                       },
                        response: function(response) {
                            // reset number of retries on a successful response
                            numRetries = 0;
                            return response;
                        },
                        'responseError': function (rejection) {

                            if (rejection.status == 403 && numRetries < MAX_RETRIES) {
                                csrfTokenData = getTokenData();
                                var $http = $injector.get('$http');
                                ++numRetries;
                                return $http(rejection.config);
                            }

                            var status = rejection.status;
                            var config = rejection.config;
                            var method = config.method;
                            var url = config.url;

                            if ($location.path() != '' && $location.path() != '/' && $location.path() != "/index") {
                                if (status == 401) {
                                    if (url == _contextPath + "/user/authenticate") {
                                        $rootScope.error = "Bad username or password...";
                                    }
                                    else if (url != "/index")// && url != _contextPath + "/user")
                                    {
                                        $rootScope.backParamURL = $location.path();
                                        $location.path("/login");
                                    }

                                } else {
                                    $rootScope.error = "Error: " + status + " please try again..";
                                }
                            }

                            return $q.reject(rejection);
                        }
                    };
                }
            );

            /* Registers auth token interceptor, auth token is either passed by header or by query parameter
             * as soon as there is an authenticated user */
            $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
                    return {
                        'request': function (config) {
                            var isRestCall = true;// config.url.indexOf('rest') == 0;
                            if (isRestCall && angular.isDefined($rootScope.authToken)) {
                                var authToken = $rootScope.authToken;
                                if (exampleAppConfig.useAuthTokenHeader) {
                                    config.headers['X-Auth-Token'] = authToken;
                                } else {
                                    config.url = config.url + "?token=" + authToken;
                                }
                            }
                            return config || $q.when(config);
                        }
                    };
                }
            );

            // Initialize angular-translate
            $translateProvider.useStaticFilesLoader({
                prefix: _contextPath + '/resources/i18n/',
                suffix: '.json'
            });

            $translateProvider.preferredLanguage('pl');

            $translateProvider.useCookieStorage();

            tmhDynamicLocaleProvider.localeLocationPattern(_contextPath + '/resources/bower_components/angular-i18n/angular-locale_{{locale}}.js')
            tmhDynamicLocaleProvider.useCookieStorage('NG_TRANSLATE_LANG_KEY');
        }]
).run(function ($rootScope, $location, $cookieStore, UserService) {

        /* Reset error when a new view is loaded */
        $rootScope.$on('$viewContentLoaded', function () {
            delete $rootScope.error;
        });

        $rootScope.hasRole = function (role) {

            if ($rootScope.user === undefined) {
                return false;
            }

            if ($rootScope.user.roles[role] === undefined) {
                return false;
            }

            return $rootScope.user.roles[role];
        };

        $rootScope.logout = function () {
            delete $rootScope.user;
            delete $rootScope.authToken;
            $cookieStore.remove('authToken');
            UserService.logout(function (logoffResult) {
                $location.path("/index");
            });

        };

        /* Try getting valid user from cookie or go to login page */
        var originalPath = $location.path();
        //$location.path("/login");
        var authToken = $cookieStore.get('authToken');
        if (authToken !== undefined) {
            $rootScope.authToken = authToken;
            UserService.get(function (user) {
                $rootScope.user = user;
                if (originalPath == "/login") {
                    originalPath = "/index";
                }
                $location.path(originalPath);
            });
        }
        else {
            if ($rootScope.user === undefined) {
                UserService.get(function (user) {
                    $rootScope.user = user;
                });
            }
        }

        $rootScope.initialized = true;
    });

App.controller('loggedUserDropDownCtrl', ['$scope',
    function ($scope) {
        $scope.status = {
            isopen: false
        };

        $scope.toggled = function (open) {
            console.log('Dropdown is now: ', open);
        };

        $scope.toggleDropdown = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.status.isopen = !$scope.status.isopen;
        };
    }]);

App.controller('SummaryScoresCtrl', ['$scope',
    function ($scope) {


    }]);
App.controller('LastUpdateInfoCtrl', ['$scope', '$rootScope',
    function ($scope, $rootScope) {
        $scope.toggleNavigation = function () {
            $rootScope.viewNavigation = !$rootScope.viewNavigation;
        }

    }]);

App.controller('YearScoresCtrl', ['$scope',
    function ($scope) {


    }]);

App.filter('reverse', function () {
    return function (items) {
        return items.slice().reverse();
    };
});


var ModalInstanceCtrl = function ($scope, $modalInstance, url, $sce) {

    $scope.url = url;
    $scope.currentProjectUrl = $sce.trustAsResourceUrl($scope.url);

    $scope.ok = function () {
        $modalInstance.close($scope.url);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};

/*
 <div ng-controller = "myCtrl">
 <input type="file" file-model="myFile"/>
 <button ng-click="uploadFile()">upload me</button>
 </div>
 */
App.controller('uploadCtrl', ['$scope', 'fileUpload', function ($scope, fileUpload) {

    $scope.uploadFile = function () {
        var file = $scope.myFile;
        console.log('file is ' + JSON.stringify(file));
        var uploadUrl = "/fileUpload";
        fileUpload.uploadFileToUrl(file, uploadUrl);
    };

}]);


App.controller('eventsController', [ '$rootScope', '$scope', "$http", "$locale", "eventsDownloadService", '$modal', '$sce', 'filterFilter', 'containerService',
    function ($rootScope, $scope, $http, $locale, eventsDownloadService, $modal, $sce, filterFilter, containerService) {
        var self = this;
        console.log("eventsController");
        //$scope.filteredEventsArray = [];
        $scope.isVisible = false;

        $scope.hasCode = function (vehicle) {
            if (vehicle != null && vehicle !== undefined && vehicle.vehicle_code != null && vehicle.vehicle_code !== undefined) {
                return true;
            }
            return false;
        };

        $scope.open = function ($event, url) {
            $event.preventDefault();
            console.log("url: " + url);

            $scope.url = url;
            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: ModalInstanceCtrl,
                resolve: {
                    url: function () {
                        console.log("url resolve");
                        return $scope.url;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                //$scope.selected = selectedItem;
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

        eventsDownloadService.setsuccess(function handleSuccess(data, status, headers) {
            console.log("handleSuccess");
            containerService.eventsForAYear = data;
        });

        $scope.$watch(function () {
            return containerService.eventsForAYear;
        }, function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } // AKA first run
            updateFilteredEventsArray(containerService.selectedYear, containerService.selectedMonth);
        }, true);
        $scope.$watch(function () {
            return containerService.selectedYear;
        }, function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } // AKA first run
            updateFilteredEventsArray(containerService.selectedYear, containerService.selectedMonth);
        }, true);
        $scope.$watch(function () {
            return containerService.selectedMonth;
        }, function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } // AKA first run
            updateFilteredEventsArray(containerService.selectedYear, containerService.selectedMonth);
        }, true);

        //$rootScope.$on("reloadCurrentEvents", function(event, args){
        //    updateFilteredEventsArray(args.year, args.month);
        //});

        self.year = 0;
        self.month = 0;
        $scope.filteredArray = {};

        $scope.filteredEventsArray = function () {
            return $scope.filteredArray;
        }

        $scope.racing_events = function () {
            return containerService.eventsForAYear;
        }

        //$scope.eventsfilter = {};

        var updateFilteredEventsArray = function (year, month) {
            if (containerService.eventsForAYear != null && year != 0 && month != 0) {
                if (self.year != year || self.month != month) {
                    self.year = year;
                    self.month = month;
                    var filterArgs = {};
                    filterArgs.year = year;
                    if (month != null) {
                        filterArgs.month = month;
                    }
                    $scope.eventsfilter = filterArgs;
                    console.log('Filter updated:', $scope.eventsfilter);

                    //$scope.filteredArray = filterFilter(containerService.eventsForAYear, filterArgs);
                }
            }
        }
        updateFilteredEventsArray(containerService.selectedYear, containerService.selectedMonth);

        $scope.isAnyVehicle = function (vehicles) {
            if (vehicles == undefined) {
                return false;
            }
            var arr = Object.keys(vehicles);
            var count = 0;
            for (var i = 0; i < arr.length; i++) {
                count = vehicles[Object.keys(vehicles)[0]].length;
                //for faster counting.... actually we do not need the complete list...
                if (count > 0) {
                    return true;
                }
            }
            return false;
        }

        $scope.tableCellClass = function (score) {
            if (score == undefined) {
                return "notinrace";
            }
            if (score == 12) {
                return "firstplace";
            }
            if (score == 10) {
                return "secondplace";
            }
            if (score == 8) {
                return "thirdplace";
            }
            return "";
        };
    }]);


App.controller('sidebarMenuController', [ '$rootScope', '$scope', "$http", "$locale", 'containerService', 'eventsDownloadService', function ($rootScope, $scope, $http, $locale, containerService, eventsDownloadService) {
    console.log("sidebarMenuController");
    $scope.loading = false;
    //$scope.monthstranslation = $locale.DATETIME_FORMATS.MONTH;

    function beginDownload() {
        $scope.loading = true;

    }

    function finishDownload(error) {
        $scope.loading = false;
        $rootScope.loading = false;

    }

    eventsDownloadService.setBeginLoadingFunc(beginDownload);
    eventsDownloadService.setFinishLoadingFunc(finishDownload);

    $scope.loadYearMonth = function (year, month) {
        console.log("loadYearMonth");
        if (containerService.selectedYear != year) {
            containerService.selectedYear = year;
            eventsDownloadService.downloadEventsForYear(year);
        }
        containerService.selectedMonth = month;
    }

    $scope.loadYear = function (year) {
        containerService.selectedMonth = null;
        if (containerService.selectedYear != year) {
            containerService.selectedYear = year;
            eventsDownloadService.downloadEventsForYear(year);
        }
        console.log("loadYear");
    }
    //containerService.clearAll();

    $scope.years = function () {
        return containerService.getYearsMonthsList();
    }
    $scope.selectedYear = function () {
        return containerService.selectedYear;
    }
    $scope.selectedMonth = function () {
        return containerService.selectedMonth;
    }

    $scope.$watch(function () {
        return $scope.years();
    }, function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } // AKA first run
        if ($scope.years()) {
            $scope.loadYearMonth($scope.years()[0].year, $scope.years()[0].months[0]);
        }
    }, true);

    if ($scope.years() == null || $scope.years().length == 0) {
        containerService.downloadYearsWithMonths();
    }
    else {
        if ($scope.years()) {
            $scope.loadYearMonth(containerService.selectedYear, containerService.selectedMonth);
            // $rootScope.$broadcast("reloadCurrentEvents", {year: containerService.selectedYear, month: containerService.selectedMonth});
        }
    }


}]);


App.controller('LoginController', [ '$scope', '$rootScope', '$location', '$cookieStore', 'UserService', function ($scope, $rootScope, $location, $cookieStore, UserService) {
    $scope.rememberMe = true;
    $scope.submitting = false;
    $scope.user = {
        username: undefined,
        password: undefined
    }
    $scope.isSubmitDisabled = function () {
        return $scope.user === undefined || $scope.user.username === undefined || $scope.user.password === undefined;
    };


    $scope.login = function () {
        $scope.submitting = true;
        var params = "username=" + $scope.user.username + "&password=" + $scope.user.password;
        UserService.authenticate(params, function (authenticationResult) {
            $scope.submitting = false;
            var authToken = authenticationResult.token;
            $rootScope.authToken = authToken;
            if ($scope.rememberMe) {
                //$cookieStore.put('authToken', authToken);
            }
            UserService.get(function (user) {
                $rootScope.user = user;
                if ($rootScope.backParamURL !== undefined && $rootScope.backParamURL != "/login") {
                    $location.path($rootScope.backParamURL);
                }
                else {
                    $location.path("/index");
                }
            });
        });
    };
}]);

App.controller('TrackListController', [ '$scope', '$modal','trackListService', function ($scope, $modal,trackListService) {
    var self = this;
    $scope.submitNewTrack = false;
    $scope.addTrackError = "";
    $scope.tracks = [];

    $scope.loading = true;
    $scope.infoText = "Loading...";
    $scope.modalInstanceAdd = null;
    $scope.modalInstanceUpdate = null;
    $scope.modalInstanceDelete = null;
    $scope.baseDialogURL = _contextPath + '/layouts/modals/tracks/';

    $scope.openAdd = function () {
        $scope.modalInstanceAdd = $modal.open({
            templateUrl: $scope.baseDialogURL + 'addNewTrackModal.html',
            controller: function ($scope, $modalInstance) {
                $scope.submitNewTrack = false;
                $scope.track = {
                    track_name: "",
                    track_code: "",
                    track_variant: "",
                    id: 0
                };
                $scope.addError = "";
                $scope.buttonAdd = function () {
                    return $scope.submitNewTrack ? "Please wait..." : "Add";
                }
                $scope.existsInList = function () {
                    if ($scope.track.track_name == null) {
                        return false;
                    }
                    return trackListService.existsInList($scope.track.track_name);
                }

                $scope.validEntry = function () {
                    if ($scope.track.track_name == null) {
                        return false;
                    }
                    return $scope.track.track_name.length > 0 && !$scope.existsInList();
                }
                $scope.addTrack = function () {
                    $scope.submitNewTrack = true;
                    trackListService.addTrack($scope.track).then(function (response) {
                        $scope.submitNewTrack = false;
                        if (response.status == 201) {
                            trackListService.addTrackToList(response.data);
                            $scope.submitNewTrack = false;
                            $scope.track = null;
                            $scope.addTrackError = "";
                            $modalInstance.close();
                        }
                        else {
                            $scope.addTrackError = "Unknown status: " + response.status;
                            trackListService.downloadTracks();
                        }
                    }, function (response) {
                        $scope.submitNewTrack = false;
                        $scope.addTrackError = "Error: " + response.statusText;
                    });
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });

        $scope.modalInstanceUpdate.result.then(function (selectedItem) {
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.openEdit = function (trackToEdit) {
        $scope.modalInstanceUpdate = $modal.open({
            templateUrl: $scope.baseDialogURL + 'updateTrackModal.html',
            resolve: {
                track: function () {
                    return trackToEdit;
                }
            },
            controller: function ($scope, $modalInstance, track) {
                $scope.submitUpdateTrack = false;
                $scope.buttonUpdate = function () {
                    return $scope.submitUpdateTrack ? "Please wait..." : "Update";
                }
                $scope.isSame = function () {
                    return track.track_name == $scope.view.name && track.track_code == $scope.view.code && track.track_variant == $scope.view.variant;
                }

                $scope.existsInList = function () {
                    if(track.track_name != $scope.view.name) {
                        return trackListService.existsInList($scope.view.name);
                    }
                    return false;
                }

                $scope.validEntry = function () {
                    if ($scope.view.name == null || $scope.view.name.length == 0) {
                        return false;
                    }

                    var edited = false;
                    if($scope.view.code != track.track_code || $scope.view.variant != track.track_variant)
                    {
                        edited = true;
                    }

                    if($scope.view.name != track.track_name) {
                        return !$scope.existsInList();
                    }
                    return edited;

                    //return !$scope.existsInList() && $scope.view.name.length > 0 && findDiff($scope.view, track);
                }


                function findDiff(edited,original) {
                    var diff = {}
                    for (var key in original) {
                        if (original[key] !== edited[key]) diff[key] = edited[key];
                    }
                    return diff.length > 0;
                }

                $scope.updateError = "";
                $scope.view = {
                    name: track.track_name,
                    code: track.track_code,
                    variant: track.track_variant,
                    editorEnabled: false
                };

                $scope.enableEditor = function () {
                    $scope.view.editorEnabled = true;
                    $scope.view.name = track.track_name;
                    $scope.view.code = track.track_code;
                    $scope.view.variant = track.track_variant;
                };

                $scope.save = function () {
                    var savedTrack = {};
                    savedTrack.track_name = $scope.view.name;
                    savedTrack.track_code = $scope.view.code;
                    savedTrack.track_variant = $scope.view.variant;
                    savedTrack.id = track.id;
                    if (track.track_name != $scope.view.name || track.track_code != $scope.view.code || track.track_variant != $scope.view.variant) {
                        $scope.submitUpdateTrack = true;
                        trackListService.updateTrack(savedTrack).then(function (response) {
                                $scope.submitUpdateTrack = false;
                                if (response.status == 202) {
                                    $scope.updateError = "";
                                    track.track_name = response.data.track_name;
                                    track.track_code = response.data.track_code;
                                    track.track_variant = response.data.track_variant;
                                    $modalInstance.close();
                                }
                                else {
                                    trackListService.downloadTracks();
                                    $modalInstance.close();
                                }
                            }, function (response) {
                                $scope.submitUpdateTrack = false;
                                $scope.updateError = "Error: " + response.statusText;
                            }
                        );
                    }
                    else {
                        $scope.updateError = "Same!";
                    }
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });

        $scope.modalInstanceUpdate.result.then(function (selectedItem) {
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.openDelete = function (trackToDelete) {
        $scope.deleteError = "";
        $scope.modalInstanceDelete = $modal.open({
            templateUrl: $scope.baseDialogURL + 'removeTrackModal.html',
            resolve: {
                track: function () {
                    return trackToDelete;
                }
            },
            controller: function ($scope, $modalInstance, track) {
                $scope.deleteError = "";
                $scope.submitDeleteTrack = false;
                $scope.buttonDelete = function () {
                    return $scope.submitDeleteTrack ? "Please wait..." : "Delete";
                }
                $scope.view = {
                    name: track.track_name,
                    editorEnabled: false
                };

                $scope.enableEditor = function () {
                    $scope.view.editorEnabled = true;
                    $scope.view.name = track.track_name;
                };

                $scope.delete = function () {
                    $scope.submitDeleteTrack = true;
                    var savedTrack = {};
                    savedTrack.track_name = $scope.view.name;
                    savedTrack.id = track.id;
                    trackListService.deleteTrack(savedTrack.id).then(function (response) {
                        $scope.submitDeleteTrack = false;
                        if (response.status == 202) {
                            $scope.updateError = "";
                            if (trackListService.removeTrackFromList(track)) {
                                $modalInstance.close();
                            }
                            else {
                                $scope.deleteError = "Unable to delete track... unknown error...";
                            }
                        }
                        else {
                            trackListService.downloadTracks();
                            $modalInstance.close();
                        }
                    }, function (response) {
                        $scope.submitDeleteTrack = false;
                        $scope.deleteError = "Error: " + response.status + ": " + response.statusText;
                    });
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });

        $scope.modalInstanceDelete.result.then(function (selectedItem) {
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };
    $scope.$watch(function () {
        return trackListService.getTrackList();
    }, function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } // AKA first run
        $scope.loading = false;
        $scope.tracks = trackListService.getTrackList();
    }, true);


    if (trackListService.getTrackList().length > 0) {
        $scope.loading = false;
        $scope.tracks = trackListService.getTrackList();
    }
    else {
        trackListService.downloadTracks();
    }

}]);

App.controller('PlayerListController', [ '$scope', '$modal', 'playerListService', function ($scope, $modal, playerListService) {
    var self = this;
    $scope.submitNewPlayer = false;
    $scope.addPlayerError = "";
    $scope.players = [];

    $scope.loading = true;
    $scope.infoText = "Loading...";
    $scope.modalInstanceAdd = null;
    $scope.modalInstanceUpdate = null;
    $scope.modalInstanceDelete = null;
    $scope.baseDialogURL  =  _contextPath + '/layouts/modals/players/';

    $scope.openAdd = function () {
        $scope.modalInstanceAdd = $modal.open({
            templateUrl: $scope.baseDialogURL + 'addNewPlayerModal.html',
            controller: function ($scope, $modalInstance) {
                $scope.submitNewPlayer = false;
                $scope.player = {
                    player_nick: "",
                    id: 0
                };
                $scope.addError = "";
                $scope.buttonAdd = function () {
                    return $scope.submitNewPlayer ? "Please wait..." : "Add";
                }
                $scope.existsInList = function () {
                    if ($scope.player.player_nick == null) {
                        return false;
                    }
                    return playerListService.existsInList($scope.player.player_nick);
                }

                $scope.validEntry = function () {
                    if ($scope.player.player_nick == null) {
                        return false;
                    }
                    return $scope.player.player_nick.length > 0 && !$scope.existsInList();
                }
                $scope.addPlayer = function () {
                    $scope.submitNewPlayer = true;
                    playerListService.addPlayer($scope.player).then(function (response) {
                        $scope.submitNewPlayer = false;
                        if (response.status == 201) {
                            playerListService.addPlayerToList(response.data);
                            $scope.submitNewPlayer = false;
                            $scope.player = null;
                            $scope.addPlayerError = "";
                            $modalInstance.close();
                        }
                        else {
                            $scope.addPlayerError = "Unknown status: " + response.status;
                            playerListService.downloadPlayers();
                        }
                    }, function (response) {
                        $scope.submitNewPlayer = false;
                        $scope.addPlayerError = "Error: " + response.statusText;
                    });
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });

        $scope.modalInstanceUpdate.result.then(function (selectedItem) {
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.openEdit = function (playerToEdit) {
        $scope.modalInstanceUpdate = $modal.open({
            templateUrl: $scope.baseDialogURL + 'updatePlayerModal.html',
            resolve: {
                player: function () {
                    return playerToEdit;
                }
            },
            controller: function ($scope, $modalInstance, player) {
                $scope.submitUpdatePlayer = false;
                $scope.buttonUpdate = function () {
                    return $scope.submitUpdatePlayer ? "Please wait..." : "Update";
                }
                $scope.isSame = function () {
                    return player.player_nick == $scope.view.nick;
                }

                $scope.existsInList = function () {
                    return playerListService.existsInList($scope.view.nick);
                }

                $scope.validEntry = function () {
                    if ($scope.view.nick == null) {
                        return false;
                    }
                    return !$scope.existsInList() && $scope.view.nick.length > 0;
                }

                $scope.updateError = "";
                $scope.view = {
                    nick: player.player_nick,
                    editorEnabled: false
                };

                $scope.enableEditor = function () {
                    $scope.view.editorEnabled = true;
                    $scope.view.nick = player.player_nick;
                };

                $scope.save = function () {
                    var savedPlayer = {};
                    savedPlayer.player_nick = $scope.view.nick;
                    savedPlayer.id = player.id;
                    if (player.player_nick != $scope.view.nick) {
                        $scope.submitUpdatePlayer = true;
                        playerListService.updatePlayer(savedPlayer).then(function (response) {
                                $scope.submitUpdatePlayer = false;
                                if (response.status == 202) {
                                    $scope.updateError = "";
                                    player.player_nick = response.data.player_nick;
                                    $modalInstance.close();
                                }
                                else {
                                    playerListService.downloadPlayers();
                                    $modalInstance.close();
                                }
                            }, function (response) {
                                $scope.submitUpdatePlayer = false;
                                $scope.updateError = "Error: " + response.statusText;
                            }
                        );
                    }
                    else {
                        $scope.updateError = "Same!";
                    }
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });

        $scope.modalInstanceUpdate.result.then(function (selectedItem) {
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.openDelete = function (playerToDelete) {
        $scope.deleteError = "";
        $scope.modalInstanceDelete = $modal.open({
            templateUrl: $scope.baseDialogURL + 'removePlayerModal.html',
            resolve: {
                player: function () {
                    return playerToDelete;
                }
            },
            controller: function ($scope, $modalInstance, player) {
                $scope.deleteError = "";
                $scope.submitDeletePlayer = false;
                $scope.buttonDelete = function () {
                    return $scope.submitDeletePlayer ? "Please wait..." : "Delete";
                }
                $scope.view = {
                    nick: player.player_nick,
                    editorEnabled: false
                };

                $scope.enableEditor = function () {
                    $scope.view.editorEnabled = true;
                    $scope.view.nick = player.player_nick;
                };

                $scope.delete = function () {
                    $scope.submitDeletePlayer = true;
                    var savedPlayer = {};
                    savedPlayer.player_nick = $scope.view.nick;
                    savedPlayer.id = player.id;
                    playerListService.deletePlayer(savedPlayer.id).then(function (response) {
                        $scope.submitDeletePlayer = false;
                        if (response.status == 202) {
                            $scope.updateError = "";
                            if (playerListService.removePlayerFromList(player)) {
                                $modalInstance.close();
                            }
                            else {
                                $scope.deleteError = "Unable to delete player... unknown error...";
                            }
                        }
                        else {
                            playerListService.downloadPlayers();
                            $modalInstance.close();
                        }
                    }, function (response) {
                        $scope.submitDeletePlayer = false;
                        $scope.deleteError = "Error: " + response.status + ": " + response.statusText;
                    });
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });

        $scope.modalInstanceDelete.result.then(function (selectedItem) {
        }, function () {
            //console.log('Modal dismissed at: ' + new Date());
        });
    };
    $scope.$watch(function () {
        return playerListService.getPlayerList();
    }, function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } // AKA first run
        $scope.loading = false;
        $scope.players = playerListService.getPlayerList();
    }, true);


    if (playerListService.getPlayerList().length > 0) {
        $scope.loading = false;
        $scope.players = playerListService.getPlayerList();
    }
    else {
        playerListService.downloadPlayers();
    }

}]);

/*
App.config(function ($httpProvider) {
    var getTokenData = function () {
        var defaultCsrfTokenHeader = 'X-CSRF-TOKEN';
        var csrfTokenHeaderName = 'X-CSRF-HEADER';
        var xhr = new XMLHttpRequest();
        xhr.open('head', _contextPath, false);
        xhr.send();
        var csrfTokenHeader = xhr.getResponseHeader(csrfTokenHeaderName);
        csrfTokenHeader = csrfTokenHeader ? csrfTokenHeader : defaultCsrfTokenHeader;
        return { headerName: csrfTokenHeader, token: xhr.getResponseHeader(csrfTokenHeader) };
    };
    var csrfTokenData = getTokenData();
    var numRetries = 0;
    var MAX_RETRIES = 5;
    $httpProvider.interceptors.push(function ($q, $injector) {
        return {
            request: function (config) {
                config.headers[csrfTokenData.headerName] = csrfTokenData.token;
                return config || $q.when(config);
            },
            responseError: function (response) {
                if (response.status == 403 && numRetries < MAX_RETRIES) {
                    csrfTokenData = getTokenData();
                    var $http = $injector.get('$http');
                    ++numRetries;
                    return $http(response.config);
                }
                return response;
            },
            response: function(response) {
                // reset number of retries on a successful response
                numRetries = 0;
                return response;
            }
        };
    });
});*/






