package pl.anubis.spring.data.repository.player;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.anubis.spring.model.Player;

public interface PlayerRepository extends JpaRepository<Player, Integer>
{


    @Query(
            value = "SELECT * FROM player WHERE player_nick = :playerNick",
            nativeQuery = true
    )
    public Player findByNick(@Param("playerNick") String playerNick);

}
