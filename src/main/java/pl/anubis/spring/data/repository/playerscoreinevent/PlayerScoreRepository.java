package pl.anubis.spring.data.repository.playerscoreinevent;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.anubis.spring.model.PlayerScoreInRun;

/**
 * Created by Anubis on 22.05.14.
 */
public interface PlayerScoreRepository extends JpaRepository<PlayerScoreInRun, Integer>
{

}
