package pl.anubis.spring.data.repository.secureuser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.anubis.spring.model.Role;
import pl.anubis.spring.model.SecuredUser;
import pl.anubis.spring.model.User;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Anubis on 27.05.14.
 */
@Service
public class RepositoryUserService implements UserService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryUserService.class);

    @Resource
    private UserRepository userRepository;

    @Autowired
    private RoleService roleService;


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User create(String login, String password) throws UserAlreadyExistException
    {
        LOGGER.debug("Creating a new User with information: " + login);

        User user = userRepository.findByUserName(login);
        if(user!=null)
        {
            throw new UserAlreadyExistException();
        }
        User newUser = new User();
        newUser.setUsername(login);
        newUser.setPassword(passwordEncoder.encode(password));

        User created =  this.userRepository.save(newUser);
        Role role = roleService.findById(1);
        created.setRole(role);

        return created;
    }

    @Transactional
    @Override
    public User create(final SecuredUser created) throws UserAlreadyExistException
    {
        LOGGER.debug("Creating a new Player score with information: " + created);

        User userCheck = userRepository.findByUserName(created.getUsername());
        if(userCheck!=null)
        {
            throw new UserAlreadyExistException("User found");
        }
        userCheck = new User();
        userCheck.setUsername(created.getUsername());
        userCheck.setPassword(passwordEncoder.encode(created.getPassword()));
        User user =  this.userRepository.save(userCheck);

        Role role = null;

        if(created.getUsername().equalsIgnoreCase("admin"))
        {
            role = roleService.findById(1);
        }
        else
        {
            role = roleService.findById(2);
        }

        user.setRole(role);

        return user;
    }

    @Transactional(rollbackFor = UsernameNotFoundException.class)
    @Override
    public User delete(final Integer userId) throws UsernameNotFoundException
    {
        LOGGER.debug("Deleting User with id: " + userId);

        User deleted = this.userRepository.findOne(userId);

        if (deleted == null)
        {
            LOGGER.debug("No User found with id: " + userId);
            throw new UsernameNotFoundException("No User found");
        }
        this.userRepository.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true)
    @Override
    public List<User> findAll()
    {
        LOGGER.debug("Finding all Users");
        return this.userRepository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public User findById(final Integer id)
    {
        LOGGER.debug("Finding User by id: " + id);
        return this.userRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    @Override
    public User findByUserName(String login)
    {
        User user = this.userRepository.findByUserName(login);
        return user;
    }

    @Transactional(rollbackFor = UsernameNotFoundException.class)
    @Override
    public User update(final User updated) throws UsernameNotFoundException, UserAlreadyExistException
    {
        LOGGER.debug("Updating User with information: " + updated);

        User userCheck = userRepository.findByUserName(updated.getUsername());
        if(userCheck!=null)
        {
            throw new UserAlreadyExistException();
        }

        final User user = this.userRepository.findOne(updated.getId());

        if (user == null)
        {
            LOGGER.debug("No User found with id: " + updated.getId());
            throw new UsernameNotFoundException("No User found");
        }
        // player.update(updated.getPlayer_nick());
        user.setUsername(updated.getUsername());
        user.setPassword(updated.getPassword());
        return user;
    }
}
