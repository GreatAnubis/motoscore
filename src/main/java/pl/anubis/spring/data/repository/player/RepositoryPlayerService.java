package pl.anubis.spring.data.repository.player;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.anubis.spring.model.Player;
import pl.anubis.spring.mvc.container.json.PlayerData;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RepositoryPlayerService implements PlayerService
{

    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryPlayerService.class);

    @Resource
    private PlayerRepository playerRepository;

    @Transactional
    @Override
    public Player create(final String playerNick)
    {
        LOGGER.debug("Creating a new Player with information: " + playerNick);

        Player track = new Player(playerNick);

        return this.playerRepository.save(track);
    }

    @Transactional
    @Override
    public Player create(final Player created)
    {
        LOGGER.debug("Creating a new Player with information: " + created);

        //Player player = Player.getBuilder(created.getPlayer_nick()).build();

        return this.playerRepository.save(created);
    }

    @Transactional(rollbackFor = PlayerNotFoundException.class)
    @Override
    public Player delete(final Integer trackId) throws PlayerNotFoundException
    {
        LOGGER.debug("Deleting track with id: " + trackId);

        Player deleted = this.playerRepository.findOne(trackId);

        if (deleted == null)
        {
            LOGGER.debug("No Player found with id: " + trackId);
            throw new PlayerNotFoundException();
        }

        this.playerRepository.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Player> findAll()
    {
        LOGGER.debug("Finding all Players");
        return this.playerRepository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public Player findByNick(final String nick)
    {
        LOGGER.debug("Finding track by nick: " + nick);
        return this.playerRepository.findByNick(nick);
    }

    @Transactional(readOnly = true)
    @Override
    public Player findById(final Integer id)
    {
        LOGGER.debug("Finding track by id: " + id);
        return this.playerRepository.findOne(id);
    }

    @Transactional(rollbackFor = PlayerNotFoundException.class)
    @Override
    public Player update(final Player updated) throws PlayerNotFoundException
    {
        LOGGER.debug("Updating player with information: " + updated);

        final Player player = this.playerRepository.findOne(updated.getId());

        if (player == null)
        {
            LOGGER.debug("No track found with id: " + updated.getId());
            throw new PlayerNotFoundException();
        }
        player.update(updated.getPlayer_nick());

        return player;
    }

    @Transactional(rollbackFor = PlayerNotFoundException.class)
    @Override
    public Player update(final PlayerData updated) throws PlayerNotFoundException
    {
        LOGGER.debug("Updating player with information: " + updated);

        final Player player = this.playerRepository.findOne(updated.getId());

        if (player == null)
        {
            LOGGER.debug("No track found with id: " + updated.getId());
            throw new PlayerNotFoundException();
        }
        player.update(updated.getPlayer_nick());

        return player;
    }
}
