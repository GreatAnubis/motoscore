package pl.anubis.spring.data.repository.vehicles;

import pl.anubis.spring.model.Vehicle;

import java.util.List;
import java.util.Set;

/**
 * Created by Anubis on 11.06.14.
 */
public interface VehicleService
{
    /**
     * Creates a new track.
     *
     * @param created   The information of the created track.
     * @return  The created track.
     */
    public Vehicle create(Vehicle created) throws VehicleAlreadyExistsException;

    /**
     * Deletes a track.
     * @param trackId  The id of the deleted track.
     * @return  The deleted track.
     * @throws VehicleNotFoundException  if no track is found with the given id.
     */
    public Vehicle delete(Integer trackId) throws VehicleNotFoundException;

    /**
     * Finds all tracks.
     * @return  A list of tracks.
     */
    public List<Vehicle> findAll();

    /**
     * Finds track by id.
     * @param id    The id of the wanted track.
     * @return  The found track. If no track is found, this method returns null.
     */
    public Vehicle findById(Integer id);

    /**
     *
     * @param name
     * @return
     */
    public Vehicle findByName(final String name);
    /**
     * Updates the information of a track.
     * @param updated   The information of the updated track.
     * @return  The updated track.
     * @throws VehicleNotFoundException  if no track is found with given id.
     */
    public Vehicle update(Vehicle updated) throws VehicleNotFoundException;

    public Set<Vehicle> findAll(Set<Integer> vehicleIDs);
}
