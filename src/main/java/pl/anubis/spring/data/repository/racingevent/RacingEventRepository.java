package pl.anubis.spring.data.repository.racingevent;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.anubis.spring.model.RacingEvent;
import pl.anubis.spring.mvc.container.YearMonths;

import java.util.List;

/**
 * Created by Anubis on 02.05.14.
 */
public interface RacingEventRepository  extends JpaRepository<RacingEvent, Integer>
{
    @Query(
            value = "SELECT * FROM racing_event WHERE visible = 1 ORDER BY event_time DESC",
            nativeQuery = true
    )
    public List<RacingEvent> findAllVisible();

    @Query(
            value = "SELECT * FROM racing_event WHERE visible = 1 AND YEAR(event_time)=:year  ORDER BY event_time DESC",
            nativeQuery = true
    )
    public List<RacingEvent> findAllVisibleFromYear(@Param("year") final Integer year);

    @Query(
            value = "SELECT * FROM racing_event WHERE visible = 1 AND YEAR(event_time)=:year AND MONTH(event_time)=:month  ORDER BY event_time DESC",
            nativeQuery = true
    )
    public List<RacingEvent> findAllVisibleFromYearAndMonth(@Param("year") final Integer year,@Param("month") final Integer month);

    @Query(
            value = "SELECT * FROM racing_event WHERE visible = 1 ORDER BY event_time DESC LIMIT 1",
            nativeQuery = true
    )
    public RacingEvent findLastVisibleFromCurrentYear();

    @Query(
            value = "SELECT DISTINCT YEAR(event_time), MONTH(event_time) FROM racing_event WHERE visible = 1 ORDER BY event_time DESC",
            nativeQuery = true
    )
    public List<Object> findYearMonths();
}
