package pl.anubis.spring.data.repository.track;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.anubis.spring.model.Track;

public interface TrackRepository extends JpaRepository<Track, Integer>
{
    @Query(
            value = "SELECT * FROM track WHERE track_name = :trackName",
            nativeQuery = true
    )
    public Track findByName(@Param("trackName") String trackName);
}
