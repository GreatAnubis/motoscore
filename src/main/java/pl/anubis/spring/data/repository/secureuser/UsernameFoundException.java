package pl.anubis.spring.data.repository.secureuser;

/**
 * Created by Anubis on 14.06.14.
 */
public class UsernameFoundException extends Throwable
{
    public UsernameFoundException(String s)
    {
        super(s);
    }
}
