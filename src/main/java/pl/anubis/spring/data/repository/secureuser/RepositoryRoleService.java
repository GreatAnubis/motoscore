package pl.anubis.spring.data.repository.secureuser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.anubis.spring.model.Role;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Anubis on 27.05.14.
 */
@Service
public class RepositoryRoleService implements RoleService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryRoleService.class);

    @Resource
    private RoleRepository roleRepository;

    @Transactional
    @Override
    public Role create(String strRole)
    {
        LOGGER.debug("Creating a new role with information: " + strRole);

        Role role = new Role();
        role.setRole(strRole);

        return this.roleRepository.save(role);
    }

    @Transactional
    @Override
    public Role create(Role created)
    {
        LOGGER.debug("Creating a new Role with information: " + created);

        return this.roleRepository.save(created);
    }

    @Transactional(rollbackFor = RoleNotFoundException.class)
    @Override
    public Role delete(Integer racingEventId) throws RoleNotFoundException
    {
        LOGGER.debug("Deleting Role with id: " + racingEventId);
        final Role racingEvent = this.roleRepository.findOne(racingEventId);
        if (racingEvent == null)
        {
            LOGGER.debug("No Role found with id: " + racingEventId);
            throw new RoleNotFoundException();
        }
        this.roleRepository.delete(racingEvent);
        return racingEvent;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Role> findAll()
    {
        LOGGER.debug("Finding all Role");
        return this.roleRepository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public Role findById(Integer id)
    {
        LOGGER.debug("Finding Role by id: " + id);
        return this.roleRepository.findOne(id);
    }

    @Transactional(rollbackFor = RoleNotFoundException.class)
    @Override
    public Role update(Role updated) throws RoleNotFoundException
    {
        LOGGER.debug("Updating Role with information: " + updated);

        final Role racingEvent = this.roleRepository.findOne(updated.getId());

        if (racingEvent == null)
        {
            LOGGER.debug("No Role found with id: " + updated.getId());
            throw new RoleNotFoundException();
        }
        //racingEvent.update(updated.getRole());

        return racingEvent;
    }
}
