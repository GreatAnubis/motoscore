package pl.anubis.spring.data.repository.playerscoreinevent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.anubis.spring.data.repository.trackinevent.TrackInEventNotFoundException;
import pl.anubis.spring.data.repository.trackinevent.TrackInEventRepository;
import pl.anubis.spring.model.PlayerScoreInRun;
import pl.anubis.spring.model.TrackInEvent;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Anubis on 22.05.14.
 */
@Service
public class RepositoryPlayerScoreService implements PlayerScoreService
{

    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryPlayerScoreService.class);

    @Resource
    private PlayerScoreRepository playerScoreRepository;


    @Resource
    private TrackInEventRepository trackInEventRepository;



    @Transactional
    @Override
    public PlayerScoreInRun create(final PlayerScoreInRun created) throws TrackInEventNotFoundException, PlayerScoreAlreadyExistsException
    {
        LOGGER.debug("Creating a new Player score with information: " + created);

        //Player player = Player.getBuilder(created.getPlayer_nick()).build();

        TrackInEvent trackInEvent = trackInEventRepository.findOne(created.getTrackInEvent().getId());
        if(trackInEvent==null)
        {
            throw new TrackInEventNotFoundException();
        }

        for(PlayerScoreInRun playerScoreInRunLoop : trackInEvent.getPlayerScoresInRun())
        {
            if(playerScoreInRunLoop.getPlayer().getId() == created.getPlayer().getId())
            {
                throw new PlayerScoreAlreadyExistsException();
            }
        }

        PlayerScoreInRun playerScoreInRun =  this.playerScoreRepository.save(created);
        trackInEvent.getPlayerScoresInRun().add(playerScoreInRun);
        return playerScoreInRun;
    }

    @Transactional(rollbackFor = {PlayerScoreNotFoundException.class,TrackInEventNotFoundException.class})
    @Override
    public PlayerScoreInRun delete(final Integer playerScoreId) throws PlayerScoreNotFoundException, TrackInEventNotFoundException
    {
        LOGGER.debug("Deleting player score with id: " + playerScoreId);

        PlayerScoreInRun deleted = this.playerScoreRepository.findOne(playerScoreId);

        if (deleted == null)
        {
            LOGGER.debug("No Player score found with id: " + playerScoreId);
            throw new PlayerScoreNotFoundException();
        }

        if(!deleted.getTrackInEvent().getPlayerScoresInRun().contains(deleted))
        {
            throw new TrackInEventNotFoundException();
        }

        deleted.getTrackInEvent().getPlayerScoresInRun().remove(deleted);
        this.playerScoreRepository.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true)
    @Override
    public List<PlayerScoreInRun> findAll()
    {
        LOGGER.debug("Finding all Players");
        return this.playerScoreRepository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public PlayerScoreInRun findById(final Integer id)
    {
        LOGGER.debug("Finding track by id: " + id);
        return this.playerScoreRepository.findOne(id);
    }

    @Transactional(rollbackFor = PlayerScoreNotFoundException.class)
    @Override
    public PlayerScoreInRun update(final PlayerScoreInRun updated) throws PlayerScoreNotFoundException
    {
        LOGGER.debug("Updating player with information: " + updated);

        final PlayerScoreInRun playerScore = this.playerScoreRepository.findOne(updated.getId());

        if (playerScore == null)
        {
            LOGGER.debug("No track found with id: " + updated.getId());
            throw new PlayerScoreNotFoundException();
        }
      // player.update(updated.getPlayer_nick());

        return playerScore;
    }

    @Transactional(rollbackFor = PlayerScoreNotFoundException.class)
    @Override
    public PlayerScoreInRun updateScore(final PlayerScoreInRun updated) throws PlayerScoreNotFoundException
    {
        LOGGER.debug("Updating player with information: " + updated);

        final PlayerScoreInRun playerScore = this.playerScoreRepository.findOne(updated.getId());

        if (playerScore == null)
        {
            LOGGER.debug("No track found with id: " + updated.getId());
            throw new PlayerScoreNotFoundException();
        }
        // player.update(updated.getPlayer_nick());

        playerScore.setScore(updated.getScore());

        return playerScore;
    }


    public void getTopScoresForMonth()
    {
        //SELECT pl.player_nick, pscor.player_id, SUM(pscor.score) FROM player_score_in_run pscor LEFT JOIN player pl ON pl.id = pscor.player_id WHERE pscor.id IN( SELECT psir.player_score_in_run_id FROM player_scores_in_run psir WHERE psir.track_in_event_id IN (  SELECT tie.track_in_event_id FROM tracks_in_event tie WHERE tie.racing_event_id in( SELECT  re.id FROM motoscore.racing_event re where re.event_time BETWEEN '2014-05-01 00:00:00' AND '2014-06-01 00:00:00'))) GROUP BY player_id ORDER by SUM(pscor.score) DESC
        

    }
}