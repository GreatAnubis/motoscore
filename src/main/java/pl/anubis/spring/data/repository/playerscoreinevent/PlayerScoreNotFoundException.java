package pl.anubis.spring.data.repository.playerscoreinevent;

/**
 * Created by Anubis on 22.05.14.
 */
public class PlayerScoreNotFoundException extends Exception
{
    public PlayerScoreNotFoundException()
    {
        super("Player Score Not Found!");
    }
    public PlayerScoreNotFoundException(final String message)
    {
        super(message);
    }
}
