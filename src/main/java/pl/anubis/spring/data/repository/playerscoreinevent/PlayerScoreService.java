package pl.anubis.spring.data.repository.playerscoreinevent;

import pl.anubis.spring.data.repository.trackinevent.TrackInEventNotFoundException;
import pl.anubis.spring.model.PlayerScoreInRun;

import java.util.List;

/**
 * Created by Anubis on 22.05.14.
 */
public interface PlayerScoreService
{
    /**
     * Creates a new track.
     * @param created   The information of the created track.
     * @return  The created track.
     */
    public PlayerScoreInRun create(PlayerScoreInRun created) throws TrackInEventNotFoundException, PlayerScoreAlreadyExistsException;

    /**
     * Deletes a track.
     * @param trackId  The id of the deleted track.
     * @return  The deleted track.
     * @throws PlayerScoreNotFoundException  if no track is found with the given id.
     */
    public PlayerScoreInRun delete(Integer trackId) throws PlayerScoreNotFoundException, TrackInEventNotFoundException;

    /**
     * Finds all tracks.
     * @return  A list of tracks.
     */
    public List<PlayerScoreInRun> findAll();

    /**
     * Finds track by id.
     * @param id    The id of the wanted track.
     * @return  The found track. If no track is found, this method returns null.
     */
    public PlayerScoreInRun findById(Integer id);

    /**
     * Updates the information of a track.
     * @param updated   The information of the updated track.
     * @return  The updated track.
     * @throws PlayerScoreNotFoundException  if no track is found with given id.
     */
    public PlayerScoreInRun update(PlayerScoreInRun updated) throws PlayerScoreNotFoundException;

    public PlayerScoreInRun updateScore(PlayerScoreInRun playerScoreInRun) throws PlayerScoreNotFoundException;
}
