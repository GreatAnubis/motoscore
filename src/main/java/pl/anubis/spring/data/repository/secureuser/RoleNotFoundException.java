package pl.anubis.spring.data.repository.secureuser;

/**
 * Created by Anubis on 27.05.14.
 */
public class RoleNotFoundException extends Exception
{
    public RoleNotFoundException(final String message)
    {
        super(message);
    }
    public RoleNotFoundException()
    {
        super();
    }
}
