package pl.anubis.spring.data.repository.secureuser;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pl.anubis.spring.model.SecuredUser;
import pl.anubis.spring.model.User;

import java.util.List;

/**
 * Created by Anubis on 27.05.14.
 */
public interface UserService
{
    public User create(final String login,final String password)  throws UserAlreadyExistException;
    /**
     * Creates a new User.
     * @param created   The information of the created User.
     * @return  The created User.
     */
    public User create(SecuredUser created)  throws UserAlreadyExistException;

    /**
     * Deletes a User.
     * @param userId  The id of the deleted user.
     * @return  The deleted user.
     * @throws UserNotFoundException  if no user is found with the given id.
     */
    public User delete(Integer userId) throws UsernameNotFoundException;

    /**
     * Finds all Users.
     * @return  A list of Users.
     */
    public List<User> findAll();

    /**
     * Finds User by id.
     * @param id    The id of the wanted User.
     * @return  The found User. If no User is found, this method returns null.
     */
    public User findById(Integer id);

    /**
     *
     * @param username
     * @return the user.
     */
    public User findByUserName(final String username);
    /**
     * Updates the information of a User.
     * @param updated   The information of the updated User.
     * @return  The updated User.
     * @throws UserNotFoundException  if no User is found with given id.
     */
    public User update(User updated) throws UsernameNotFoundException, UserAlreadyExistException;
}
