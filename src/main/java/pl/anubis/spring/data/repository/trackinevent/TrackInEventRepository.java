package pl.anubis.spring.data.repository.trackinevent;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.anubis.spring.model.TrackInEvent;

/**
 * Created by Anubis on 20.05.14.
 */
public interface TrackInEventRepository  extends JpaRepository<TrackInEvent, Integer>
{

}
