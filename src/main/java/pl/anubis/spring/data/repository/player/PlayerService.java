package pl.anubis.spring.data.repository.player;

import java.util.List;

import pl.anubis.spring.model.Player;
import pl.anubis.spring.mvc.container.json.PlayerData;

public interface PlayerService 
{
	public Player create(String playerNick);
    /**
     * Creates a new Player.
     * @param created rThe information of the created Player.
     * @return  The created Player.
     */
    public Player create(Player created);

    /**
     * Deletes a Player.
     * @param PlayerId The id of the deleted track.
     * @return The deleted Player.
     * @throws PlayerNotFoundException  if no Player is found with the given id.
     */
    public Player delete(Integer playerId) throws PlayerNotFoundException;

    /**
     * Finds all Players.
     * @return  A list of Players.
     */
    public List<Player> findAll();

    /**
     * Finds Player by id.
     * @param id    The id of the wanted Player.
     * @return  The found Player. If no Player is found, this method returns null.
     */
    public Player findById(Integer id);

    /**
     *
     * @param nick
     * @return
     */
    public Player findByNick(final String nick);

    /**
     * Updates the information of a Player.
     * @param updated   The information of the updated Player.
     * @return  The updated Player.
     * @throws PlayerNotFoundException  if no Player is found with given id.
     */
    public Player update(Player updated) throws PlayerNotFoundException;

    public Player update(PlayerData updated) throws PlayerNotFoundException;



}
