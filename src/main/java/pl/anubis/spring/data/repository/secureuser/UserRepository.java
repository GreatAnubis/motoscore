package pl.anubis.spring.data.repository.secureuser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.anubis.spring.model.User;

/**
 * Created by Anubis on 27.05.14.
 */
public interface UserRepository  extends JpaRepository<User, Integer>
{
    @Query(
            value = "SELECT DISTINCT * FROM users WHERE username = :username",
            nativeQuery = true
    )
    public User findByUserName(@Param("username") String username);

}
