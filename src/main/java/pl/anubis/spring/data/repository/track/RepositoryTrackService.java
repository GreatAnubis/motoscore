package pl.anubis.spring.data.repository.track;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.anubis.spring.model.Track;
import pl.anubis.spring.mvc.container.json.TrackInfo;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RepositoryTrackService implements TrackService
{

    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryTrackService.class);
    
    @Resource
    private TrackRepository trackRepository;
    
    @Transactional
    @Override
    public Track create(final String imageLink, final String trackName)
    {
        LOGGER.debug("Creating a new Track with information: " + imageLink + " / " + trackName);
        Track track = Track.getBuilder(trackName).build();
        return trackRepository.save(track);
    }
    
    @Transactional
    @Override
    public Track create(Track created)
    {
        LOGGER.debug("Creating a new Track with information: " + created);
        return trackRepository.save(created);
    }

    @Transactional(rollbackFor = TrackNotFoundException.class)
    @Override
    public Track delete(Integer trackId) throws TrackNotFoundException
    {
        LOGGER.debug("Deleting track with id: " + trackId);
        Track deleted = trackRepository.findOne(trackId);
        if (deleted == null)
        {
            LOGGER.debug("No track found with id: " + trackId);
            throw new TrackNotFoundException();
        }
        
        trackRepository.delete(deleted);
        return deleted;
	}

    @Transactional(readOnly = true)
    @Override
    public Track findByName(final String name)
    {
        LOGGER.debug("Finding track by name: " + name);
        return this.trackRepository.findByName(name);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Track> findAll() 
    {
        LOGGER.debug("Finding all tracks");
        return trackRepository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public Track findById(Integer id)
    {
        LOGGER.debug("Finding track by id: " + id);
        return trackRepository.findOne(id);
    }
    
    @Transactional(rollbackFor = TrackNotFoundException.class)
	@Override
	public Track update(Track updated) throws TrackNotFoundException {
        LOGGER.debug("Updating track with information: " + updated);
        Track track = trackRepository.findOne(updated.getId());
        
        if (track == null) 
        {
            LOGGER.debug("No track found with id: " + updated.getId());
            throw new TrackNotFoundException();
        }
       // track.setImage_link(updated.getImage_link());
        track.setTrack_name(updated.getTrack_name());
        track.setTrack_code(updated.getTrack_code());

        return track;
	}

    @Transactional
    @Override
    public Track create(final TrackInfo trackForm)
    {
        LOGGER.debug("Creating a new Track with information: " + trackForm.getTrack_code() + " / " + trackForm.getTrack_name());
        Track track = Track.getBuilder(trackForm).build();
        return trackRepository.save(track);
    }

    @Transactional(rollbackFor = TrackNotFoundException.class)
    @Override
    public Track update(final TrackInfo updated) throws TrackNotFoundException
    {
        LOGGER.debug("Updating player with information: " + updated);

        final Track track = this.trackRepository.findOne(updated.getId());

        if (track == null)
        {
            LOGGER.debug("No track found with id: " + updated.getId());
            throw new TrackNotFoundException();
        }
        track.update(updated);

        return track;
    }

}
