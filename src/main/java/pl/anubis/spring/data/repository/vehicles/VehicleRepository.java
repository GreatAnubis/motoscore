package pl.anubis.spring.data.repository.vehicles;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.anubis.spring.model.Vehicle;

/**
 * Created by Anubis on 11.06.14.
 */
public interface VehicleRepository  extends JpaRepository<Vehicle, Integer>
{

    @Query(
            value = "SELECT * FROM vehicle WHERE vehicle_name = :vehicle_name",
            nativeQuery = true
    )
    Vehicle findByName(@Param("vehicle_name") String vehicle_name);
}
