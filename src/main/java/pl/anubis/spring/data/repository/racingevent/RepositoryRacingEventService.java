package pl.anubis.spring.data.repository.racingevent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.anubis.spring.data.repository.trackinevent.TrackInEventService;
import pl.anubis.spring.model.RacingEvent;
import pl.anubis.spring.model.TrackInEvent;
import pl.anubis.spring.mvc.container.YearMonths;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Anubis on 02.05.14.
 */
@Service
public class RepositoryRacingEventService implements RacingEventService
{

    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryRacingEventService.class);

    @Resource
    private RacingEventRepository racingEventRepository;

    @Autowired
    private TrackInEventService repositoryTrackInEventService;

    @Transactional
    @Override
    public RacingEvent create(Date timestamp, String movielink) throws ParseException
    {
        LOGGER.debug("Creating a new RacingEvent with information: " + timestamp + " Movie: " + movielink);

        RacingEvent racingEvent = RacingEvent.getBuilder(timestamp, movielink).build();

        return this.racingEventRepository.save(racingEvent);
    }

    @Transactional
    @Override
    public RacingEvent create(RacingEvent created) throws ParseException
    {
        LOGGER.debug("Creating a new RacingEvent with information: " + created);

        return this.racingEventRepository.save(created);
    }

    @Transactional(rollbackFor = RacingEventNotFoundException.class)
    @Override
    public RacingEvent delete(Integer racingEventId) throws RacingEventNotFoundException
    {
        LOGGER.debug("Deleting RacingEvent with id: " + racingEventId);

        RacingEvent racingEvent = this.racingEventRepository.findOne(racingEventId);

        if (racingEvent == null)
        {
            LOGGER.debug("No Player found with id: " + racingEventId);
            throw new RacingEventNotFoundException();
        }

        List<TrackInEvent> tracksInEvent = new ArrayList<TrackInEvent>(racingEvent.getTracks());
        for(TrackInEvent trie : tracksInEvent)
        {
            repositoryTrackInEventService.delete(racingEvent.getId(),trie.getId());
        }
        racingEvent.getTracks().removeAll(tracksInEvent);
        tracksInEvent.clear();

        this.racingEventRepository.delete(racingEvent);

        return racingEvent;
    }

    @Transactional(readOnly = true)
    @Override
    public List<RacingEvent> findAll()
    {
        LOGGER.debug("Finding all RacingEvents");
        return this.racingEventRepository.findAll(new Sort(Sort.Direction.DESC, "timestamp"));
    }

    @Transactional(readOnly = true)
    @Override
    public List<RacingEvent> findAllVisible()
    {
        LOGGER.debug("Finding all RacingEvents");
        return this.racingEventRepository.findAllVisible();
    }

    @Transactional(readOnly = true)
    @Override
    public List<RacingEvent> findAllVisibleFromYearAndMonth(final Integer year, final Integer month)
    {
        if(month == null)
        {
            LOGGER.debug("Finding all findAllVisibleFromYearAndMonth: YEAR ONLY : " + year);
            return this.racingEventRepository.findAllVisibleFromYear(year);
        }
        else
        {
            LOGGER.debug("Finding all findAllVisibleFromYearAndMonth: YEAR : " + year + " MONTH: " + month);
            return this.racingEventRepository.findAllVisibleFromYearAndMonth(year,month);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public List<RacingEvent> findAllVisibleFromYear(final Integer year)
    {
        LOGGER.debug("Finding all findAllVisibleFromYear: YEAR ONLY : " + year);
        return this.racingEventRepository.findAllVisibleFromYear(year);
    }

    @Override
    public RacingEvent findLastVisibleFromCurrentYear()
    {
        LOGGER.debug("Finding all findLastVisibleFromCurrentYear");
        return this.racingEventRepository.findLastVisibleFromCurrentYear();
    }

    @Transactional(readOnly = true)
    @Override
    public RacingEvent findById(Integer id)
    {
        LOGGER.debug("Finding RacingEvent by id: " + id);
        RacingEvent racing = this.racingEventRepository.findOne(id);
        //if(racing != null)
        //{
         //   Hibernate.initialize(racing.getTracks());
        //    List<TrackInEvent> orders = racing.getTracks();
        //}
        return racing;
    }

    @Transactional(rollbackFor = RacingEventNotFoundException.class)
    @Override
    public RacingEvent update(RacingEvent updated) throws RacingEventNotFoundException, ParseException
    {
        LOGGER.debug("Updating RacingEvent with information: " + updated);

        final RacingEvent racingEvent = this.racingEventRepository.findOne(updated.getId());

        if (racingEvent == null)
        {
            LOGGER.debug("No RacingEvent found with id: " + updated.getId());
            throw new RacingEventNotFoundException();
        }
        racingEvent.update(updated.getTimestamp(), updated.getMovieLink());

        return racingEvent;
    }

    @Transactional(rollbackFor = RacingEventNotFoundException.class)
    @Override
    public RacingEvent updateAll(RacingEvent updated) throws RacingEventNotFoundException, ParseException
    {
        LOGGER.debug("Updating RacingEvent with information: " + updated);

        final RacingEvent racingEvent = this.racingEventRepository.findOne(updated.getId());

        if (racingEvent == null)
        {
            LOGGER.debug("No RacingEvent found with id: " + updated.getId());
            throw new RacingEventNotFoundException();
        }
        racingEvent.update(updated.getTimestamp(), updated.getMovieLink());
        racingEvent.setVisible(updated.isVisible());

        for(TrackInEvent currentTrackInEvent : racingEvent.getTracks())
        {
            for(TrackInEvent trackInEvent : updated.getTracks())
            {
               // try
               // {
                    if(currentTrackInEvent.getId() == trackInEvent.getId())
                    {
                        currentTrackInEvent.setOrdering(trackInEvent.getOrdering());
                        //repositoryTrackInEventService.update(currentTrackInEvent);
                    }
              //  }
               // catch (TrackInEventNotFoundException e)
               // {
               //     e.printStackTrace();
               // }
            }
        }
        return racingEvent;

    }


    public List<YearMonths> getRacingEventsYearMonths()
    {
        final List<Object> yearMonths = this.racingEventRepository.findYearMonths();
        final List<YearMonths> result = new ArrayList<YearMonths>();

        for(Object oym : yearMonths)
        {
            final Object[] aroym = (Object[])oym;
            final Integer Year = (Integer)aroym[0];
            final Integer Month = (Integer)aroym[1];
            boolean bFound = false;
            for(YearMonths rym : result)
            {
                if(rym.getYear().intValue() == Year.intValue())
                {
                    rym.addMonth(Month);
                    bFound = true;
                    break;
                }
            }
            if(!bFound)
            {
                result.add(new YearMonths(Year, Month));
            }
        }
        return result;
    }
}
