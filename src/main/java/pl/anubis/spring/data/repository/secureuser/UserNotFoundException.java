package pl.anubis.spring.data.repository.secureuser;

/**
 * Created by Anubis on 27.05.14.
 */
public class UserNotFoundException extends Exception
{
    public UserNotFoundException(final String message)
    {
        super(message);
    }

    public UserNotFoundException()
    {
        super();
    }

}
