package pl.anubis.spring.data.repository.track;

import pl.anubis.spring.model.Track;
import pl.anubis.spring.mvc.container.json.TrackInfo;

import java.util.List;

public interface TrackService 
{
	public Track create(String imageLink, String trackName);
    /**
     * Creates a new track.
     * @param created   The information of the created track.
     * @return  The created track.
     */
    public Track create(Track created);

    /**
     * Deletes a track.
     * @param trackId  The id of the deleted track.
     * @return  The deleted track.
     * @throws TrackNotFoundException  if no track is found with the given id.
     */
    public Track delete(Integer trackId) throws TrackNotFoundException;

    /**
     * Finds all tracks.
     * @return  A list of tracks.
     */
    public List<Track> findAll();

    /**
     * Finds track by id.
     * @param id    The id of the wanted track.
     * @return  The found track. If no track is found, this method returns null.
     */
    public Track findById(Integer id);

    /**
     *
     * @param name
     * @return
     */
    public Track findByName(final String name);
    /**
     * Updates the information of a track.
     * @param updated   The information of the updated track.
     * @return  The updated track.
     * @throws TrackNotFoundException  if no track is found with given id.
     */
    public Track update(Track updated) throws TrackNotFoundException;

    public Track create(TrackInfo trackForm);

    public Track update(TrackInfo trackForm) throws TrackNotFoundException;
}
