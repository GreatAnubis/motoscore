package pl.anubis.spring.data.repository.secureuser;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.anubis.spring.model.Role;

/**
 * Created by Anubis on 27.05.14.
 */
public interface RoleRepository  extends JpaRepository<Role, Integer>
{

}
