package pl.anubis.spring.data.repository.trackinevent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.anubis.spring.data.repository.playerscoreinevent.PlayerScoreRepository;
import pl.anubis.spring.data.repository.racingevent.RacingEventNotFoundException;
import pl.anubis.spring.data.repository.racingevent.RacingEventRepository;
import pl.anubis.spring.data.repository.vehicles.VehicleRepository;
import pl.anubis.spring.model.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.*;

/**
 * Created by Anubis on 20.05.14.
 */
@Service
public class RepositoryTrackInEventService implements TrackInEventService
{

    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryTrackInEventService.class);

    @Resource
    private TrackInEventRepository trackInEventRepository;

    @Autowired
    private RacingEventRepository racingEventService;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private PlayerScoreRepository playerScoreService;

    @Transactional
    @Override
    public TrackInEvent create(Integer racingEventId,Track track, Integer ordering) throws ParseException, RacingEventNotFoundException
    {
        LOGGER.debug("Creating a new trackInEvent with trackid: " + track.getId() + " track name: " + track.getTrack_name() + " Ordering: " + ordering);

        RacingEvent racingEvent = racingEventService.findOne(racingEventId);
        TrackInEvent trackInEvent = new TrackInEvent(track, ordering);

        trackInEvent = this.trackInEventRepository.save(trackInEvent);
        racingEvent.getTracks().add(trackInEvent);
        //racingEventService.update(racingEvent);
        return trackInEvent;
    }

    @Transactional
    @Override
    public TrackInEvent create(Integer racingEventId,TrackInEvent created) throws ParseException, RacingEventNotFoundException
    {
        LOGGER.debug("Creating a new RacingEvent with information: " + created);
        RacingEvent racingEvent = racingEventService.findOne(racingEventId);
        TrackInEvent trackInEvent = this.trackInEventRepository.save(created);
        racingEvent.getTracks().add(trackInEvent);
        //racingEventService.update(racingEvent);
        return trackInEvent;
    }

    @Override
    @Transactional
    public TrackInEvent delete(Integer racingEventId, Integer trackInEvent_id)
    {
        RacingEvent rev = racingEventService.findOne(racingEventId);
        TrackInEvent trackInEvent = this.trackInEventRepository.findOne(trackInEvent_id);
        rev.getTracks().remove(trackInEvent);
        Set<PlayerScoreInRun> spsir= trackInEvent.getPlayerScoresInRun();
        for(PlayerScoreInRun psir : trackInEvent.getPlayerScoresInRun())
        {
            this.playerScoreService.delete(psir);
        }
        trackInEvent.getPlayerScoresInRun().removeAll(spsir);
        trackInEvent.getPlayerScoresInRun().clear();
        List<Vehicle> veh = new ArrayList<Vehicle>(trackInEvent.getVehiclesInRun());
        trackInEvent.getVehiclesInRun().removeAll(veh);
        //for(Vehicle vsir : veh)
//        {
        //    trackInEvent.getVehiclesInRun().remove(vsir);
//        }

        this.trackInEventRepository.delete(trackInEvent);
        return trackInEvent;
    }


    @Transactional(rollbackFor = TrackInEventNotFoundException.class)
    @Override
    public TrackInEvent delete(Integer trackInEvent_id) throws TrackInEventNotFoundException
    {
        LOGGER.debug("Deleting RacingEvent with id: " + trackInEvent_id);

        TrackInEvent trackInEvent = this.trackInEventRepository.findOne(trackInEvent_id);

        if (trackInEvent == null)
        {
            LOGGER.debug("No TrackInEventEvent found with id: " + trackInEvent_id);
            throw new TrackInEventNotFoundException();
        }

        for(PlayerScoreInRun psir : trackInEvent.getPlayerScoresInRun())
        {
            psir.getTrackInEvent().getPlayerScoresInRun().remove(psir);
            this.playerScoreService.delete(psir);
        }
        trackInEvent.getPlayerScoresInRun().clear();
        List<Vehicle> veh = new ArrayList<Vehicle>(trackInEvent.getVehiclesInRun());
        for(Vehicle vsir : veh)
        {
            trackInEvent.getVehiclesInRun().remove(vsir);
        }
        trackInEvent.getVehiclesInRun().clear();
        //this.vehicleRepository.delete(trackInEvent.getVehiclesInRun());

        this.trackInEventRepository.delete(trackInEvent);
        return trackInEvent;
    }

    @Transactional(readOnly = true)
    @Override
    public List<TrackInEvent> findAll()
    {
        LOGGER.debug("Finding all RacingEvents");
        return sortByOrdering(this.trackInEventRepository.findAll());
    }

    private List<TrackInEvent> sortByOrdering(List<TrackInEvent> trackInevent)
    {
        if(trackInevent!=null)
        {
            Collections.sort(trackInevent, new Comparator<TrackInEvent>()
            {
                public int compare(TrackInEvent o1, TrackInEvent o2)
                {
                    Integer i1 = o1.getOrdering();
                    Integer i2 = o2.getOrdering();
                    return (i1 > i2 ? -1 : (i1 == i2 ? 0 : 1));
                }
            });
        }
        return trackInevent;
    }

    @Transactional(readOnly = true)
    @Override
    public TrackInEvent findById(Integer id)
    {
        LOGGER.debug("Finding RacingEvent by id: " + id);
        return this.trackInEventRepository.findOne(id);
    }

    @Transactional(rollbackFor = TrackInEventNotFoundException.class)
    @Override
    public TrackInEvent update(TrackInEvent updated) throws TrackInEventNotFoundException
    {
        LOGGER.debug("Updating RacingEvent with information: " + updated);

        final TrackInEvent trackInEvent = this.trackInEventRepository.findOne(updated.getId());

        if (trackInEvent == null)
        {
            LOGGER.debug("No RacingEvent found with id: " + updated.getId());
            throw new TrackInEventNotFoundException();
        }
        trackInEvent.setOrdering(updated.getOrdering());
        trackInEvent.setPlayerScoresInRun(updated.getPlayerScoresInRun());
        //trackInEvent.setVehiclesInRun(updated.getVehiclesInRun());

        return trackInEvent;
    }

    @Transactional(rollbackFor = TrackInEventNotFoundException.class)
    @Override
    public TrackInEvent updateTrack(TrackInEvent updated) throws TrackInEventNotFoundException
    {
        LOGGER.debug("Updating RacingEvent with information: " + updated);

        final TrackInEvent trackInEvent = this.trackInEventRepository.findOne(updated.getId());

        if (trackInEvent == null)
        {
            LOGGER.debug("No RacingEvent found with id: " + updated.getId());
            throw new TrackInEventNotFoundException();
        }
        trackInEvent.setTrack(updated.getTrack());

        return trackInEvent;
    }


    @Transactional(rollbackFor = TrackInEventNotFoundException.class)
    @Override
    public TrackInEvent updateVehicles(TrackInEvent updated) throws TrackInEventNotFoundException
    {
        LOGGER.debug("Updating RacingEvent with information: " + updated);

        final TrackInEvent trackInEvent = this.trackInEventRepository.findOne(updated.getId());

        if (trackInEvent == null)
        {
            LOGGER.debug("No RacingEvent found with id: " + updated.getId());
            throw new TrackInEventNotFoundException();
        }

        Set<Integer> vehicleIDs = new HashSet<Integer>();
        Set<Vehicle> vehicleSet = new HashSet<Vehicle>();
        if(updated.getVehiclesInRun() != null)
        {
            for(Vehicle veh : updated.getVehiclesInRun())
            {
                vehicleIDs.add(veh.getId());
            }

            vehicleSet = new HashSet<Vehicle>(vehicleRepository.findAll(vehicleIDs));
        }

        trackInEvent.setVehiclesInRun(vehicleSet);
        return trackInEvent;
    }


    @Transactional(rollbackFor = TrackInEventNotFoundException.class)
    @Override
    public TrackInEvent updateOrdering(TrackInEvent updated) throws TrackInEventNotFoundException
    {
        LOGGER.debug("Updating RacingEvent with information: " + updated);

        final TrackInEvent trackInEvent = this.trackInEventRepository.findOne(updated.getId());

        if (trackInEvent == null)
        {
            LOGGER.debug("No RacingEvent found with id: " + updated.getId());
            throw new TrackInEventNotFoundException();
        }
        trackInEvent.setOrdering(updated.getOrdering());
        return trackInEvent;
    }
}
