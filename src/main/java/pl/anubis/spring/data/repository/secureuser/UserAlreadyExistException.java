package pl.anubis.spring.data.repository.secureuser;

/**
 * Created by Anubis on 27.05.14.
 */
public class UserAlreadyExistException extends Throwable
{
    public UserAlreadyExistException(String s)
    {
        super(s);
    }

    public UserAlreadyExistException()
    {

    }
}
