package pl.anubis.spring.data.repository.vehicles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.anubis.spring.model.Vehicle;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Anubis on 11.06.14.
 */
@Service
public class RepositoryVehicleService implements VehicleService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryVehicleService.class);

    @Resource
    private VehicleRepository vehicleRepository;

    @Override
    @Transactional
    public Vehicle create(Vehicle created) throws VehicleAlreadyExistsException
    {
        Vehicle vehCheck = vehicleRepository.findByName(created.getVehicle_name());
        if(vehCheck!=null)
        {
            throw new VehicleAlreadyExistsException();
        }
        Vehicle newEntity = vehicleRepository.save(created);
        return newEntity;
    }

    @Override
    @Transactional(rollbackFor = VehicleNotFoundException.class)
    public Vehicle delete(Integer vehicleId) throws VehicleNotFoundException
    {
        LOGGER.debug("Deleting vehicle with id: " + vehicleId);

        Vehicle deleted = vehicleRepository.findOne(vehicleId);

        if (deleted == null) {
            LOGGER.debug("No vehicle found with id: " + vehicleId);
            throw new VehicleNotFoundException();
        }

        vehicleRepository.delete(deleted);
        return deleted;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Vehicle> findAll()
    {
        return vehicleRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Vehicle findById(Integer id)
    {
        return vehicleRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Vehicle findByName(String name)
    {
        return vehicleRepository.findByName(name);
    }

    @Override
    @Transactional(rollbackFor = VehicleNotFoundException.class)
    public Vehicle update(Vehicle updated) throws VehicleNotFoundException
    {
        LOGGER.debug("Updating Vehicle with information: " + updated);
        final Vehicle vehicle = vehicleRepository.findOne(updated.getId());
        if (vehicle == null)
        {
            LOGGER.debug("No vehicle found with id: " + updated.getId());
            throw new VehicleNotFoundException();
        }
        vehicle.setVehicle_name(updated.getVehicle_name());
        vehicle.setVehicle_code(updated.getVehicle_code());
        return vehicle;
    }

    @Override
    @Transactional
    public Set<Vehicle> findAll(Set<Integer> vehicleIDs)
    {
        return new HashSet<Vehicle>(vehicleRepository.findAll(vehicleIDs));
    }
}
