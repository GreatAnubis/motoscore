package pl.anubis.spring.data.repository.racingevent;

import pl.anubis.spring.model.RacingEvent;
import pl.anubis.spring.mvc.container.YearMonths;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by Anubis on 02.05.14.
 */
public interface RacingEventService
{
    public RacingEvent create(Date timestamp, String movielink) throws ParseException;
    /**
     * Creates a new track.
     * @param created   The information of the created track.
     * @return  The created track.
     */
    public RacingEvent create(RacingEvent created) throws ParseException;

    /**
     * Deletes a track.
     * @param trackId  The id of the deleted track.
     * @return  The deleted track.
     * @throws pl.anubis.spring.data.repository.track.TrackNotFoundException  if no track is found with the given id.
     */
    public RacingEvent delete(Integer trackId) throws RacingEventNotFoundException;

    /**
     * Finds all tracks.
     * @return  A list of tracks.
     */
    public List<RacingEvent> findAll();

    public List<RacingEvent> findAllVisible();

    public List<RacingEvent> findAllVisibleFromYearAndMonth(final Integer year, final Integer month);

    public List<RacingEvent> findAllVisibleFromYear(final Integer year);

    public RacingEvent findLastVisibleFromCurrentYear();

    public List<YearMonths> getRacingEventsYearMonths();

    /**
     * Finds track by id.
     * @param id    The id of the wanted track.
     * @return  The found track. If no track is found, this method returns null.
     */
    public RacingEvent findById(Integer id);

    /**
     * Updates the information of a track.
     * @param updated   The information of the updated track.
     * @return  The updated track.
     * @throws pl.anubis.spring.data.repository.track.TrackNotFoundException  if no track is found with given id.
     */
    public RacingEvent update(RacingEvent updated) throws RacingEventNotFoundException, ParseException;

    public RacingEvent updateAll(RacingEvent updated) throws RacingEventNotFoundException, ParseException;

}