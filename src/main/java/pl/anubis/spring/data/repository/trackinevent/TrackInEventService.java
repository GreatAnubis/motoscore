package pl.anubis.spring.data.repository.trackinevent;

import pl.anubis.spring.data.repository.racingevent.RacingEventNotFoundException;
import pl.anubis.spring.model.Track;
import pl.anubis.spring.model.TrackInEvent;

import java.text.ParseException;
import java.util.List;

/**
 * Created by Anubis on 20.05.14.
 */
public interface TrackInEventService
{
    public TrackInEvent create(Integer racingEventId, Track track, Integer ordering) throws ParseException, RacingEventNotFoundException;
    /**
     * Creates a new Player.
     * @param created rThe information of the created Player.
     * @return  The created Player.
     */
    public TrackInEvent create(Integer racingEventId,TrackInEvent created) throws ParseException, RacingEventNotFoundException;

    /**
     * Deletes a Player.
     * @param trackInEventId The id of the deleted track.
     * @return The deleted Player.
     * @throws TrackInEventNotFoundException  if no Player is found with the given id.
     */
    public TrackInEvent delete(Integer trackInEventId) throws TrackInEventNotFoundException;

    /**
     * Finds all Players.
     * @return  A list of Players.
     */
    public List<TrackInEvent> findAll();

    /**
     * Finds Player by id.
     * @param id    The id of the wanted Player.
     * @return  The found Player. If no Player is found, this method returns null.
     */
    public TrackInEvent findById(Integer id);

    /**
     * Updates the information of a Player.
     * @param updated   The information of the updated Player.
     * @return  The updated Player.
     * @throws TrackInEventNotFoundException  if no Player is found with given id.
     */
    public TrackInEvent update(TrackInEvent updated) throws TrackInEventNotFoundException;


    public TrackInEvent updateOrdering(TrackInEvent updated) throws TrackInEventNotFoundException;

    public TrackInEvent updateVehicles(TrackInEvent updated) throws TrackInEventNotFoundException;


    public TrackInEvent updateTrack(TrackInEvent trackInEvent) throws TrackInEventNotFoundException;

    public TrackInEvent delete(Integer racingEventId, Integer trackInEvent_id);
}
