package pl.anubis.spring.data.repository.secureuser;

import pl.anubis.spring.model.Role;

import java.util.List;

/**
 * Created by Anubis on 27.05.14.
 */
public interface RoleService
{
    public Role create(final String role);
    /**
     * Creates a new User.
     * @param created   The information of the created User.
     * @return  The created User.
     */
    public Role create(Role created);

    /**
     * Deletes a User.
     * @param userId  The id of the deleted user.
     * @return  The deleted user.
     * @throws UserNotFoundException  if no user is found with the given id.
     */
    public Role delete(Integer userId) throws RoleNotFoundException;

    /**
     * Finds all Users.
     * @return  A list of Users.
     */
    public List<Role> findAll();

    /**
     * Finds User by id.
     * @param id    The id of the wanted User.
     * @return  The found User. If no User is found, this method returns null.
     */
    public Role findById(Integer id);
    /**
     * Updates the information of a User.
     * @param updated   The information of the updated User.
     * @return  The updated User.
     * @throws UserNotFoundException  if no User is found with given id.
     */
    public Role update(Role updated) throws RoleNotFoundException;

}
