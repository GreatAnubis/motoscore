package pl.anubis.spring.data.repository.racingevent;

/**
 * Created by Anubis on 02.05.14.
 */
public class RacingEventNotFoundException extends Exception
{
    public RacingEventNotFoundException()
    {
        super();
    }
    public RacingEventNotFoundException(final String reason)
    {
        super(reason);
    }
}
