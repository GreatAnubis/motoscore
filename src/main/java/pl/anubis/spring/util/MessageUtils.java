package pl.anubis.spring.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;

/**
 * Created by Anubis on 2014-07-12.
 */
@Service
public class MessageUtils
{
    @Autowired
    private MessageSource messageSource;

    public String getText(final String code, final String ... params)
    {
        return messageSource.getMessage(code, params, Locale.getDefault());
    }


}
