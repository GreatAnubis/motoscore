/**
 * Copyright (c) 2012 Cognitran Limited. All Rights Reserved.
 */
package pl.anubis.spring.util;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.QName;
import org.dom4j.XPath;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import pl.anubis.spring.model.Player;
import pl.anubis.spring.model.RacingEvent;
import pl.anubis.spring.model.Track;
import pl.anubis.spring.model.TrackInEvent;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

/**
 * Processes classes on the class-path to generate a persistence XML descriptor.
 */
public class EntityScanner
{
    /**
     * OSH CRM Persistence unit name.
     */
    public static final String MOTO_DB_PU = "motodbpu";

    /**
     * The logger.
     */
    // private static final Logger LOGGER = Logger.getLogger(EntityScanner.class);

    /**
     * The persistence namespace.
     */
    private static final String PERSISTENCE_NAMESPACE = "http://java.sun.com/xml/ns/persistence";

    /**
     * Main method.
     * 
     * @param args
     *            the arguments.
     * @throws Exception
     *             on failure.
     */
    public static void main(final String[] args) throws Exception
    {
        try
        {
            if (args.length != 2)
            {
                throw new IllegalArgumentException("Invalid number of arguments.");
            }

            final File source = new File(args[0]);
            System.out.println("Source: " + source.getPath());

            final File destination = new File(args[1]);
            System.out.println("Destination: " + destination.getPath());
            System.out.println("Source: " + source.getPath());
            System.out.println("Destination: " + destination.getPath());
            System.out.println("Persistence Unit: " + MOTO_DB_PU);
            final Document document = new SAXReader().read(source);

            addClassesToDocument(document, MOTO_DB_PU);

            writePersistenceXML(destination, document);
            /*
             * System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
             * Ejb3Configuration cfg = new Ejb3Configuration().configure(MOTO_DB_PU, new Properties());
             * EntityManagerFactory pers = Persistence.createEntityManagerFactory(MOTO_DB_PU);
             * //Configuration hbmcfg = cfg.getHibernateConfiguration();
             * SchemaExport schemaExport = new SchemaExport(hbmcfg);
             * schemaExport.setOutputFile(args[0] + ".sql");
             * System.out.println("Output: " + args[0] + ".sql");
             * schemaExport.setFormat(true);
             * schemaExport.execute(true, true, false, true);
             */
        }
        catch (final Throwable t)
        {
            logError(t);
            throw new RuntimeException(t);
        }
    }

    /**
     * Add classes to document.
     * 
     * @param document
     *            the document
     * @param persistenceUnit
     *            the persistence unit
     */
    @SuppressWarnings("unchecked")
    private static void addClassesToDocument(final Document document, final String persistenceUnit)
    {
        final Collection<Class<?>> entities = scanEntities(persistenceUnit);
        System.out.println("Processing " + entities.size() + " entities.");
        System.out.println("Processing " + entities.size() + " entities.");
        final String path = "//ns:persistence/ns:persistence-unit[@name='" + persistenceUnit + "']/ns:properties";
        final XPath xpath = DocumentHelper.createXPath(path);
        xpath.setNamespaceURIs(Collections.singletonMap("ns", PERSISTENCE_NAMESPACE));

        final Element propertiesElem = (Element) xpath.selectNodes(document).get(0);
        final Element persistenceUnitElem = propertiesElem.getParent();
        final int indexOfPropertiesElement = persistenceUnitElem.indexOf(propertiesElem);

        for (final Class<?> entity : entities)
        {
            final Element classElem = DocumentHelper.createElement(new QName("class", new Namespace("", PERSISTENCE_NAMESPACE)));
            classElem.addText(entity.getName());
            persistenceUnitElem.content().add(indexOfPropertiesElement, classElem);
        }
    }

    /**
     * Scan entities.
     * 
     * @param persistenceUnit
     *            the persistence unit
     * @return the collection
     */
    private static Collection<Class<?>> scanEntities(final String persistenceUnit)
    {
        final Collection<Class<?>> entities = new TreeSet<Class<?>>(new ClassNameComparator());

        // OSH CRM entities
        // entities.add(DatabaseCheck.class);
        // entities.add(HighestKey.class);
        entities.add(Player.class);
        entities.add(RacingEvent.class);
        entities.add(Track.class);
        entities.add(TrackInEvent.class);
        // entities.add(RacingEvent.class);

        return entities;
    }

    /**
     * Write persistence xml.
     * 
     * @param destination
     *            the destination
     * @param document
     *            the document
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private static void writePersistenceXML(final File destination, final Document document) throws IOException
    {
        XMLWriter writer = null;

        try
        {
            if (!destination.getParentFile().exists() && !destination.getParentFile().mkdirs())
            {
                throw new IOException("Failed to create destination directory " + destination.getParentFile().getAbsolutePath());
            }

            writer = new XMLWriter(Files.newWriter(destination, Charsets.UTF_8), OutputFormat.createPrettyPrint());
            writer.write(document);
        }
        finally
        {
            if (writer != null)
            {
                writer.flush();
                writer.close();
            }
        }
    }

    /**
     * Log error.
     * 
     * @param throwable
     *            the throwable
     */
    private static void logError(final Throwable throwable)
    {
        if (throwable instanceof IllegalArgumentException)
        {
            System.out.println(throwable.getMessage());
        }
        else
        {
            throwable.printStackTrace();
        }

        System.out.println("***** Usage: source_directory persistence_file *****");
    }

    /**
     * Compares classes by name.
     */
    private static class ClassNameComparator implements Comparator<Class<?>>, Serializable
    {
        /**
         * Constructor.
         */
        public ClassNameComparator()
        {
            super();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int compare(final Class<?> o1, final Class<?> o2)
        {
            return o2.getName().compareTo(o1.getName());
        }

        /**
         * The serialVersionUID.
         */
        private static final long serialVersionUID = 6159481772630922737L;
    }
}
