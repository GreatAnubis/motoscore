package pl.anubis.spring.mvc.container;

import pl.anubis.spring.mvc.container.json.VehicleInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class VehicleInfoForRacingEvent 
{
	private int racingEventId;
	private Map<Integer, Set<VehicleInfo>> vehicles = new HashMap<Integer, Set<VehicleInfo>>();
	public int getRacingEventId() {
		return racingEventId;
	}
	public void setRacingEventId(int racingEventId) {
		this.racingEventId = racingEventId;
	}
	public Map<Integer, Set<VehicleInfo>> getVehicles() {
		return vehicles;
	}
	

}
