package pl.anubis.spring.mvc.container;

import pl.anubis.spring.model.TrackInEvent;

/**
 * Created by Anubis on 12.06.14.
 */
public class VehicleAssigningDTO
{
    public TrackInEvent getTrackInEvent()
    {
        return trackInEvent;
    }

    public void setTrackInEvent(TrackInEvent trackInEvent)
    {
        this.trackInEvent = trackInEvent;
    }
    private TrackInEvent trackInEvent;
}
