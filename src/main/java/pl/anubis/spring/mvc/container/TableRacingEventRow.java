package pl.anubis.spring.mvc.container;

import pl.anubis.spring.model.Player;
import pl.anubis.spring.mvc.container.json.PlayerData;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anubis on 02.06.14.
 */
public class TableRacingEventRow implements Serializable
{
    private int playerId;
    private int totalScore;

    public int getId()
    {
        return id;
    }

    public void setId(final int id)
    {
        this.id = id;
    }

    private int id;

    public TableRacingEventRow()
    {

    }

    public TableRacingEventRow(final PlayerData player, final int totalScore)
    {
        this.totalScore = totalScore;
        this.playerId =player.getId();
    }

    private Map<Integer, Integer> playerScores = new HashMap<Integer, Integer>();
    

    public int getTotalScore()
    {
        return totalScore;
    }

    public void setTotalScore(int totalScore)
    {
        this.totalScore = totalScore;
    }

    public int getPlayerId()
    {
        return playerId;
    }

    public void setPlayerId(PlayerData player)
    {
        this.playerId = player.getId();
    }


    public void setPlayerId(int playerId)
    {
        this.playerId = playerId;
    }

    public void setPlayer(Player player)
    {
        this.playerId = player.getId();
    }

    public Map<Integer, Integer> getPlayerScores()
    {
        return playerScores;
    }
}
