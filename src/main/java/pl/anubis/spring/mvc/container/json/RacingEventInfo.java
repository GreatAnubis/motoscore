package pl.anubis.spring.mvc.container.json;

import pl.anubis.spring.mvc.container.TableRacingEventRow;

import java.util.*;

/**
 * Created by Anubis on 2014-07-14.
 */
public class RacingEventInfo
{
    public String getTimeStamp()
    {
        return timeStamp;
    }

    public void setTimeStamp(final String timeStamp)
    {
        this.timeStamp = timeStamp;
    }

    public String getMovieLink()
    {
        return movieLink;
    }

    public void setMovieLink(final String movieLink)
    {
        this.movieLink = movieLink;
    }

    public Map<Integer, TrackInfo> getTracks()
    {
        return tracks;
    }

    public void setTracks(final Map<Integer, TrackInfo> tracks)
    {
        this.tracks = tracks;
    }

    public void addTrack(final TrackInfo track)
    {
        if(!this.tracks.containsKey(track.getId()))
        {
            this.tracks.put(track.getId(), track);
        }
    }


    public Map<Integer,VehicleInfo> getVehicles()
    {
        return vehicles;
    }


    public void addVehicle(final VehicleInfo vehicle)
    {
        if(!this.vehicles.containsKey(vehicle.getId()))
        {
            vehiclesUsed++;
            this.vehicles.put(vehicle.getId(), vehicle);
        }
    }

    public void setVehicles(final Map<Integer,VehicleInfo> vehicles)
    {
        vehiclesUsed = vehicles.size();
        this.vehicles = vehicles;
    }

    public Map<Integer, Set<Integer>> getVehiclePresenceInRun()
    {
        return vehiclesPresenceInRun;
    }

    public void setVehiclesPresenceInRun(final Map<Integer, Set<Integer>> vehiclesPresenceInRun)
    {
        this.vehiclesPresenceInRun = vehiclesPresenceInRun;
    }

    public List<TableRacingEventRow> getTableRacingEventRows()
    {
        return tableRacingEventRows;
    }

    public void setTableRacingEventRows(final List<TableRacingEventRow> tableRacingEventRows)
    {
        this.tableRacingEventRows = tableRacingEventRows;
    }

    public Integer getYear()
    {
        return year;
    }

    public void setYear(final Integer year)
    {
        this.year = year;
    }

    public Integer getMonth()
    {
        return month;
    }

    public void setMonth(final Integer month)
    {
        this.month = month;
    }


    public Integer getId()
    {
        return id;
    }

    public void setId(final Integer id)
    {
        this.id = id;
    }

    public  Map<Integer, PlayerData> getPlayers()
    {
        return players;
    }

    public void addPlayer(final PlayerData player)
    {
        if(!this.players.containsKey(player.getId()))
        {
            this.players.put(player.getId(), player);
        }
    }

    public void setPlayers(final Map<Integer, PlayerData> players)
    {
        this.players = players;
    }


    public int getVehiclesUsed()
    {
        return vehiclesUsed;
    }


    private Integer id;
    private String timeStamp;
    private String movieLink;


    int vehiclesUsed = 0;
    private Integer year;
    private Integer month;

    private Map<Integer, PlayerData> players = new HashMap<Integer,PlayerData>();
    private Map<Integer, TrackInfo> tracks = new HashMap<Integer,TrackInfo>();
    private Map<Integer, VehicleInfo> vehicles = new HashMap<Integer,VehicleInfo>();
    private Map<Integer, Set<Integer>> vehiclesPresenceInRun = new HashMap<Integer, Set<Integer>>();
    private List<TableRacingEventRow> tableRacingEventRows = new ArrayList<TableRacingEventRow>();
}
