package pl.anubis.spring.mvc.container.json;

import pl.anubis.spring.model.Vehicle;

import java.io.Serializable;

public class VehicleInfo implements Comparable<VehicleInfo>, Serializable
{
    private static final long serialVersionUID = 8874600610040305746L;
    private String vehicle_name;
    private String vehicle_code;
    private int id;

    public int getId()
    {
        return id;
    }

    public void setId(final int id)
    {
        this.id = id;
    }


    public VehicleInfo(final Vehicle veh)
    {
        this.vehicle_name = veh.getVehicle_name();
        this.vehicle_code = veh.getVehicle_code();
        this.id = veh.getId();
    }

    public String getVehicle_name()
    {
        return vehicle_name;
    }

    public void setVehicle_name(final String vehicle_name)
    {
        this.vehicle_name = vehicle_name;
    }

    public String getVehicle_code()
    {
        return vehicle_code;
    }

    public void setVehicle_code(final String vehicle_code)
    {
        this.vehicle_code = vehicle_code;
    }

    public boolean equals(Object obj)
    {
        if(obj == null)
        {
            return  false;
        }
        if(obj instanceof  VehicleInfo)
        {
            if(this.getVehicle_name().equals(((VehicleInfo) obj).getVehicle_name()))
            {
                return  true;
            }
        }
        return false;
    }

    @Override
    public int compareTo(final VehicleInfo o)
    {
        return this.getId() - o.getId();
    }
}
