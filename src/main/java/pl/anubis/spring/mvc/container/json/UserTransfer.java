package pl.anubis.spring.mvc.container.json;

import java.util.Map;

/**
 * Created by Anubis on 2014-09-17.
 */
public class UserTransfer
{
    private final String name;

    private final Map<String, Boolean> roles;


    public UserTransfer(String userName, Map<String, Boolean> roles)
    {
        this.name = userName;
        this.roles = roles;
    }


    public String getName()
    {
        return this.name;
    }


    public Map<String, Boolean> getRoles()
    {
        return this.roles;
    }

}
