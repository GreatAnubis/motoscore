package pl.anubis.spring.mvc.container.json;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import pl.anubis.spring.model.Track;
import pl.anubis.spring.model.TrackInEvent;

import java.io.Serializable;

/**
 * Created by Anubis on 2014-07-14.
 */
public class TrackInfo implements Serializable
{
    private static final long serialVersionUID = -5216914287985263243L;
    @NotEmpty
    @Length(min = 3, max = 128)
    private String track_name;
    private String track_code;
    private String track_variant;
    private int id;

    public TrackInfo()
    {


    }
    public TrackInfo(final TrackInEvent track)
    {
        track_name = track.getTrack().getTrack_name();
        track_code = track.getTrack().getTrack_code();
        track_variant = track.getTrack().getTrack_variant();
        this.id = track.getId();
    }

    public TrackInfo(final Track track)
    {
        track_name = track.getTrack_name();
        track_code = track.getTrack_code();
        track_variant = track.getTrack_variant();
        this.id = track.getId();
    }

    public int getId()
    {
        return id;
    }

    public void setId(final int id)
    {
        this.id = id;
    }

    public String getTrack_name()
    {
        return track_name;
    }

    public void setTrack_name(final String track_name)
    {
        this.track_name = track_name;
    }

    public String getTrack_code()
    {
        return track_code;
    }

    public void setTrack_code(final String track_code)
    {
        this.track_code = track_code;
    }

    public String getTrack_variant()
    {
        return track_variant;
    }

    public void setTrack_variant(final String track_variant)
    {
        this.track_variant = track_variant;
    }

    public boolean equals(Object other)
    {
        if (other == this) return true;
        if (other == null) return false;
        if (getClass() != other.getClass()) return false;
        if(this.getTrack_name().compareToIgnoreCase(((TrackInfo) other).getTrack_name()) == 0)
        {
            return true;
        }
        return  false;
    }

}
