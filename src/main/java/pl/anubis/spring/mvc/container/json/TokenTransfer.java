package pl.anubis.spring.mvc.container.json;

/**
 * Created by Anubis on 2014-09-17.
 */
public class TokenTransfer
{
    private final String token;


    public TokenTransfer(String token)
    {
        this.token = token;
    }


    public String getToken()
    {
        return this.token;
    }
}
