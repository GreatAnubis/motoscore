package pl.anubis.spring.mvc.container.json;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import pl.anubis.spring.model.Player;

import java.io.Serializable;

/**
 * Created by Anubis on 08.06.14.
 */
public class PlayerData implements Serializable
{
    private static final long serialVersionUID = -1264447338052958012L;

    @NotEmpty
    @Length(min = 3, max = 32)
    private String player_nick;
    private int id;


    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }



    public PlayerData()
    {

    }

    public PlayerData(Player player)
    {
        this.id = player.getId();
        this.player_nick = player.getPlayer_nick();
    }

    public PlayerData(final String nick)
    {
        this.player_nick = nick;
    }

    public String getPlayer_nick()
    {
        return player_nick;
    }

    public void setPlayer_nick(String player_nick)
    {
        this.player_nick = player_nick;
    }

    public boolean equals(Object other)
    {
        if (other == this) return true;
        if (other == null) return false;
        if (getClass() != other.getClass()) return false;
        if(this.getPlayer_nick().compareToIgnoreCase(((PlayerData) other).getPlayer_nick()) == 0)
        {
            return true;
        }
        return  false;
    }
}
