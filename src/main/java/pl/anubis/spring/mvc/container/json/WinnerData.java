package pl.anubis.spring.mvc.container.json;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Anubis on 08.06.14.
 */
public class WinnerData implements Serializable
{
    private static final long serialVersionUID = 3950733898366053554L;

    private String scoresTitle;
    private String range;

    public String getRange()
    {
        return range;
    }

    public void setRange(String range)
    {
        this.range = range;
    }

    public WinnerData(String range)
    {
        this.range = range;
    }

    public List<PlayerScoreDataRow> getScores()
    {
        return scores;
    }

    public void setScores(List<PlayerScoreDataRow> scores)
    {
        this.scores = scores;
    }

    List<PlayerScoreDataRow> scores;


    public void setScoresTitle(String scoresTitle)
    {
        this.scoresTitle = scoresTitle;
    }

    public String getScoresTitle()
    {
        return scoresTitle;
    }
}
