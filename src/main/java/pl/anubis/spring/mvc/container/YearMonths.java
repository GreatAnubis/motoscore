package pl.anubis.spring.mvc.container;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anubis on 25.06.14.
 */
public class YearMonths implements Serializable
{
    public YearMonths(Integer year)
    {
        this.year = year;
    }

    public YearMonths(Integer year, Integer month)
    {
        this.year = year;
        this.months.add(month);
    }

    public void addMonth(Integer month)
    {
        months.add(month);
    }

    public Integer getYear()
    {
        return year;
    }

    public List<Integer> getMonths()
    {
        return months;
    }

    private Integer year;
    private List<Integer> months = new ArrayList<Integer>();

}
