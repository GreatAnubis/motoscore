package pl.anubis.spring.mvc.container.json;

import java.io.Serializable;

/**
 * Created by Anubis on 06.06.14.
 */
public class PlayerScoreDataRow implements Serializable
{
    private PlayerData player;
    private int place;
    private int score;

    public PlayerData getPlayer()
    {
        return player;
    }

    public void setPlayer(PlayerData player)
    {
        this.player = player;
        this.place = 0;
        this.score = 0;
    }

    public PlayerScoreDataRow()
    {
        this.place = 0;
        this.score = 0;
    }

    public PlayerScoreDataRow(PlayerData player,int score)
    {
        this.player = player;
        this.place = 0;
        this.score = score;
    }

    public PlayerScoreDataRow(int score)
    {
        this.score = score;
        this.place = 0;
    }

    public PlayerScoreDataRow(int score, int place)
    {
        this.place = place;
        this.score = score;
    }


    public int getPlace()
    {
        return place;
    }

    public void setPlace(int place)
    {
        this.place = place;
    }

    public int getScore()
    {
        return score;
    }

    public void setScore(int score)
    {
        this.score = score;
    }




}
