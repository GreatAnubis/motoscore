package pl.anubis.spring.security.filter;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.GenericFilterBean;
import pl.anubis.spring.model.SecuredUser;
import pl.anubis.spring.security.TokenUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Anubis on 2014-09-16.
 */
public class AuthenticationTokenProcessingFilter extends GenericFilterBean
{
    private UserDetailsService userService;


    public AuthenticationTokenProcessingFilter(UserDetailsService userService)
    {
        this.userService = userService;
    }


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException
    {
        try
        {
            HttpServletRequest httpRequest = this.getAsHttpRequest(request);

            String authToken = this.extractAuthTokenFromRequest(httpRequest);
            String userName = TokenUtils.getUserNameFromToken(authToken);

            if (userName != null)
            {
                boolean needsToBeAuthenticated = true;
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (authentication != null)
                {
                    Object principal = authentication.getPrincipal();
                    if (!(principal instanceof String) && (principal instanceof SecuredUser))
                    {
                        ((HttpServletResponse) response).addHeader("Authenticated", "true");
                    }
                    else
                    {
                        needsToBeAuthenticated = false;
                    }
                }
                if(needsToBeAuthenticated)
                {
                    UserDetails userDetails = this.userService.loadUserByUsername(userName);

                    if (TokenUtils.validateToken(authToken, userDetails))
                    {

                        UsernamePasswordAuthenticationToken usauthentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                        usauthentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
                        SecurityContextHolder.getContext().setAuthentication(usauthentication);
                    }
                }
            }
            else
            {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (authentication != null)
                {
                    Object principal = authentication.getPrincipal();
                    if (!(principal instanceof String) && (principal instanceof SecuredUser))
                    {
                        ((HttpServletResponse) response).addHeader("Authenticated", "true");
                    }
                }
            }
        }
        finally
        {
            chain.doFilter(request, response);
        }
    }


    private HttpServletRequest getAsHttpRequest(ServletRequest request)
    {
        if (!(request instanceof HttpServletRequest)) {
            throw new RuntimeException("Expecting an HTTP request");
        }

        return (HttpServletRequest) request;
    }


    private String extractAuthTokenFromRequest(HttpServletRequest httpRequest)
    {
		/* Get token from header */
        String authToken = httpRequest.getHeader("X-Auth-Token");

		/* If token not found get it from request parameter */
        if (authToken == null) {
            authToken = httpRequest.getParameter("token");
        }

        return authToken;
    }
}