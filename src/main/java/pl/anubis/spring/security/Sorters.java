package pl.anubis.spring.security;

import pl.anubis.spring.mvc.container.json.PlayerScoreDataRow;
import pl.anubis.spring.mvc.container.TableRacingEventRow;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Anubis on 10.06.14.
 */
public class Sorters
{

    public static Sorters getSorters()
    {
        return new Sorters();
    }

    public List<PlayerScoreDataRow> sortByScore(List<PlayerScoreDataRow> playersRow)
    {
        if(playersRow!=null)
        {
            Collections.sort(playersRow, new Comparator<PlayerScoreDataRow>()
            {
                public int compare(PlayerScoreDataRow o1, PlayerScoreDataRow o2)
                {
                    Integer i1 = o1.getScore();
                    Integer i2 = o2.getScore();
                    int result = (i1 > i2 ? -1 : (i1 == i2 ? 0 : 1));
                    return result;
                }
            });
        }
        return playersRow;
    }

    public List<TableRacingEventRow> sortByTotalScore(List<TableRacingEventRow> playersRow)
    {
        if(playersRow!=null)
        {
            Collections.sort(playersRow, new Comparator<TableRacingEventRow>()
            {
                public int compare(TableRacingEventRow o1, TableRacingEventRow o2)
                {
                    Integer i1 = o1.getTotalScore();
                    Integer i2 = o2.getTotalScore();
                    return (i1 > i2 ? -1 : (i1 == i2 ? 0 : 1));
                }
            });
        }
        return playersRow;
    }
}
