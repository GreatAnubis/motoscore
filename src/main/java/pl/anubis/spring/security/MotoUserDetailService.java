package pl.anubis.spring.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.anubis.spring.data.repository.secureuser.UserService;
import pl.anubis.spring.model.SecuredUser;
import pl.anubis.spring.model.User;

/**
 * Created by Anubis on 27.05.14.
 */
@Service
@Transactional(readOnly=true)
public class MotoUserDetailService implements UserDetailsService
{
    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        User user = userService.findByUserName(username);
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        return new SecuredUser(user);
    }



//    public Collection<? extends GrantedAuthority> getAuthorities(Integer role)
//    {
//        List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role));
//        return authList;
//    }
//    public List<String> getRoles(Integer role)
//    {
//
//        List<String> roles = new ArrayList<String>();
//
//        if (role.intValue() == 1)
//        {
//            roles.add("ROLE_MODERATOR");
//            roles.add("ROLE_ADMIN");
//        }
//        else if (role.intValue() == 2)
//        {
//            roles.add("ROLE_MODERATOR");
//        }
//        return roles;
//    }
//
//
//    public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
//        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//
//        for (String role : roles) {
//            authorities.add(new SimpleGrantedAuthority(role));
//        }
//        return authorities;
//    }
}