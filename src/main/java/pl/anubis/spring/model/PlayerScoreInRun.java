/**
 * 
 */
package pl.anubis.spring.model;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;

/**
 * @author Anubis
 */
@Entity
@Table(name = "player_score_in_run")
public class PlayerScoreInRun extends AbstractPersistable<Integer>
{
    /*
     * @Id
     * @GeneratedValue(strategy = GenerationType.AUTO)
     * private Long id;
     * public Long getId() {
     * return id;
     * }
     */

    public PlayerScoreInRun()
    {

    }

    public PlayerScoreInRun(TrackInEvent trackInEvent,Player player, Integer score )
    {
        this.trackInEvent = trackInEvent;
        this.player = player;
        this.score = score;
    }

    public TrackInEvent getTrackInEvent()
    {
        return trackInEvent;
    }

    public void setTrackInEvent(TrackInEvent trackInEvent)
    {
        this.trackInEvent = trackInEvent;
    }

    public Player getPlayer()
    {
        return player;
    }

    public void setPlayer(Player player)
    {
        this.player = player;
    }

    public Integer getScore()
    {
        return score;
    }

    public void setScore(Integer score)
    {
        this.score = score;
    }

    public int hashCode()
    {
        return getPlayer().getId();
    }
    @Override
    public void setId(final Integer id)
    {
        super.setId(id);
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "track_in_event_id")
    private TrackInEvent trackInEvent;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player_id")
    private Player player;

    @Column(name = "score")
    private Integer score;
    /**
     * 
     */
    private static final long serialVersionUID = -2973579882867107633L;

}
