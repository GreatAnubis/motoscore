package pl.anubis.spring.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "player")
public class Player extends AbstractPersistable<Integer>
{
    @NotEmpty
    @Length(min = 3, max = 32)
    @Column(name = "player_nick",nullable = false, length = 128, unique = true)
    private String player_nick;

    public Date getModificationTime()
    {
        return modificationTime;
    }

    @Column(name = "modification_time", nullable = false)
    private Date modificationTime;

    // @Column(nullable = true, length = 128)
    // private String trophies_link;

    // @Column(nullable = true, length = 255)
    // private String avatar_link;

    public Player()
    {

    }

    public Player(final String nick)
    {
        this.player_nick = nick;
    }

    public String getPlayer_nick()
    {
        return player_nick;
    }

    public void setPlayer_nick(String player_nick)
    {
        this.player_nick = player_nick;
    }

    public void update(final String player_nick)
    {
        this.player_nick = player_nick;
    }

    @PreUpdate
    public void preUpdate()
    {
        this.modificationTime = new Date();
    }

    @PrePersist
    public void prePersist()
    {
        Date now = new Date();
        this.modificationTime = now;
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode()
    {
        int result;
        result = player_nick.hashCode();
        int id = getId() == null ? 0 : getId();
        result = 29 * result + id;
        return result;
    }

    @Override
    public void setId(final Integer id)
    {
        super.setId(id);
    }
    /**
	 * 
	 */
    private static final long serialVersionUID = 2559089181208344088L;

}
