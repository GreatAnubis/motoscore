package pl.anubis.spring.model;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;

import java.util.Set;

@Entity
@Table(name = "track_in_event")
public class TrackInEvent extends AbstractPersistable<Integer>
{
    /*
     * @Id
     * @GeneratedValue(strategy = GenerationType.AUTO)
     * private Long id;
     * public Long getId() {
     * return id;
     * }
     */

    public TrackInEvent()
    {
        this.setId(0);
        this.ordering = 0;
        this.track = null;
        this.image = null;
        this.contentType = null;
        this.playerScoresInRun = null;
        this.vehiclesInRun = null;
    }

    public TrackInEvent(Track track, Integer ordering)
    {
        this.setId(0);
        this.contentType = null;
        this.playerScoresInRun = null;
        this.vehiclesInRun = null;
        this.image = null;
        this.setTrack(track);
        if(ordering==null)
        {
            this.setOrdering(0);
        }
        else
        {
            this.setOrdering(ordering);
        }
    }


    public Track getTrack()
    {
        return track;
    }

    public void setTrack(Track track)
    {
        this.track = track;
    }

    public byte[] getImage()
    {
        return image;
    }

    public void setImage(byte[] image)
    {
        this.image = image;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    @ManyToOne
    @PrimaryKeyJoinColumn
    private Track track;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @OrderBy("score DESC")
    @JoinTable(name = "player_scores_in_run", joinColumns = { @JoinColumn(name = "track_in_event_id", referencedColumnName = "id", nullable = false, table = "track_in_event")

    }, inverseJoinColumns = @JoinColumn(name = "player_score_in_run_id", referencedColumnName = "id", nullable = false, table = "player_score_in_run"))
    private Set<PlayerScoreInRun> playerScoresInRun;

    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @OrderBy("vehicle_name DESC")
    @JoinTable(name = "vehicles_in_run", joinColumns = { @JoinColumn(name = "track_in_event_id", referencedColumnName = "id", nullable = false, table = "track_in_event")

    }, inverseJoinColumns = @JoinColumn(name = "vehicle_in_run_id", referencedColumnName = "id", nullable = false, table = "vehicle"))
    private Set<Vehicle> vehiclesInRun;

    @Column(name = "ordering")
    private Integer ordering;


    @Lob
    @Column(name="image",nullable = true)
    private byte[] image;

    @Column(name="image_type",nullable = true, length = 255)
    private String contentType;


    public Integer getOrdering()
    {
        return this.ordering;
    }

    public void setOrdering(final Integer ordering)
    {
        this.ordering = ordering;
    }

    public Set<PlayerScoreInRun> getPlayerScoresInRun()
    {
        return this.playerScoresInRun;
    }

    public void setPlayerScoresInRun(final Set<PlayerScoreInRun> playerScoresInRun)
    {
        this.playerScoresInRun = playerScoresInRun;
    }


    public Set<Vehicle> getVehiclesInRun()
    {
        return this.vehiclesInRun;
    }

    public void setVehiclesInRun(Set<Vehicle> vehiclesInRun)
    {
        this.vehiclesInRun = vehiclesInRun;
    }

    @Override
    public void setId(final Integer id)
    {
        super.setId(id);
    }

    public boolean containsVehicleInSet(final Vehicle vehicle)
    {
        try
        {
            if(vehicle!=null)
            {
                if(this.vehiclesInRun!=null && this.vehiclesInRun.size() > 0)
                {
                    return  this.vehiclesInRun.contains(vehicle);
                }
            }
        }
        catch (Exception ex)
        {
            return false;
        }
        return false;
    }

//    @Override
//    public String toString()
//    {
//        if(getId() != null)
//        {
//            return getId().toString();
//        }
//        return "0";
//    }

    /**
	 * 
	 */
    private static final long serialVersionUID = 9064050450258990523L;

}
