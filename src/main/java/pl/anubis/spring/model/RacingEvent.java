package pl.anubis.spring.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.format.annotation.DateTimeFormat;
import pl.anubis.spring.config.CustomStrings;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Entity
@Table(name = "racing_event")
public class RacingEvent extends AbstractPersistable<Integer>
{
    /*
     * @Id
     * @GeneratedValue(strategy = GenerationType.AUTO)
     * private Long id;
     * public Long getId() {
     * return id;
     * }
     */

    public void setId(final Integer id)
    {
        super.setId(id);
    }

    /**
     * The timestamp.
     */
    @NotNull
    @DateTimeFormat(pattern = CustomStrings.DEFAULT_DATE_PATTERN)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "event_time", updatable = true, nullable = false)
    private Date timestamp;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @OrderBy("ordering")
    @Fetch(FetchMode.SUBSELECT)
    @JoinTable(name = "tracks_in_event", joinColumns = { @JoinColumn(name = "racing_event_id", referencedColumnName = "id", nullable = false, table = "racing_event")

    }, inverseJoinColumns = @JoinColumn(name = "track_in_event_id", referencedColumnName = "id", nullable = false, table = "track_in_event"))
    private List<TrackInEvent> tracks;

    @Column(name="movie_link", nullable = true, length = 255)
    private String movieLink;

    @Column(name = "visible", nullable =  false)
    boolean visible;

    public Date getTimestamp()
    {
        return timestamp;
    }


    public void setTimestamp(String timestamp) throws ParseException
    {
        this.timestamp = new SimpleDateFormat(CustomStrings.DEFAULT_DATE_PATTERN, Locale.ENGLISH).parse(timestamp);
    }

    public void setTimestamp(Date timestamp)
    {
        this.timestamp = timestamp;
    }

    public List<TrackInEvent> getTracks()
    {
        return tracks;
    }

    public void setTracks(List<TrackInEvent> tracks)
    {
        this.tracks = tracks;
    }

    public String getMovieLink()
    {
        return movieLink;
    }

    public void setMovieLink(String movieLink)
    {
        this.movieLink = movieLink;
    }

    public boolean isVisible()
    {
        return visible;
    }

    public void setVisible(boolean visible)
    {
        this.visible = visible;
    }

    public static Builder getBuilder(final String timestamp,final  String movieLink) throws ParseException
    {
        return new Builder(timestamp, movieLink);
    }

    public static Builder getBuilder(final Date timestamp,final  String movieLink) throws ParseException
    {
        return new Builder(timestamp, movieLink);
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }

    public void update(Date timestamp, String movieLink) throws ParseException
    {
        this.setTimestamp(timestamp);
        this.setMovieLink(movieLink);
    }

    public static class Builder
    {
        RacingEvent built;

        Builder(final Date timestamp,final  String movieLink) throws ParseException
        {
            this.built = new RacingEvent();
            this.built.timestamp =  timestamp;
            this.built.movieLink = movieLink;
        }

        Builder(final String timestamp,final  String movieLink) throws ParseException
        {
            this.built = new RacingEvent();
            this.built.timestamp =  new SimpleDateFormat(CustomStrings.DEFAULT_DATE_PATTERN, Locale.ENGLISH).parse(timestamp);
            this.built.movieLink = movieLink;
        }

        /**
         * Builds the new Track object.
         *
         * @return The created Track object.
         */
        public RacingEvent build()
        {
            return this.built;
        }


    }
    /**
	 * 
	 */
    private static final long serialVersionUID = 9041493501396272250L;

}
