package pl.anubis.spring.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;
import pl.anubis.spring.mvc.container.json.TrackInfo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "track")
public class Track extends ImageContent
{

    @NotEmpty
    @Column(name="track_name", nullable = false, length = 128, unique = true)
    private String track_name;

    @Column(name="track_code", nullable = true, length = 32)
    private String track_code;

    @Column(name="track_variant", nullable = true, length = 32)
    private String track_variant;

    public Track() {
        super();
    }


    public String getTrack_variant()
    {
        return track_variant;
    }

    public void setTrack_variant(final String track_variant)
    {
        this.track_variant = track_variant;
    }

    public String getTrack_code()
    {
        return track_code;
    }

    public void setTrack_code(String track_code)
    {
        this.track_code = track_code;
    }

    /**
     * @return the track_name
     */
    public String getTrack_name()
    {
        return this.track_name;
    }

    /**
     * @param track_name
     *            the track_name to set
     */
    public void setTrack_name(final String track_name)
    {
        this.track_name = track_name;
    }


    public static Builder getBuilder(final String track_name)
    {
        return new Builder(track_name);
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public void setId(final Integer id)
    {
        super.setId(id);
    }

    public void update(final TrackInfo updated)
    {
        this.track_name = updated.getTrack_name();
        this.track_code = updated.getTrack_code();
        this.track_variant = updated.getTrack_variant();
    }

    public static Builder getBuilder(final TrackInfo trackForm)
    {
        return new Builder(trackForm.getTrack_name(), trackForm.getTrack_code(), trackForm.getTrack_variant());
    }

    /**
     * A Builder class used to create new Track objects.
     */
    public static class Builder
    {
        Track built;

        /**
         * Creates a new Builder instance.
         * @param track_name
         *            The track name of the created Track object.
         */
        Builder(final String track_name)
        {
            this.built = new Track();
            this.built.track_name = track_name;
        }

        Builder(final String track_name, final String track_code, final String track_variant)
        {
            this.built = new Track();
            this.built.track_name = track_name;
            this.built.track_code = track_code;
            this.built.track_variant = track_variant;
        }

        /**
         * Builds the new Track object.
         * 
         * @return The created Track object.
         */
        public Track build()
        {
            return this.built;
        }
    }

    /**
	 * 
	 */
    private static final long serialVersionUID = 8583487732720129455L;
}
