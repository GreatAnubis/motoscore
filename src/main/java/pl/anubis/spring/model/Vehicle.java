package pl.anubis.spring.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Anubis on 06.06.14.
 */
@Entity
@Table(name = "vehicle")
public class Vehicle  extends AbstractPersistable<Integer>
{
    public Vehicle()
    {
        super();
    }

    public String getVehicle_name()
    {
        return vehicle_name;
    }

    public void setVehicle_name(String vehicle_name)
    {
        this.vehicle_name = vehicle_name;
    }

    public String getVehicle_code()
    {
        return vehicle_code;
    }

    public void setVehicle_code(String vehicle_code)
    {
        this.vehicle_code = vehicle_code;
    }

    public int hashCode()
    {
        int result;
        result = vehicle_name.hashCode();
        int id = getId() == null ? 0 : getId();
        result = 19 * result + id;
        return result;
    }
    @Override
    public void setId(final Integer id)
    {
        super.setId(id);
    }

    @NotEmpty
    @Column(name="vehicle_name", nullable = false, length = 32, unique = true)
    private String vehicle_name;

    @NotEmpty
    @Column(name="vehicle_code", nullable = false, length = 16)
    private String vehicle_code;
}
