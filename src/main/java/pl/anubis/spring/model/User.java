package pl.anubis.spring.model;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anubis on 23.05.14.
 */
@Entity
@Table(name="users")
public class User extends AbstractPersistable<Integer>
{
    private boolean enabled;

    public User()
    {

    }

    public User(final String username, final String password)
    {
        this.username = username;
        this.password = password;
    }

    @Length(min = 3, max = 10)
    @Column(name = "username", length = 10)
    private String username;

    @Column(name = "password", length = 128)
    @Length(min = 3, max = 128)
    private String password;


//    @JoinTable(name="user_roles",
//            joinColumns = {@JoinColumn(name="user_id", referencedColumnName="id", table="users")},
//            inverseJoinColumns = {@JoinColumn(name="role_id", referencedColumnName="id", table = "roles")}
//    )
    @ManyToOne(cascade= CascadeType.ALL)
    @JoinColumn(name = "role_id")
    private Role role;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public List<String> getAuthorities()
    {
        //List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role));
        if(getRole() != null)
        {
            return getRoles(getRole().getId());
        }
        else
        {
            return new ArrayList<String>();
        }
    }
    public List<String> getRoles(Integer role)
    {

        List<String> roles = new ArrayList<String>();

        if (role.intValue() == 1)
        {
            roles.add("ROLE_MODERATOR");
            roles.add("ROLE_ADMIN");
        }
        else if (role.intValue() == 2)
        {
            roles.add("ROLE_MODERATOR");
        }
        return roles;
    }


//    public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
//        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//
//        for (String role : roles) {
//            authorities.add(new SimpleGrantedAuthority(role));
//        }
//        return authorities;
//    }
}

