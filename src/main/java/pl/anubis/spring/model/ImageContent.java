package pl.anubis.spring.model;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;

/**
 * Created by Anubis on 01.05.14.
 */
@MappedSuperclass
public abstract class ImageContent extends AbstractPersistable<Integer>
{
    public ImageContent()
    {
        this.image = null;
        this.contentType = null;
    }

    @Lob
    @Column(name="image",nullable = true)
    private byte[] image;

    @Column(name="image_type",nullable = true, length = 255)
    private String contentType;



    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

}
