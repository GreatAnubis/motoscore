package pl.anubis.spring.model;

import com.google.common.base.Optional;

public class Objects
{

    /**
     * Constructor.
     */
    private Objects()
    {
        // Utility class.
    }

    /**
     * Returns {@literal true} if both objects are non-null.
     *
     * @param object1 the first objects.
     * @param object2 the second objects.
     * @return whether both objects are non-null.
     */
    public static boolean notNull(Object object1, Object object2)
    {
        return object1 != null && object2 != null;
    }

    /**
     * Returns {@literal true} if all objects are non-null.
     *
     * @param objects the objects.
     * @return whether all objects are non-null.
     */
    public static boolean notNull(Object... objects)
    {
        for (Object object : objects)
        {
            if (object == null)
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns an option containing {@literal 0}, {@literal -1}, {@literal 1} or an absent optional depending on whether the both
     * arguments are null, the left is null, the right is null or neither are null respectively.
     *
     * @param left the left.
     * @param right the right.
     * @return the comparison.
     */
    public static Optional<Integer> nullCompare(final Object left, final Object right)
    {
        Integer comparison = null;
        if (left == null && right == null)
        {
            comparison = 0;
        }
        else if (left == null)
        {
            comparison = -1;
        }
        else if (right == null)
        {
            comparison = 1;
        }
        return Optional.fromNullable(comparison);
    }

    /**
     * Compares two objects for equality. If both are null, true is returned.
     *
     * @param o1 the first object.
     * @param o2 the second object.
     * @return whether the objects are equal.
     */
    public static boolean equals(final Object o1, final Object o2)
    {
        return o1 == null ? o2 == null : (o2 == null ? false : o1.equals(o2));
    }

    /**
     * Performs some basic checks on the two given objects that can quickly determine whether they are definitely equal or
     * non-equal. This method can be called by {@code equals} methods before doing more in-depth check to determine equality. This
     * method check for nulls, identity matches and class mismatches.
     *
     * @param self the object responsible for the check.
     * @param other the object being checked for equality.
     * @return an optional return value. If the value is present the equality has been determined by this method.
     */
    public static Optional<Boolean> initialEqualityCheck(final Object self, final Object other)
    {
        Optional<Boolean> result = Optional.absent();
        if (self == null || other == null || !self.getClass().isInstance(other))
        {
            result = Optional.of(Boolean.FALSE);
        }
        else if (self == other)
        {
            result = Optional.of(Boolean.TRUE);
        }
        return result;
    }

    /**
     * Returns {@literal "null"} if the object is null or the result of the objects {@code toString()} method otherwise.
     *
     * @param object the object.
     * @return the string representation of the object.
     */
    public static String nullSafeToString(final Object object)
    {
        return object == null ? "null" : object.toString();
    }
}
