package pl.anubis.spring.model;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Anubis on 23.05.14.
 */
@Entity
@Table(name="roles")
public class Role  extends AbstractPersistable<Integer>
{
    @Column(name="role", length = 32, unique = true, nullable = false)
    private String role;

//    @OneToMany(cascade= CascadeType.ALL)
//    @JoinTable(name="user_roles",
//            joinColumns = {@JoinColumn(name="role_id", referencedColumnName="id", table="roles")},
//            inverseJoinColumns = {@JoinColumn(name="user_id", referencedColumnName="id", table="users")}
//    )
//    private Set<User> userRoles;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

//    public Set<User> getUserRoles() {
//        return userRoles;
//    }
//
//    public void setUserRoles(Set<User> userRoles) {
//        this.userRoles = userRoles;
//    }

}
