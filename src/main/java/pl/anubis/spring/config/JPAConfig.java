/**
 * 
 */
package pl.anubis.spring.config;

import java.sql.SQLException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.sql.Driver;

/**
 * @author Anubis
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "pl.anubis.spring.data.repository" })
@PropertySources(value = { @PropertySource("classpath:application.properties") })
public class JPAConfig
{
    private static final Logger LOGGER = LoggerFactory.getLogger(JPAConfig.class);

    @Autowired
    private Environment environment;

    @Bean
    public DataSource dataSource() throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException
    {
        LOGGER.debug("dataSource===============>");
        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        // dataSource.setDriver(new com.mysql.jdbc.Driver());
        // Driver dr = new Driver();
        //final Driver driver = new com.mysql.jdbc.Driver();// this.environment.getProperty(CustomStrings.PROPERTY_NAME_DATABASE_DRIVER, Driver.class);
        final String dbURL = this.environment.getRequiredProperty(CustomStrings.PROPERTY_NAME_DATABASE_URL);
        final String dbUser = this.environment.getRequiredProperty(CustomStrings.PROPERTY_NAME_DATABASE_USERNAME);
        final String dbPass = this.environment.getRequiredProperty(CustomStrings.PROPERTY_NAME_DATABASE_PASSWORD);
        final String dbStrDriver = this.environment.getRequiredProperty(CustomStrings.PROPERTY_NAME_DATABASE_DRIVER);
        LOGGER.debug("Driver: " + dbStrDriver);
        LOGGER.debug("URL: " + dbURL);
        LOGGER.debug("USER: " + dbUser);
        LOGGER.debug("PASS: " + dbPass);

        final Class<?> clazz = Class.forName(dbStrDriver);
        final Object obj = clazz.newInstance();
        final Driver dbDriver = (Driver)obj;

        dataSource.setDriver(dbDriver);
        dataSource.setUrl(dbURL);
        dataSource.setUsername(dbUser);
        dataSource.setPassword(dbPass);
        LOGGER.debug("<===============dataSource");
        return dataSource;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter()
    {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setShowSql(true);
        hibernateJpaVendorAdapter.setGenerateDdl(true);
        hibernateJpaVendorAdapter.setDatabase(Database.MYSQL);

        // hibernateJpaVendorAdapter.setDatabase(Database.H2);
        return hibernateJpaVendorAdapter;
    }

    @Bean
    public EntityManagerFactory entityManagerFactory() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        LOGGER.debug("entityManagerFactory===============>");
        final LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

        entityManagerFactoryBean.setDataSource(this.dataSource());
        entityManagerFactoryBean.setPackagesToScan(this.environment.getRequiredProperty(CustomStrings.PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN));// "pl.anubis.spring"
        // entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistence.class);
        // entityManagerFactoryBean.setPersistenceXmlLocation("classpath*:META-INF/persistence.xml");
        // entityManagerFactoryBean.setPersistenceUnitName("motodbpu");
        entityManagerFactoryBean.setJpaVendorAdapter(this.jpaVendorAdapter());
        entityManagerFactoryBean.setJpaProperties(this.getJpaProperties());
        entityManagerFactoryBean.afterPropertiesSet();

        LOGGER.debug("<===============entityManagerFactory");
        return entityManagerFactoryBean.getObject();
    }

    private Properties getJpaProperties()
    {
        final Properties jpaProterties = new Properties();
        jpaProterties.put(CustomStrings.PROPERTY_NAME_HIBERNATE_DIALECT, this.environment.getRequiredProperty(CustomStrings.PROPERTY_NAME_HIBERNATE_DIALECT));
        jpaProterties.put(CustomStrings.PROPERTY_NAME_HIBERNATE_FORMAT_SQL, this.environment.getRequiredProperty(CustomStrings.PROPERTY_NAME_HIBERNATE_FORMAT_SQL));
        jpaProterties.put(CustomStrings.PROPERTY_NAME_HIBERNATE_NAMING_STRATEGY, this.environment.getRequiredProperty(CustomStrings.PROPERTY_NAME_HIBERNATE_NAMING_STRATEGY));
        jpaProterties.put(CustomStrings.PROPERTY_NAME_HIBERNATE_SHOW_SQL, this.environment.getRequiredProperty(CustomStrings.PROPERTY_NAME_HIBERNATE_SHOW_SQL));
        LOGGER.debug(ToStringBuilder.reflectionToString(jpaProterties, ToStringStyle.MULTI_LINE_STYLE, true));
        return jpaProterties;
    }

    @Bean
    public JpaTransactionManager transactionManager() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        LOGGER.debug("entityManagerFactory===============>");
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        final DataSource dat = this.dataSource();
        transactionManager.setDataSource(dat);
        final EntityManagerFactory fac = this.entityManagerFactory();
        transactionManager.setEntityManagerFactory(fac);
        transactionManager.setJpaDialect(new HibernateJpaDialect());
        LOGGER.debug("<===============entityManagerFactory");
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation()
    {
        return new PersistenceExceptionTranslationPostProcessor();
    }
}
