/**
 * 
 */
package pl.anubis.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import pl.anubis.spring.security.filter.CsrfTokenResponseHeaderBindingFilter;
import pl.anubis.spring.security.filter.AuthenticationTokenProcessingFilter;

import javax.sql.DataSource;

/**
 * @author Anubis
 */
@Configuration
@EnableWebSecurity
public class SecurityConfigurerAdapter extends WebSecurityConfigurerAdapter
{
    @Autowired
    private DataSource dataSource;

    @Autowired
    private UserDetailsService motoUserDetailService;


    @Bean
    public AuthenticationEntryPoint entryPoint() {
        //BasicAuthenticationEntryPoint basicAuthEntryPoint = new BasicAuthenticationEntryPoint();
        //basicAuthEntryPoint.setRealmName("MotoRealmScore");

        UnauthorisedEntryPoint basicAuthEntryPoint = new UnauthorisedEntryPoint();
        //basicAuthEntryPoint.setRealmName("My Realm");

        return basicAuthEntryPoint;
    }

    /*@Override
    public void configure(final HttpSecurity http) throws Exception
    {
        final HttpSessionCsrfTokenRepository tokenRepository = new HttpSessionCsrfTokenRepository();
        tokenRepository.setHeaderName("X-XSRF-TOKEN");

        http.csrf().csrfTokenRepository(tokenRepository);
    }*/


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.jdbcAuthentication()
                .dataSource(dataSource).authoritiesByUsernameQuery(
                    "select usrs.username, rls.role From users usrs JOIN roles rls ON usrs.id = rls.id and usrs.username = ?")
                .and()
                    .userDetailsService(motoUserDetailService).passwordEncoder(passwordEncoder())
                //.and().inMemoryAuthentication().withUser("test").password("admin").roles("ADMIN", "MODERATOR")
                ;

       // AbstractAuthenticationFilterConfigurer d;

       // d.loginProcessingUrl("/j_spring_security_check");
        //auth.jdbcAuthentication().dataSource(dataSource)
        //        .usersByUsernameQuery(
        //                "select login,password, enabled from users where login=?")
        //       .authoritiesByUsernameQuery(
        //                "select login, role from user_roles where login=?");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        final HttpSessionCsrfTokenRepository tokenRepository = new HttpSessionCsrfTokenRepository();
        tokenRepository.setHeaderName("X-CSRF-TOKEN");

        http.csrf().csrfTokenRepository(tokenRepository).and()//.disable()//.and()//.disable()
                .addFilterAfter(new CsrfTokenResponseHeaderBindingFilter(), CsrfFilter.class)
                //Configures form login
                .exceptionHandling()
                .authenticationEntryPoint(entryPoint()).and()

                .formLogin()
                    .loginPage("/user-login")///index?login")
                    .loginProcessingUrl("/j_spring_security_check")
                    .failureUrl("/error-login")
                    .defaultSuccessUrl("/index")
                            //Configures the logout function
                .and()
                    .logout()
                        .deleteCookies("JSESSIONID")
                        .logoutUrl("/logout")
                        .logoutSuccessUrl("/index")
                .and()
                    .addFilterBefore(new AuthenticationTokenProcessingFilter(motoUserDetailService), BasicAuthenticationFilter.class)
                    //.addFilter(new AuthenticationTokenProcessingFilter(motoUserDetailService))
                    .authorizeRequests()
                            //Anyone can access the urls
                    .antMatchers("/", "/index", "/user-login", "/secretpath1977/createAdmins").permitAll()
                    //The rest of the our application is protected.
                    .antMatchers("/admin/**").hasRole("ADMIN").antMatchers("/admin/**").hasRole("MODERATOR")
                .and()
                .httpBasic();//.and()
                //.sessionManagement()
                //.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        //.anyRequest().authenticated()
                //;



//        http.userDetailsService(motoUserDetailService).authorizeRequests()
//                .antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
//                .and()
//                .formLogin().
//                loginPage("/user-login").
//                defaultSuccessUrl("/success-login")
//                .failureUrl("/error-login")
//                .usernameParameter("username").passwordParameter("password")
//                .and()
//                .logout()
//                .logoutSuccessUrl("/success-logout")
//                .and()
//                .exceptionHandling().accessDeniedPage("/403")
//                .and()
//                .csrf();
    }

    @Override
    public void configure(WebSecurity web) throws Exception
    {
        web.ignoring().antMatchers("/resources/**");
        //http.authorizeRequests().antMatchers("/resources/**").permitAll().anyRequest().authenticated().and().formLogin().loginPage("/login").permitAll().and()
        //.logout().permitAll();
    }

   // @Bean
    //public AuthenticationTokenProcessingFilter tokenInfoTokenFilterSecurityInterceptor() throws Exception
   // {
   //     return new AuthenticationTokenProcessingFilter(motoUserDetailService);
   // }

//    @Bean
//    public TokenFilterSecurityInterceptor<TokenTransfer> tokenInfoTokenFilterSecurityInterceptor() throws Exception
//    {
//        TokenService<TokenInfo> tokenService = new TokenServiceImpl(userInfoCache);
//        return new TokenFilterSecurityInterceptor<TokenInfo>(tokenService, serverStatusService, "RUN_ROLE");
//    }


    @Bean
    public PasswordEncoder passwordEncoder()
    {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);
        return encoder;
    }

    @Bean
    public AuthenticationManager getAuthenticationManager() throws Exception {
        return super.authenticationManager();
    }

    /*@Bean
    public SaltSource saltSource()
    {
        final ReflectionSaltSource saltSource = new ReflectionSaltSource();
        saltSource.setUserPropertyToUse("username");
        return saltSource;
    }*/


    // ...
}
