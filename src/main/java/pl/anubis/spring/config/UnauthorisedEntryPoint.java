package pl.anubis.spring.config;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Anubis on 2014-07-17.
 */
@Component(value = "unauthorisedEntryPoint")
public class UnauthorisedEntryPoint implements AuthenticationEntryPoint
{
    @Override
    public void commence(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException authException) throws
            IOException,
            ServletException
    {
        //response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        //response.sendRedirect(request.getContextPath()+"/user-login");
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
        //response.sendError(
          //      HttpServletResponse.SC_UNAUTHORIZED,
            //    "Unauthorized: Authentication token was either missing or invalid.");
    }
}