package pl.anubis.spring.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

import javax.servlet.ServletContext;

/**
 * Created by Anubis on 14.06.14.
 */
@Configuration
@EnableWebSecurity
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer
{
    /**
     * Invoked after the springSecurityFilterChain is added.
     * @param servletContext the {@link ServletContext}
     */
   // protected void afterSpringSecurityFilterChain(ServletContext servletContext) {
   //     servletContext.addFilter("authenticationTokenProcessingFilter", new AuthenticationTokenProcessingFilter());
    //}


    @Override
    protected void beforeSpringSecurityFilterChain(ServletContext servletContext) {
        // insertFilters(servletContext, new CsrfTokenResponseHeaderBindingFilter());
    }
}