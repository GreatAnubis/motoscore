/**
 * 
 */
package pl.anubis.spring.config;

/**
 * @author Anubis
 */
public class CustomStrings
{
    public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd HH:mm";

    public static final String MAPPING_URL = "/";

    public static final String VIEW_RESOLVER_PREFIX = "/WEB-INF/views/";
    public static final String VIEW_RESOLVER_SUFFIX = ".jsp";

    public static final String PROPERTY_NAME_DATABASE_DRIVER = "db.driver";
    public static final String PROPERTY_NAME_DATABASE_PASSWORD = "db.password";
    public static final String PROPERTY_NAME_DATABASE_URL = "db.url";
    public static final String PROPERTY_NAME_DATABASE_USERNAME = "db.username";

    public static final String PROPERTY_NAME_HIBERNATE_DIALECT = "hibernate.dialect";
    public static final String PROPERTY_NAME_HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
    public static final String PROPERTY_NAME_HIBERNATE_NAMING_STRATEGY = "hibernate.ejb.naming_strategy";
    public static final String PROPERTY_NAME_HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    public static final String PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN = "entitymanager.packages.to.scan";

    public static final String PROPERTY_NAME_MESSAGESOURCE_BASENAME = "message.source.basename";
    public static final String PROPERTY_NAME_MESSAGESOURCE_USE_CODE_AS_DEFAULT_MESSAGE = "message.source.use.code.as.default.message";

}
