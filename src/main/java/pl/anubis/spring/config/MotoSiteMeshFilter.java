package pl.anubis.spring.config;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;
import org.sitemesh.content.tagrules.html.Sm2TagRuleBundle;

/**
 * Created by Anubis on 07.05.14.
 */

public class MotoSiteMeshFilter extends ConfigurableSiteMeshFilter
{
    @Override
    protected void applyCustomConfiguration(SiteMeshFilterBuilder builder)
    {
        builder.addDecoratorPath("/*", "/WEB-INF/decorators/decorator.jsp");
        builder.addExcludedPath("/resources/*")
                .addExcludedPath("*/include/*")
                .addExcludedPath("*/containers/*")
                .addExcludedPath("*/angular/*")
                .addExcludedPath("*/layouts/*")
                .addExcludedPath("*/index/events*")
                .addExcludedPath("*index_ang.jsp");//.addDecoratorPath("/admin/*", "/admin/decorator.jsp");
        builder.setMimeTypes("text/html", "application/xhtml+xml", "application/vnd.wap.xhtml+xml");
        builder.addTagRuleBundle(new Sm2TagRuleBundle());

    }
}
