package pl.anubis.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.mobile.device.DeviceHandlerMethodArgumentResolver;
import org.springframework.mobile.device.DeviceResolverHandlerInterceptor;
import org.springframework.mobile.device.site.SitePreferenceHandlerInterceptor;
import org.springframework.mobile.device.site.SitePreferenceHandlerMethodArgumentResolver;
import org.springframework.mobile.device.view.LiteDeviceDelegatingViewResolver;
import org.springframework.util.StringUtils;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.util.List;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "pl.anubis.spring")
public class WebMvcConfig extends WebMvcConfigurerAdapter
{
    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry)
    {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
        registry.addResourceHandler("/layouts/**").addResourceLocations("/layouts/");
        //registry.addResourceHandler("/user/**");


    }

    @Bean
    public DeviceResolverHandlerInterceptor deviceResolverHandlerInterceptor()
    {
        return new DeviceResolverHandlerInterceptor();
    }

    @Bean
    public DeviceHandlerMethodArgumentResolver deviceHandlerMethodArgumentResolver()
    {
        return new DeviceHandlerMethodArgumentResolver();
    }
    @Bean
    public SitePreferenceHandlerInterceptor sitePreferenceHandlerInterceptor()
    {
        return new SitePreferenceHandlerInterceptor();
    }

    @Bean
    public SitePreferenceHandlerMethodArgumentResolver sitePreferenceHandlerMethodArgumentResolver()
    {
        return new SitePreferenceHandlerMethodArgumentResolver();
    }
    @Bean
    public LocaleResolver localeResolver() {

        CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
        cookieLocaleResolver.setDefaultLocale(StringUtils.parseLocaleString("en"));
        return cookieLocaleResolver;
    }
    @Override
    public void addInterceptors(final InterceptorRegistry registry)
    {
        LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
        localeChangeInterceptor.setParamName("lang");
        registry.addInterceptor(localeChangeInterceptor);

        registry.addInterceptor(this.deviceResolverHandlerInterceptor());
        registry.addInterceptor(this.sitePreferenceHandlerInterceptor());
    }
    @Bean
    public ReloadableResourceBundleMessageSource messageSource()
    {
        ReloadableResourceBundleMessageSource res = new ReloadableResourceBundleMessageSource();
        res.setBasename("/WEB-INF/messages/messages");
        res.setDefaultEncoding("UTF-8");
        res.setCacheSeconds(0);
        res.setFallbackToSystemLocale(true);
        res.setUseCodeAsDefaultMessage(true);
        res.setAlwaysUseMessageFormat(true);
        return res;
    }

    @Override
    public void addArgumentResolvers(final List<HandlerMethodArgumentResolver> argumentResolvers)
    {
        argumentResolvers.add(this.deviceHandlerMethodArgumentResolver());
        argumentResolvers.add(this.sitePreferenceHandlerMethodArgumentResolver());
    }

    @Bean
    public LiteDeviceDelegatingViewResolver liteDeviceAwareViewResolver()
    {
        InternalResourceViewResolver delegate = new InternalResourceViewResolver();
        delegate.setPrefix(CustomStrings.VIEW_RESOLVER_PREFIX);
        delegate.setSuffix(".jsp");
        delegate.setViewClass(JstlView.class);
        LiteDeviceDelegatingViewResolver resolver = new LiteDeviceDelegatingViewResolver(delegate);
        //resolver.setMobilePrefix("mobile/");
        //resolver.setTabletPrefix("tablet/");
        return resolver;
    }

    /*
     * @Override
     * public void addViewControllers(final ViewControllerRegistry registry)
     * {
     * registry.addViewController("/login").setViewName("login");
     * registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
     * }
     */

}
