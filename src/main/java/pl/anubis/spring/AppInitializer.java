package pl.anubis.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.DispatcherServlet;
import pl.anubis.spring.config.*;

import javax.servlet.*;
import java.util.EnumSet;


public class AppInitializer implements WebApplicationInitializer
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AppInitializer.class);

    @Override
    public void onStartup(final ServletContext servletContext) throws ServletException
    {
        LOGGER.debug("onStartup===============>");
        WebApplicationContext context = this.getContext();
        servletContext.addListener(new ContextLoaderListener(context));
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        servletContext.addFilter("characterEncodingFilter",characterEncodingFilter);
        servletContext.addFilter("hiddenHttpMethodFilter", new HiddenHttpMethodFilter());
        //servletContext.addFilter("authenticationTokenProcessingFilter", new AuthenticationTokenProcessingFilter());
        //FilterRegistration.Dynamic reg = servletContext.addFilter("SiteMeshFilter","com.opensymphony.sitemesh.webapp.SiteMeshFilter");
        FilterRegistration.Dynamic reg = servletContext.addFilter("sitemesh", new MotoSiteMeshFilter());
        EnumSet<DispatcherType> disps = EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD);
        reg.addMappingForUrlPatterns(disps, true, "/*");

        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping(CustomStrings.MAPPING_URL);

        LOGGER.debug("<===============onStartup");
    }

    private AnnotationConfigWebApplicationContext getContext()
    {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        // context.setConfigLocation(CONFIG_LOCATION);
        context.register(JPAConfig.class, WebMvcConfig.class , SecurityConfigurerAdapter.class );

        return context;
    }
}

