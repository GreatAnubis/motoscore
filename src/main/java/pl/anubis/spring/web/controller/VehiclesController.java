package pl.anubis.spring.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.anubis.spring.data.repository.vehicles.VehicleAlreadyExistsException;
import pl.anubis.spring.data.repository.vehicles.VehicleNotFoundException;
import pl.anubis.spring.data.repository.vehicles.VehicleService;
import pl.anubis.spring.model.Vehicle;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Anubis on 12.06.14.
 */
@Controller
@RequestMapping("/admin/vehicles")
public class VehiclesController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(VehiclesController.class);

    @Autowired
    private VehicleService vehicleService;
    @Autowired
    private ReloadableResourceBundleMessageSource messageSource;

    @RequestMapping("/")
    public String root()
    {
        return "redirect:/admin/vehicles/list";
    }

    @RequestMapping("")
    public String rootEmpty()
    {
        return "redirect:/admin/vehicles/list";
    }
    @ModelAttribute("vehicle")
    public Vehicle prepareVehicleModel()
    {
        LOGGER.debug("new Vehicle()");
        return new Vehicle();
    }

    @RequestMapping("/list")
    public String list(final Model model)
    {
        final List<Vehicle> vehicles = vehicleService.findAll();
        model.addAttribute("vehicles", vehicles);

        return "vehicles/list";
    }

    @RequestMapping(value = "/add", method = POST, produces = "text/plain; charset=utf-8")
    @ResponseStatus(HttpStatus.CREATED)
    public
    @ResponseBody
    String add(final HttpSession session, @Valid final Vehicle vehicleForm, final BindingResult result, Locale locale) throws VehicleAlreadyExistsException
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            return errorMessage;
        }
        Vehicle vehicle = vehicleService.findByName(vehicleForm.getVehicle_name());
        if (vehicle != null)
        {
            LOGGER.debug(vehicleForm.getVehicle_name() + ": Exists");
            return "EXISTS";
        }
        LOGGER.debug("Creating vehicle: " + vehicleForm.getVehicle_name());
        vehicle = vehicleService.create(vehicleForm);
        final String strResult = vehicle != null ? "OK" : "Unknown error";
        LOGGER.debug("Creating vehicle: " + strResult);
        return strResult;
    }

    @RequestMapping(value = "/edit", method = POST, produces = "text/plain; charset=utf-8")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    String edit(final HttpSession session, @Valid final Vehicle vehicleForm, final BindingResult result, Locale locale) throws VehicleAlreadyExistsException, VehicleNotFoundException
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            return errorMessage;
        }
        Vehicle vehicle = vehicleService.findByName(vehicleForm.getVehicle_name());
        if (vehicle != null)
        {
            LOGGER.debug(vehicleForm.getVehicle_name() + ": Exists");
            return "EXISTS";
        }
        LOGGER.debug("Updating vehicle: " + vehicleForm.getVehicle_name());
        vehicle = vehicleService.update(vehicleForm);
        final String strResult = vehicle != null ? "OK" : "Unknown error";
        LOGGER.debug("Updating vehicle: " + strResult);
        return strResult;
    }

    @RequestMapping(value = "/delete", method = POST, produces = "text/plain; charset=utf-8")
    public
    @ResponseBody
    String delete(final HttpSession session, @RequestParam(value = "vehicle_id", required = true) Integer id) throws Exception
    {
        try
        {
            LOGGER.debug("Deleting vehicle (" + id + ")");
            Vehicle vehicle = vehicleService.delete(id);
            LOGGER.debug("Deleted vehicle (" + id + "): " + vehicle.getVehicle_name());
        }
        catch (final VehicleNotFoundException ex)
        {
            return "NOTEXISTS";
        }
        return "OK";
    }

}
