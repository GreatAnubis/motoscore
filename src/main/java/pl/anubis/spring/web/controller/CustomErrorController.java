package pl.anubis.spring.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

/**
 * Created by Anubis on 05.05.14.
 */
@Controller
public class CustomErrorController
{
    @ExceptionHandler({NullPointerException.class, NoSuchRequestHandlingMethodException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "My Response Status Change….!!")
    public ModelAndView handleExceptionArray(Exception ex)
    {

        //logger.info("handleExceptionArray - Catching: " + ex.getClass().getSimpleName());
        return errorModelAndView(ex);
    }

    /**
     * Get the users details for the 'personal' page
     */
    private ModelAndView errorModelAndView(Exception ex)
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error");
        modelAndView.addObject("name", ex.getClass().getSimpleName());
        //modelAndView.addObject("user", userDao.readUserName());

        return modelAndView;
    }
}
