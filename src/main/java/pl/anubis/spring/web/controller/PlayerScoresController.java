package pl.anubis.spring.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import pl.anubis.spring.data.repository.player.PlayerService;
import pl.anubis.spring.data.repository.playerscoreinevent.PlayerScoreAlreadyExistsException;
import pl.anubis.spring.data.repository.playerscoreinevent.PlayerScoreNotFoundException;
import pl.anubis.spring.data.repository.playerscoreinevent.PlayerScoreService;
import pl.anubis.spring.data.repository.racingevent.RacingEventNotFoundException;
import pl.anubis.spring.data.repository.racingevent.RacingEventService;
import pl.anubis.spring.data.repository.track.TrackService;
import pl.anubis.spring.data.repository.trackinevent.TrackInEventService;
import pl.anubis.spring.data.repository.vehicles.VehicleService;
import pl.anubis.spring.model.*;
import pl.anubis.spring.mvc.container.VehicleAssigningDTO;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.util.List;
import java.util.Locale;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Anubis on 22.05.14.
 */
@Controller
@RequestMapping("/admin/playerscores")
public class PlayerScoresController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerScoresController.class);

    @Autowired
    private PlayerScoreService playerScoreService;

    @Autowired
    private RacingEventService racingEventService;

    @Autowired
    private ReloadableResourceBundleMessageSource messageSource;
    @Autowired
    private PlayerService repositoryPlayerService;


    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private TrackService trackService;

    @ModelAttribute("trackInEvent")
    public TrackInEvent getTrackInEvent()
    {
        return new TrackInEvent();
    }


    @ModelAttribute("vehicleAssigningDTO")
    public VehicleAssigningDTO getVehicleAssigningDTO()
    {
        return new VehicleAssigningDTO();
    }

    @ModelAttribute("availableTracks")
    public List<Track> prepareTrackListModel()
    {
        LOGGER.debug("new RacingEvent()");
        return trackService.findAll();
    }


    @ModelAttribute("availableVehicles")
    public List<Vehicle> prepareVehicleListModel()
    {
        LOGGER.debug("new RacingEvent()");
        return vehicleService.findAll();
    }


    @Autowired
    private TrackInEventService trackInEventService;

    @RequestMapping(value = "/{eventid}/{trackid}/edit", method = RequestMethod.GET)
    public String prepareEditRacingEvent(@PathVariable("eventid") Integer racingEventId, @PathVariable("trackid") Integer trackid, Model model) throws RacingEventNotFoundException
    {
        final TrackInEvent trackInEvent = trackInEventService.findById(trackid);
        model.addAttribute("trackInEvent", trackInEvent);
        final PlayerScoreInRun playerScoreInRun = new PlayerScoreInRun();
        playerScoreInRun.setTrackInEvent(trackInEvent);

        final List<Player> availablePlayers = repositoryPlayerService.findAll();
        for (PlayerScoreInRun playerScoreInRunLoop : trackInEvent.getPlayerScoresInRun())
        {
            if (availablePlayers.contains(playerScoreInRunLoop.getPlayer()))
            {
                availablePlayers.remove(playerScoreInRunLoop.getPlayer());
            }
        }

        final VehicleAssigningDTO vehicleAssigningDTO = new VehicleAssigningDTO();
        vehicleAssigningDTO.setTrackInEvent(trackInEvent);
        model.addAttribute("availablePlayers", availablePlayers);
        model.addAttribute("playerScoreInRun", playerScoreInRun);

        model.addAttribute("eventid", racingEventId);
        model.addAttribute("trackid", trackid);
        model.addAttribute("vehicleAssigningDTO", vehicleAssigningDTO);



        return "playerscores/edit";
    }

    @RequestMapping(value = "/{eventid}/{trackid}/edittrack", method = POST, produces = "text/plain; charset=utf-8")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    String updateTrackInTrackInEvent(final HttpSession session, TrackInEvent trackInEventform,final BindingResult result,Locale locale) throws Exception
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            return errorMessage;
        }
        TrackInEvent updated = trackInEventService.updateTrack(trackInEventform);
        final String strResult = updated != null ? "OK" : "Unknown error";
        LOGGER.debug("Update Track in trackInEvent: " + strResult);
        return strResult;
    }

    @RequestMapping(value = "/{eventid}/{trackid}/add", method = POST)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public
    @ResponseBody
    String addRacingEvent(final HttpSession session, @PathVariable("eventid") Integer racingEventId, @PathVariable("trackid") Integer trackid, @Valid final PlayerScoreInRun playerScoreInRun, Model model, final BindingResult result, Locale locale) throws Exception
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            // racingEventForm.setId(racingEventId);
            //model.addAttribute("racingevent",racingEventForm);
            return errorMessage;//"racingevents/edit";
        }
        String strResult = "OK";
        LOGGER.debug("Creating racingEvent: " + playerScoreInRun.getPlayer().getPlayer_nick() + " Scores: " + playerScoreInRun.getScore());
        {
            final PlayerScoreInRun created;
            try
            {
                LOGGER.debug("Creating racingEvent: " + strResult);
                created = playerScoreService.create(playerScoreInRun);
                strResult = created != null ? "OK" : "Unknown error";
            }
            catch (PlayerScoreAlreadyExistsException e)
            {
                strResult = "Player score already created";
                LOGGER.debug(strResult);
            }
        }
        return strResult;
    }

    @RequestMapping(value = "/{eventid}/{trackid}/edit", method = POST)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public
    @ResponseBody
    String editPlayerScore(final HttpSession session, @PathVariable("eventid") Integer racingEventId, @PathVariable("trackid") Integer trackid, @Valid final PlayerScoreInRun playerScoreInRun, Model model, final BindingResult result, Locale locale) throws Exception
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            // racingEventForm.setId(racingEventId);
            //model.addAttribute("racingevent",racingEventForm);
            return errorMessage;//"racingevents/edit";
        }
        String strResult = "OK";
        LOGGER.debug("Updating racingEvent: " + playerScoreInRun.getId());
        {
            final PlayerScoreInRun updated;
            try
            {
                LOGGER.debug("Updating playerScoreInRun: " + strResult);
                updated = playerScoreService.updateScore(playerScoreInRun);
                strResult = updated != null ? "OK" : "Unknown error";
            }
            catch (PlayerScoreNotFoundException e)
            {
                strResult = "Player score not found";
                LOGGER.debug(strResult);
            }
        }
        return strResult;
    }

    @RequestMapping(value = "/{eventid}/{trackid}/delete", method = POST, produces = "text/plain; charset=utf-8")
    public
    @ResponseBody
    String delete(final HttpSession session, @RequestParam(value = "player_score_id", required = true) Integer id) throws Exception
    {
        try
        {
            LOGGER.debug("Deleting PlayerScore (" + id + ")");
            PlayerScoreInRun playerScore = playerScoreService.delete(id);
            LOGGER.debug("Deleted PlayerScore (" + id + "): " + playerScore.getScore());
        }
        catch (final PlayerScoreNotFoundException ex)
        {
            return "NOTEXISTS";
        }
        return "OK";
    }

    @RequestMapping(value = "/{eventid}/{trackid}/assign", method = POST, produces = "text/plain; charset=utf-8")
    public String assign(final HttpSession session, @PathVariable("trackid") Integer trackid, VehicleAssigningDTO vehicleAssigningDTO) throws Exception
    {
        //  try
        //  {
        LOGGER.debug("Updating trackInEvent (" + vehicleAssigningDTO.getTrackInEvent().getId() + ")");
        trackInEventService.updateVehicles(vehicleAssigningDTO.getTrackInEvent());
        //PlayerScoreInRun playerScore = playerScoreService.delete(id);
        //LOGGER.debug("Deleted PlayerScore ("+id+"): " +playerScore.getScore());
        //  }
        // catch(final PlayerScoreNotFoundException ex)
        // {
        return "redirect:edit";
        //  }
        //  return  "OK";
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder)
    {
        binder.registerCustomEditor(Player.class, new PropertyEditorSupport()
        {
            @Override
            public void setAsText(String text)
            {
                Player custom = repositoryPlayerService.findById(Integer.parseInt(text));
                setValue(custom);
            }
        });
        binder.registerCustomEditor(TrackInEvent.class, new PropertyEditorSupport()
        {
            @Override
            public void setAsText(String text)
            {
                TrackInEvent custom = trackInEventService.findById(Integer.parseInt(text));
                setValue(custom);
            }
        });
        binder.registerCustomEditor(Track.class, new PropertyEditorSupport()
        {
            @Override
            public void setAsText(String text)
            {
                Track custom = trackService.findById(Integer.parseInt(text));
                setValue(custom);
            }
        });
        binder.registerCustomEditor(Vehicle.class, new PropertyEditorSupport()
        {
            @Override
            public void setAsText(String text)
            {
                Vehicle custom = vehicleService.findById(Integer.parseInt(text));
                setValue(custom);
            }
        });
    }

}
