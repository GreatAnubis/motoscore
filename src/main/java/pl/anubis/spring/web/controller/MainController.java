/**
 *
 */
package pl.anubis.spring.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import pl.anubis.spring.data.repository.player.PlayerService;
import pl.anubis.spring.data.repository.racingevent.RacingEventService;
import pl.anubis.spring.data.repository.track.TrackService;
import pl.anubis.spring.data.repository.trackinevent.TrackInEventService;
import pl.anubis.spring.model.*;
import pl.anubis.spring.mvc.container.*;
import pl.anubis.spring.mvc.container.json.PlayerData;
import pl.anubis.spring.mvc.container.json.PlayerScoreDataRow;
import pl.anubis.spring.mvc.container.json.VehicleInfo;
import pl.anubis.spring.mvc.container.json.WinnerData;
import pl.anubis.spring.security.Sorters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @author Anubis
 */

/**
 * Application home page and login.
 */
@Controller
public class MainController// implements org.springframework.web.servlet.mvc.Controller
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private RacingEventService racingEventService;

    @Autowired
    private TrackService trackService;

    @Autowired
    private TrackInEventService trackInEventService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private MessageSource messageSource;


    @RequestMapping("/")
    public String root()
    {
        return "redirect:/angular/";
    }

    @RequestMapping(value = "/thisYearScore", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    WinnerData getScoreForYear()
    {

        Map<Player, Integer> scores = new HashMap<Player, Integer>();

        Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        date.add(Calendar.MONTH, -1);
        Calendar eventDate = Calendar.getInstance();
        final List<RacingEvent> racingEvents = racingEventService.findAllVisibleFromYear(date.get(Calendar.YEAR));

        WinnerData winnerData = new WinnerData(date.get(Calendar.YEAR) + "");

        for (RacingEvent racingEvent : racingEvents)
        {
            for (final TrackInEvent trackInEvent : racingEvent.getTracks())
            {
                for (final PlayerScoreInRun playerScoreInRun : trackInEvent.getPlayerScoresInRun())
                {
                    if (scores.containsKey(playerScoreInRun.getPlayer()))
                    {
                        Integer scor = scores.get(playerScoreInRun.getPlayer());
                        scor += playerScoreInRun.getScore();
                        scores.put(playerScoreInRun.getPlayer(), scor);
                    }
                    else
                    {
                        Integer scor = playerScoreInRun.getScore();
                        scores.put(playerScoreInRun.getPlayer(), scor);
                    }
                }
            }
        }

        List<PlayerScoreDataRow> scorePerMonth = new ArrayList<PlayerScoreDataRow>();
        for (Player player : scores.keySet())
        {
            scorePerMonth.add(new PlayerScoreDataRow(new PlayerData(player.getPlayer_nick()), scores.get(player).intValue()));
        }
        scorePerMonth = Sorters.getSorters().sortByScore(scorePerMonth);
        int lastScore = 0;
        int place = 1;
        int realPlace = 1;
        for (PlayerScoreDataRow scorePerMth : scorePerMonth)
        {
            if (lastScore > scorePerMth.getScore())
            {
                realPlace = place;
            }


            lastScore = scorePerMth.getScore();
            scorePerMth.setPlace(realPlace);

            place += 1;
        }
        winnerData.setScores(scorePerMonth);
        return winnerData;
    }


    @RequestMapping(value = "/lastMonthScore", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    WinnerData getScoreForMonth(@RequestParam(value = "month", required = false) String strmonth)
    {

        Map<Player, Integer> scores = new HashMap<Player, Integer>();

        Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        Integer month = date.get(Calendar.MONTH);
        if(strmonth == null || strmonth.isEmpty())
        {
            month = date.get(Calendar.MONTH);
        }
        else
        {
            if (strmonth != null && strmonth.length() != 0)
            {
                try
                {
                    month = Integer.parseInt(strmonth);
                }
                catch (NumberFormatException ex)
                {
                    month = date.get(Calendar.MONTH);
                }

            }
        }

        String msg = "month." + month;

        final List<RacingEvent> racingEvents = racingEventService.findAllVisibleFromYearAndMonth(date.get(Calendar.YEAR), month);

        WinnerData winnerData = new WinnerData(messageSource.getMessage(msg, null, Locale.getDefault()));

        for (RacingEvent racingEvent : racingEvents)
        {
            for (final TrackInEvent trackInEvent : racingEvent.getTracks())
            {
                for (final PlayerScoreInRun playerScoreInRun : trackInEvent.getPlayerScoresInRun())
                {
                    if (scores.containsKey(playerScoreInRun.getPlayer()))
                    {
                        Integer scor = scores.get(playerScoreInRun.getPlayer());
                        scor += playerScoreInRun.getScore();
                        scores.put(playerScoreInRun.getPlayer(), scor);
                    }
                    else
                    {
                        Integer scor = playerScoreInRun.getScore();
                        scores.put(playerScoreInRun.getPlayer(), scor);
                    }
                }
            }
        }

        List<PlayerScoreDataRow> scorePerMonth = new ArrayList<PlayerScoreDataRow>();
        for (Player player : scores.keySet())
        {
            scorePerMonth.add(new PlayerScoreDataRow(new PlayerData(player.getPlayer_nick()), scores.get(player).intValue()));
        }
        scorePerMonth = Sorters.getSorters().sortByScore(scorePerMonth);
        int lastScore = 0;
        int place = 1;
        int realPlace = 1;
        for (PlayerScoreDataRow scorePerMth : scorePerMonth)
        {
            if (lastScore > scorePerMth.getScore())
            {
                realPlace = place;
            }


            lastScore = scorePerMth.getScore();
            scorePerMth.setPlace(realPlace);

            place += 1;
        }
        winnerData.setScores(scorePerMonth);
        return winnerData;
    }

    @RequestMapping(value = "/lastEventScore", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    WinnerData getScoreForLastEvent()
    {

        Map<Player, Integer> scores = new HashMap<Player, Integer>();
        //Calendar eventDate = Calendar.getInstance();

        final RacingEvent racingEvent = racingEventService.findLastVisibleFromCurrentYear();

        WinnerData winnerData = new WinnerData(racingEvent.getTimestamp().toString());

        for (final TrackInEvent trackInEvent : racingEvent.getTracks())
        {
            for (final PlayerScoreInRun playerScoreInRun : trackInEvent.getPlayerScoresInRun())
            {
                if (scores.containsKey(playerScoreInRun.getPlayer()))
                {
                    Integer scor = scores.get(playerScoreInRun.getPlayer());
                    scor += playerScoreInRun.getScore();
                    scores.put(playerScoreInRun.getPlayer(), scor);
                }
                else
                {
                    Integer scor = playerScoreInRun.getScore();
                    scores.put(playerScoreInRun.getPlayer(), scor);
                }
            }
        }


        List<PlayerScoreDataRow> scorePerMonth = new ArrayList<PlayerScoreDataRow>();
        for (Player player : scores.keySet())
        {
            scorePerMonth.add(new PlayerScoreDataRow(new PlayerData(player.getPlayer_nick()), scores.get(player).intValue()));
        }
        scorePerMonth = Sorters.getSorters().sortByScore(scorePerMonth);
        int lastScore = 0;
        int place = 1;
        int realPlace = 1;
        for (PlayerScoreDataRow scorePerMth : scorePerMonth)
        {
            if (lastScore > scorePerMth.getScore())
            {
                realPlace = place;
            }


            lastScore = scorePerMth.getScore();
            scorePerMth.setPlace(realPlace);

            place += 1;
        }
        winnerData.setScores(scorePerMonth);
        return winnerData;
    }

    @RequestMapping(value = "/index/eventsjson", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<List<TableRacingEventRow>> indexJSONWithSorting(@RequestParam(value = "year") String stryear, @RequestParam(value = "month", required = false) String strmonth)
    {
        Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        Integer year = 0;
        Integer month = null;

        if (stryear == null || stryear.length() == 0)
        {
            year = date.get(Calendar.YEAR);
        }
        if (strmonth == null || strmonth.length() == 0)
        {
            strmonth = "all";
        }
        if (stryear != null)
        {
            try
            {
                year = Integer.parseInt(stryear);
            }
            catch (NumberFormatException ex)
            {
                year = date.get(Calendar.YEAR);
            }
        }
        if (strmonth != null && strmonth.length() != 0)
        {
            if (strmonth.compareToIgnoreCase("all") == 0)
            {
                month = null;
            }
            else
            {
                try
                {
                    month = Integer.parseInt(strmonth);
                }
                catch (NumberFormatException ex)
                {
                    month = date.get(Calendar.MONTH) + 1;
                }
            }
        }

        List<VehicleInfoForRacingEvent> vehRacingEvents = new ArrayList<VehicleInfoForRacingEvent>();
        final List<RacingEvent> racingevents = racingEventService.findAllVisibleFromYearAndMonth(year, month);
        List<List<TableRacingEventRow>> resultMap = new ArrayList<List<TableRacingEventRow>>();
        //Map<Player, TableRacingEventRow> listRacingEvents = new HashMap<Player, TableRacingEventRow>();
        for (RacingEvent racingEvent : racingevents)
        {
            Map<Player, TableRacingEventRow> listRacingEvents = new HashMap<Player, TableRacingEventRow>();
            VehicleInfoForRacingEvent vehRacingEvent = new VehicleInfoForRacingEvent();
            vehRacingEvent.setRacingEventId(racingEvent.getId());

            List<TrackInEvent> tracks = racingEvent.getTracks();
            for (final TrackInEvent trackInEvent : tracks)
            {
                if (!vehRacingEvent.getVehicles().containsKey(trackInEvent.getId()))
                {

                    Set<VehicleInfo> vehInfo = new HashSet<VehicleInfo>();
                    if (trackInEvent.getVehiclesInRun() != null)
                    {
                        for (Vehicle veh : trackInEvent.getVehiclesInRun())
                        {
                            vehInfo.add(new VehicleInfo(veh));
                        }
                    }
                    vehRacingEvent.getVehicles().put(trackInEvent.getId(), vehInfo);
                    vehRacingEvents.add(vehRacingEvent);
                }

                for (final PlayerScoreInRun playerScoreInRun : trackInEvent.getPlayerScoresInRun())
                {
                    if (playerScoreInRun.getTrackInEvent().getId() == trackInEvent.getId())
                    {
                        if (!listRacingEvents.containsKey(playerScoreInRun.getPlayer()))
                        {
                            final TableRacingEventRow tableRacingEventRow = new TableRacingEventRow();
                            tableRacingEventRow.setPlayer(playerScoreInRun.getPlayer());
                            tableRacingEventRow.setTotalScore(playerScoreInRun.getScore());
                            tableRacingEventRow.getPlayerScores().put(trackInEvent.getId(), playerScoreInRun.getScore());
                            listRacingEvents.put(playerScoreInRun.getPlayer(), tableRacingEventRow);
                        }
                        else
                        {
                            final TableRacingEventRow tableRacingEventRow = listRacingEvents.get(playerScoreInRun.getPlayer());
                            tableRacingEventRow.setTotalScore(tableRacingEventRow.getTotalScore() + playerScoreInRun.getScore());
                            tableRacingEventRow.getPlayerScores().put(trackInEvent.getId(), playerScoreInRun.getScore());
                            listRacingEvents.put(playerScoreInRun.getPlayer(), tableRacingEventRow);
                        }
                    }
                }
            }
            List<TableRacingEventRow> playersRow = new ArrayList<TableRacingEventRow>();
            for (Player player : listRacingEvents.keySet())
            {
                playersRow.add(listRacingEvents.get(player));
            }

            resultMap.add(Sorters.getSorters().sortByTotalScore(playersRow));
        }

        // Map<Player, Integer> scores = new HashMap<Player, Integer>();

        //model.addAttribute("vehRacingEvents", vehRacingEvents);
       // model.addAttribute("resultMap", resultMap);
       // model.addAttribute("racingevents", racingevents);

        //List<Object> resultList = new ArrayList<Object>();
        //resultList.add(vehRacingEvents);
        //resultList.add(resultMap);
        //resultList.add(racingevents);
        return resultMap;
        //return "containers/events";
    }


    @RequestMapping(value = "/index/events")
    public String indexWithSorting(final Model model, @RequestParam(value = "year") String stryear, @RequestParam(value = "month", required = false) String strmonth)
    {
        Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        Integer year = 0;
        Integer month = null;

        if (stryear == null || stryear.length() == 0)
        {
            year = date.get(Calendar.YEAR);
        }
        if (strmonth == null || strmonth.length() == 0)
        {
            strmonth = "all";
        }
        if (stryear != null)
        {
            try
            {
                year = Integer.parseInt(stryear);
            }
            catch (NumberFormatException ex)
            {
                year = date.get(Calendar.YEAR);
            }
        }
        if (strmonth != null && strmonth.length() != 0)
        {
            if (strmonth.compareToIgnoreCase("all") == 0)
            {
                month = null;
            }
            else
            {
                try
                {
                    month = Integer.parseInt(strmonth);
                }
                catch (NumberFormatException ex)
                {
                    month = date.get(Calendar.MONTH) + 1;
                }
            }
        }

        List<VehicleInfoForRacingEvent> vehRacingEvents = new ArrayList<VehicleInfoForRacingEvent>();
        final List<RacingEvent> racingevents = racingEventService.findAllVisibleFromYearAndMonth(year, month);
        List<List<TableRacingEventRow>> resultMap = new ArrayList<List<TableRacingEventRow>>();
        //Map<Player, TableRacingEventRow> listRacingEvents = new HashMap<Player, TableRacingEventRow>();
        for (RacingEvent racingEvent : racingevents)
        {
            Map<Player, TableRacingEventRow> listRacingEvents = new HashMap<Player, TableRacingEventRow>();
            VehicleInfoForRacingEvent vehRacingEvent = new VehicleInfoForRacingEvent();
            vehRacingEvent.setRacingEventId(racingEvent.getId());

            List<TrackInEvent> tracks = racingEvent.getTracks();
            for (final TrackInEvent trackInEvent : tracks)
            {
                if (!vehRacingEvent.getVehicles().containsKey(trackInEvent.getId()))
                {

                    Set<VehicleInfo> vehInfo = new HashSet<VehicleInfo>();
                    if (trackInEvent.getVehiclesInRun() != null)
                    {
                        for (Vehicle veh : trackInEvent.getVehiclesInRun())
                        {
                            vehInfo.add(new VehicleInfo(veh));
                        }
                    }
                    vehRacingEvent.getVehicles().put(trackInEvent.getId(), vehInfo);

                }

                for (final PlayerScoreInRun playerScoreInRun : trackInEvent.getPlayerScoresInRun())
                {
                    if (playerScoreInRun.getTrackInEvent().getId() == trackInEvent.getId())
                    {
                        if (!listRacingEvents.containsKey(playerScoreInRun.getPlayer()))
                        {
                            final TableRacingEventRow tableRacingEventRow = new TableRacingEventRow();
                            tableRacingEventRow.setPlayer(playerScoreInRun.getPlayer());
                            tableRacingEventRow.setTotalScore(playerScoreInRun.getScore());
                            tableRacingEventRow.getPlayerScores().put(trackInEvent.getId(), playerScoreInRun.getScore());
                            listRacingEvents.put(playerScoreInRun.getPlayer(), tableRacingEventRow);
                        }
                        else
                        {
                            final TableRacingEventRow tableRacingEventRow = listRacingEvents.get(playerScoreInRun.getPlayer());
                            tableRacingEventRow.setTotalScore(tableRacingEventRow.getTotalScore() + playerScoreInRun.getScore());
                            tableRacingEventRow.getPlayerScores().put(trackInEvent.getId(), playerScoreInRun.getScore());
                            listRacingEvents.put(playerScoreInRun.getPlayer(), tableRacingEventRow);
                        }
                    }
                }
            }
            vehRacingEvents.add(vehRacingEvent);
            List<TableRacingEventRow> playersRow = new ArrayList<TableRacingEventRow>();
            for (Player player : listRacingEvents.keySet())
            {
                playersRow.add(listRacingEvents.get(player));
            }

            resultMap.add(Sorters.getSorters().sortByTotalScore(playersRow));
        }


        model.addAttribute("vehRacingEvents", vehRacingEvents);
        model.addAttribute("resultMap", resultMap);
        List<Player> lplayers = playerService.findAll();
        Map<Integer, PlayerData> players = new HashMap<Integer, PlayerData>();
        for(Player player : lplayers)
        {
            players.put(player.getId(), new PlayerData(player));
        }
        model.addAttribute("playerdata", players);
        model.addAttribute("racingevents", racingevents);

        return "containers/events";
    }

    @RequestMapping("/indexang")
    public String indexAng(final Model model)
    {
        return "index_ang";
    }

        /**
         * Home page.
         */
    @RequestMapping("/index")
    public String index(final Model model)
    {
        return "redirect:/angular/";
    }

    @RequestMapping("/index_old")
    public String oldindex(final Model model)
    {
        //        final List<RacingEvent> racingevents = racingEventService.findAllVisible();
        //
        //        List<List<TableRacingEventRow>> resultMap = new ArrayList<List<TableRacingEventRow>>();
        //        for(RacingEvent racingEvent : racingevents)
        //        {
        //            Map<Player, TableRacingEventRow> listRacingEvents = new HashMap<Player, TableRacingEventRow>();
        //            List<TrackInEvent> tracks = racingEvent.getTracks();
        //            for(final TrackInEvent trackInEvent : tracks)
        //            {
        //                for(final PlayerScoreInRun playerScoreInRun: trackInEvent.getPlayerScoresInRun())
        //                {
        //                    if(playerScoreInRun.getTrackInEvent().getId() == trackInEvent.getId())
        //                    {
        //                        if(!listRacingEvents.containsKey(playerScoreInRun.getPlayer()))
        //                        {
        //                            final TableRacingEventRow tableRacingEventRow = new TableRacingEventRow();
        //                            tableRacingEventRow.setPlayer(playerScoreInRun.getPlayer());
        //                            tableRacingEventRow.setTotalScore(playerScoreInRun.getScore());
        //                            tableRacingEventRow.getPlayerScores().put(trackInEvent.getId(), playerScoreInRun.getScore());
        //                            listRacingEvents.put(playerScoreInRun.getPlayer(),tableRacingEventRow);
        //                        }
        //                        else
        //                        {
        //                            final TableRacingEventRow tableRacingEventRow = listRacingEvents.get(playerScoreInRun.getPlayer());
        //                            tableRacingEventRow.setTotalScore(tableRacingEventRow.getTotalScore()+playerScoreInRun.getScore());
        //                            tableRacingEventRow.getPlayerScores().put(trackInEvent.getId(), playerScoreInRun.getScore());
        //                            listRacingEvents.put(playerScoreInRun.getPlayer(),tableRacingEventRow);
        //                        }
        //                    }
        //                }
        //            }
        //            List<TableRacingEventRow> playersRow = new ArrayList<TableRacingEventRow>();
        //            for(Player player : listRacingEvents.keySet())
        //            {
        //                playersRow.add(listRacingEvents.get(player));
        //            }
        //
        //            resultMap.add(Sorters.getSorters().sortByTotalScore(playersRow));
        //        }
        //
        //        Map<Player, Integer> scores = new HashMap<Player, Integer>();
        //
        //        model.addAttribute("resultMap", resultMap);
        //        model.addAttribute("racingevents", racingevents);

        List<Integer> months = new ArrayList<Integer>();
        Calendar dateMon = Calendar.getInstance();
        dateMon.setTime(new Date());
        int month = dateMon.get(Calendar.MONTH) + 1;
        for(int mon = month;mon > 0;mon--)
        {
            months.add(mon);
        }

        // Map<Player, Integer> scores = new HashMap<Player, Integer>();
        model.addAttribute("monthvars", months);

        return "index";
    }


    @RequestMapping(value = "/user-login", method = RequestMethod.GET)
    public ModelAndView loginForm()
    {
        return new ModelAndView("login-form");
    }

    @RequestMapping(value = "/error-login", method = RequestMethod.GET)
    public ModelAndView invalidLogin()
    {
        ModelAndView modelAndView = new ModelAndView("login-form");
        modelAndView.addObject("error", true);
        return modelAndView;
    }

    @RequestMapping(value = "/success-login", method = RequestMethod.GET)
    public ModelAndView successLogin()
    {
        return new ModelAndView("success-login");
    }

    @RequestMapping(value = "/success-logout", method = RequestMethod.GET)
    public ModelAndView successLogout()
    {
        return new ModelAndView("success-logout");
    }

    /**
     * Login form with error.
     */
    @RequestMapping("/login-error.jsp")
    public String loginError(final Model model)
    {
        model.addAttribute("loginError", true);
        return "login-form";
    }

    /**
     * Simulation of an exception.
     */
    @RequestMapping("/simulateError.jsp")
    public void simulateError()
    {
        throw new RuntimeException("This is a simulated error message");
    }

    /**
     * Error page.
     */
    @RequestMapping("/error.jsp")
    public String error(final HttpServletRequest request, final Model model)
    {
        model.addAttribute("errorCode", "Error " + request.getAttribute("javax.servlet.error.status_code"));
        Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
        StringBuilder errorMessage = new StringBuilder();
        errorMessage.append("<ul>");
        while (throwable != null)
        {
            errorMessage.append("<li>").append(this.escapeTags(throwable.getMessage())).append("</li>");
            throwable = throwable.getCause();
        }
        errorMessage.append("</ul>");
        model.addAttribute("errorMessage", errorMessage.toString());
        return "error.html";
    }

    /**
     * Substitute 'less than' and 'greater than' symbols by its HTML entities.
     */
    private String escapeTags(final String text)
    {
        if (text == null)
        {
            return null;
        }
        return text.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
    }


    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        String keyword = request.getParameter("keyword");
        if (keyword != null)
        {
            if (!StringUtils.hasLength(keyword))
            {
                return new ModelAndView("Error", "message", "Please enter a keyword to search for, then press the search button.");
            }
            PagedListHolder productList = new PagedListHolder(new ArrayList<String>());//this.petStore.searchProductList(keyword.toLowerCase()));
            productList.setPageSize(4);
            request.getSession().setAttribute("SearchProductsController_productList", productList);
            return new ModelAndView("SearchProducts", "productList", productList);
        }
        else
        {
            String page = request.getParameter("page");
            PagedListHolder productList = (PagedListHolder) request.getSession().getAttribute("SearchProductsController_productList");
            if (productList == null)
            {
                return new ModelAndView("Error", "message", "Your session has timed out. Please start over again.");
            }
            if ("next".equals(page))
            {
                productList.nextPage();
            }
            else if ("previous".equals(page))
            {
                productList.previousPage();
            }
            return new ModelAndView("SearchProducts", "productList", productList);
        }
    }
}
