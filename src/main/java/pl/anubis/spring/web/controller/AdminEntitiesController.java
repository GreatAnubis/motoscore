package pl.anubis.spring.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.anubis.spring.data.repository.secureuser.RoleService;
import pl.anubis.spring.data.repository.secureuser.UserAlreadyExistException;
import pl.anubis.spring.data.repository.secureuser.UserService;
import pl.anubis.spring.model.Role;
import pl.anubis.spring.model.SecuredUser;
import pl.anubis.spring.model.User;

/**
 * Created by Anubis on 14.06.14.
 */
@Controller
@RequestMapping("/secretpath1977")
public class AdminEntitiesController
{


    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;


    @Autowired
    private MessageSource messageSource;

    @RequestMapping("/createAdmins")
    public String createAdmins()
    {
        createBasicUser();

        return "redirect:/index";
    }


    private void createBasicUser()
    {

        Role role = new Role();
        role.setRole("ROLE_ADMIN");
        try
        {
            roleService.create(role);
        }
        catch (Exception ex)
        {

        }

        role = new Role();
        role.setRole("ROLE_MODERATOR");
        try
        {
            roleService.create(role);
        }
        catch (Exception ex)
        {

        }

        User nUser = new User("Admin","2mortal22");
        nUser.setEnabled(true);
        SecuredUser suser = new SecuredUser(nUser);


        User user = null;
        try
        {
            user =  userService.create(suser);
        }
        catch (UserAlreadyExistException e)
        {
            e.printStackTrace();
        }

        nUser = new User("Glineck","wR3cK4g3");
        nUser.setEnabled(true);
        suser = new SecuredUser(nUser);


        user = null;
        try
        {
            user =  userService.create(suser);
        }
        catch (UserAlreadyExistException e)
        {
            e.printStackTrace();
        }
    }
}
