/**
 *
 */
package pl.anubis.spring.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.anubis.spring.data.repository.player.PlayerNotFoundException;
import pl.anubis.spring.data.repository.player.PlayerService;
import pl.anubis.spring.model.Player;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Anubis
 */
@Controller
@RequestMapping("/admin/players")
public class PlayersController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(PlayersController.class);

    @Autowired
    private PlayerService repositoryPlayerService;

    @Autowired
    private ReloadableResourceBundleMessageSource messageSource;

    @RequestMapping("/")
    public String root()
    {
        return "redirect:/admin/players/list";
    }

    @RequestMapping("")
    public String rootEmpty()
    {
        return "redirect:/admin/players/list";
    }

    @ModelAttribute("player")
    public Player preparePlayerModel()
    {
        LOGGER.debug("new Player()");
        return new Player();
    }

    @RequestMapping("/list")
    public String list(final Model model)
    {
        final List<Player> players = repositoryPlayerService.findAll();
        model.addAttribute("players", players);

        return "players/list";
    }

    @RequestMapping(value = "/add", method = POST, produces = "text/plain; charset=utf-8")
    @ResponseStatus(HttpStatus.CREATED)
    public
    @ResponseBody
    String add(final HttpSession session, @Valid final Player playerForm, final BindingResult result, Locale locale) throws Exception
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            return errorMessage;
        }
        Player player = repositoryPlayerService.findByNick(playerForm.getPlayer_nick());
        if (player != null)
        {
            LOGGER.debug(playerForm.getPlayer_nick() + ": Exists");
            return "EXISTS";
        }
        LOGGER.debug("Creating player: " + playerForm.getPlayer_nick());
        player = repositoryPlayerService.create(playerForm);
        final String strResult = player != null ? "OK" : "Unknown error";
        LOGGER.debug("Creating player: " + strResult);
        return strResult;
    }

    @RequestMapping(value = "/edit", method = POST, produces = "text/plain; charset=utf-8")
    @ResponseStatus(HttpStatus.CREATED)
    public
    @ResponseBody
    String edit(final HttpSession session, @Valid final Player playerForm, final BindingResult result, Locale locale) throws Exception
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            return errorMessage;
        }
        Player player = repositoryPlayerService.findByNick(playerForm.getPlayer_nick());
        if (player != null)
        {
            LOGGER.debug(playerForm.getPlayer_nick() + ": Exists");
            return "EXISTS";
        }
        LOGGER.debug("Updating player: " + playerForm.getPlayer_nick());
        player = repositoryPlayerService.update(playerForm);
        final String strResult = player != null ? "OK" : "Unknown error";
        LOGGER.debug("Updating player result: " + strResult);
        return strResult;
    }

    @RequestMapping(value = "/delete", method = POST, produces = "text/plain; charset=utf-8")
    public
    @ResponseBody
    String delete(final HttpSession session, @RequestParam(value = "player_id", required = true) Integer id) throws Exception
    {
        try
        {
            LOGGER.debug("Deleting player (" + id + ")");
            Player player = repositoryPlayerService.delete(id);
            LOGGER.debug("Deleted player (" + id + "): " + player.getPlayer_nick());
        }
        catch (final PlayerNotFoundException ex)
        {
            return "NOTEXISTS";
        }
        return "OK";
    }
}
