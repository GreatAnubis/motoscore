package pl.anubis.spring.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.anubis.spring.data.repository.racingevent.RacingEventService;
import pl.anubis.spring.data.repository.track.TrackService;
import pl.anubis.spring.data.repository.trackinevent.TrackInEventService;
import pl.anubis.spring.model.Player;
import pl.anubis.spring.model.PlayerScoreInRun;
import pl.anubis.spring.model.RacingEvent;
import pl.anubis.spring.model.TrackInEvent;
import pl.anubis.spring.mvc.container.YearMonths;
import pl.anubis.spring.mvc.container.json.PlayerData;
import pl.anubis.spring.mvc.container.json.PlayerScoreDataRow;
import pl.anubis.spring.mvc.container.json.WinnerData;
import pl.anubis.spring.security.Sorters;

import java.util.*;

/**
 * Created by Anubis on 23.06.14.
 */
@Controller
@RequestMapping(value = "/angular")
public class MainControllerAngular
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private RacingEventService racingEventService;

    @Autowired
    private TrackService trackService;

    @Autowired
    private TrackInEventService trackInEventService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping("/")
    public String root()
    {
        return "index_ang";
    }

    @RequestMapping("/index")
    public String index()
    {
        return "index_ang";
    }

    private void fillWinnerData(final WinnerData winnerData, final List<RacingEvent> racingEvents)
    {
        Map<Player, Integer> scores = new HashMap<Player, Integer>();
        for (RacingEvent racingEvent : racingEvents)
        {
            for (final TrackInEvent trackInEvent : racingEvent.getTracks())
            {
                for (final PlayerScoreInRun playerScoreInRun : trackInEvent.getPlayerScoresInRun())
                {
                    if (scores.containsKey(playerScoreInRun.getPlayer()))
                    {
                        Integer scor = scores.get(playerScoreInRun.getPlayer());
                        scor += playerScoreInRun.getScore();
                        scores.put(playerScoreInRun.getPlayer(), scor);
                    }
                    else
                    {
                        Integer scor = playerScoreInRun.getScore();
                        scores.put(playerScoreInRun.getPlayer(), scor);
                    }
                }
            }
        }
        List<PlayerScoreDataRow> scorePerMonth = new ArrayList<PlayerScoreDataRow>();
        for (Player player : scores.keySet())
        {
            scorePerMonth.add(new PlayerScoreDataRow(new PlayerData(player.getPlayer_nick()), scores.get(player).intValue()));
        }
        scorePerMonth = Sorters.getSorters().sortByScore(scorePerMonth);

        int lastScore = 0;
        int place = 1;
        int realPlace = 1;
        for (PlayerScoreDataRow scorePerMth : scorePerMonth)
        {
            if (lastScore == 0)
            {
                lastScore = scorePerMth.getScore();
                scorePerMth.setPlace(realPlace);
            }
            if (lastScore > scorePerMth.getScore())
            {
                realPlace = place;
                scorePerMth.setPlace(realPlace);
            }

            place += 1;
        }
        winnerData.setScores(scorePerMonth);
    }

    @RequestMapping(value = "/sidebaryearsandmonths.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<YearMonths> getYearsAndMonths()
    {
        List<YearMonths> yearsMonths = new ArrayList<YearMonths>();
        YearMonths yearMonth = new YearMonths(2014);
        yearMonth.addMonth(6);
        yearMonth.addMonth(5);
        yearMonth.addMonth(4);
        yearMonth.addMonth(3);
        yearMonth.addMonth(2);
        yearMonth.addMonth(1);
        yearsMonths.add(yearMonth);

        return yearsMonths;
    }

    @RequestMapping(value = "/currentYearScore.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody WinnerData getScoreForYear()
    {
        final Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        date.add(Calendar.MONTH, -1);
        final List<RacingEvent> racingEvents = racingEventService.findAllVisibleFromYear(date.get(Calendar.YEAR));
        final WinnerData winnerData = new WinnerData(date.get(Calendar.YEAR) + "");
        winnerData.setScoresTitle("Punktacja roku: ");
        fillWinnerData(winnerData, racingEvents);

        return winnerData;
    }

    @RequestMapping(value = "/monthScore.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody WinnerData getScoreForMonth()
    {
        final Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        date.add(Calendar.MONTH, -1);
        final String msg = "month." + (date.get(Calendar.MONTH) + 1);
        final List<RacingEvent> racingEvents = racingEventService.findAllVisibleFromYearAndMonth(date.get(Calendar.YEAR), date.get(Calendar.MONTH) + 1);
        final WinnerData winnerData = new WinnerData(messageSource.getMessage(msg, null, Locale.getDefault()));
        winnerData.setScoresTitle("Punktacja miesiąca: ");
        fillWinnerData(winnerData, racingEvents);

        return winnerData;
    }

    @RequestMapping(value = "/lastEventScore.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody WinnerData getScoreForLastEvent()
    {
        final Map<Player, Integer> scores = new HashMap<Player, Integer>();
        final Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        date.add(Calendar.MONTH, -1);
        final RacingEvent racingEvent = racingEventService.findLastVisibleFromCurrentYear();
        final WinnerData winnerData = new WinnerData("");//racingEvent.getTimestamp().toString());
        winnerData.setScoresTitle("Punktacja ostatniego eventu");
        fillWinnerData(winnerData, new ArrayList<RacingEvent>(){
            {
                add(racingEvent);
            }
        });

        return winnerData;
    }

}
