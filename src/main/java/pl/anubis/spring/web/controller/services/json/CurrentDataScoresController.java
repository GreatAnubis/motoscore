package pl.anubis.spring.web.controller.services.json;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.anubis.spring.data.repository.racingevent.RacingEventService;
import pl.anubis.spring.data.repository.track.TrackService;
import pl.anubis.spring.data.repository.trackinevent.TrackInEventService;
import pl.anubis.spring.model.Player;
import pl.anubis.spring.model.PlayerScoreInRun;
import pl.anubis.spring.model.RacingEvent;
import pl.anubis.spring.model.TrackInEvent;
import pl.anubis.spring.mvc.container.YearMonths;
import pl.anubis.spring.mvc.container.json.PlayerData;
import pl.anubis.spring.mvc.container.json.PlayerScoreDataRow;
import pl.anubis.spring.mvc.container.json.WinnerData;
import pl.anubis.spring.security.Sorters;
import pl.anubis.spring.util.MessageUtils;

import java.util.*;

/**
 * Created by Anubis on 2014-07-11.
 */
@Controller
@RequestMapping(value = "/current")
public class CurrentDataScoresController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(CurrentDataScoresController.class);

    @Autowired
    private RacingEventService racingEventService;

    @Autowired
    private TrackService trackService;

    @Autowired
    private TrackInEventService trackInEventService;

    @Autowired
    private MessageUtils messageUtils;

    ////////////////////////////////////////////////////////////////


    private void fillWinnerData(final WinnerData winnerData, final List<RacingEvent> racingEvents)
    {
        Map<Player, Integer> scores = new HashMap<Player, Integer>();
        for (RacingEvent racingEvent : racingEvents)
        {
            for (final TrackInEvent trackInEvent : racingEvent.getTracks())
            {
                for (final PlayerScoreInRun playerScoreInRun : trackInEvent.getPlayerScoresInRun())
                {
                    if (scores.containsKey(playerScoreInRun.getPlayer()))
                    {
                        Integer score = scores.get(playerScoreInRun.getPlayer());
                        score += playerScoreInRun.getScore();
                        scores.put(playerScoreInRun.getPlayer(), score);
                    }
                    else
                    {
                        final Integer score = playerScoreInRun.getScore();
                        scores.put(playerScoreInRun.getPlayer(), score);
                    }
                }
            }
        }
        List<PlayerScoreDataRow> scorePerMonth = new ArrayList<PlayerScoreDataRow>();
        for (Player player : scores.keySet())
        {
            scorePerMonth.add(new PlayerScoreDataRow(new PlayerData(player.getPlayer_nick()), scores.get(player).intValue()));
        }
        scorePerMonth = Sorters.getSorters().sortByScore(scorePerMonth);

        int lastScore = 0;
        int place = 1;
        int realPlace = 1;
        for (PlayerScoreDataRow scorePerMth : scorePerMonth)
        {
            if (lastScore == 0)
            {
                lastScore = scorePerMth.getScore();
                scorePerMth.setPlace(realPlace);
            }
            if (lastScore > scorePerMth.getScore())
            {
                realPlace = place;
                scorePerMth.setPlace(realPlace);
            }
            else if (lastScore == scorePerMth.getScore())
            {
                scorePerMth.setPlace(realPlace);
            }

            place += 1;
        }
        winnerData.setScores(scorePerMonth);
    }



    @RequestMapping(value = "/racingeventsyearmonths.json", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<YearMonths> getYearsAndMonths()
    {
        return racingEventService.getRacingEventsYearMonths();
    }

    @RequestMapping(value = "/currentYearScore.json", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    WinnerData getScoreForYear()
    {
        final Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        date.add(Calendar.MONTH, -1);
        final List<RacingEvent> racingEvents = racingEventService.findAllVisibleFromYear(date.get(Calendar.YEAR));
        final WinnerData winnerData = new WinnerData(date.get(Calendar.YEAR) + "");
        winnerData.setScoresTitle(messageUtils.getText("summary.score.year"));
        fillWinnerData(winnerData, racingEvents);

        return winnerData;
    }
    @RequestMapping(value = "/monthScore.json", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    WinnerData getScoreForYearAndMonth(final Integer year, final Integer month)
    {
        final String msg = "month." + month;
        final List<RacingEvent> racingEvents = racingEventService.findAllVisibleFromYearAndMonth(year, month);
        final WinnerData winnerData = new WinnerData(String.valueOf(month));
        winnerData.setScoresTitle(messageUtils.getText("summary.score.month"));
        fillWinnerData(winnerData, racingEvents);

        return winnerData;
    }

    @RequestMapping(value = "/lastEventScore.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody WinnerData getScoreForLastEvent()
    {
        final RacingEvent racingEvent = racingEventService.findLastVisibleFromCurrentYear();
        final WinnerData winnerData = new WinnerData("");//racingEvent.getTimestamp().toString());
        winnerData.setScoresTitle(messageUtils.getText("summary.score.last-event"));
        fillWinnerData(winnerData, new ArrayList<RacingEvent>(){
            {
                add(racingEvent);
            }
        });

        return winnerData;
    }

}
