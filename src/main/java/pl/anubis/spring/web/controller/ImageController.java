package pl.anubis.spring.web.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.anubis.spring.data.repository.track.TrackRepository;
import pl.anubis.spring.model.Track;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Anubis on 21.04.14.
 */
@Controller
@RequestMapping("/image")
public class ImageController
{
    @Autowired
    private TrackRepository trackRepository;

    @RequestMapping(value = "/image/track/{id}", method = RequestMethod.GET)
    public String showImage(@PathVariable("id") Integer id, final HttpServletResponse response, final Model model)
    {
        final Track track = trackRepository.findOne(id);
        if (track != null && track.getImage() != null)
        {
            try
            {
                OutputStream out = response.getOutputStream();
                response.setContentType(track.getContentType());
                IOUtils.copy(new ByteArrayInputStream(track.getImage()), out);
                out.flush();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return null;
    }

}
