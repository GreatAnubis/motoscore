package pl.anubis.spring.web.controller.services.json;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.anubis.spring.data.repository.track.TrackNotFoundException;
import pl.anubis.spring.data.repository.track.TrackService;
import pl.anubis.spring.model.Track;
import pl.anubis.spring.mvc.container.json.TrackInfo;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Anubis on 2014-09-30.
 */
@Controller
@RequestMapping("/admin/tracklist")
public class AdminTrackListController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminPlayerListController.class);

    @Autowired
    private TrackService repositoryTrackService;

    @Autowired
    private ReloadableResourceBundleMessageSource messageSource;


    @RequestMapping("/list.json")
    public @ResponseBody
    List<TrackInfo> list(final Model model)
    {
        final List<Track> tracks = repositoryTrackService.findAll();
        List<TrackInfo> trackList  = new ArrayList<TrackInfo>();
        for(Track track : tracks)
        {
            trackList.add(new TrackInfo(track));
        }
        return trackList;
    }

    @RequestMapping(value = "/add", method = POST, produces = "application/json")
    public @ResponseBody TrackInfo add(final HttpSession session, @RequestBody @Valid final TrackInfo trackForm,final HttpServletResponse response, final BindingResult result, Locale locale) throws Exception
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, errorMessage);
            return trackForm;
        }
        Track track = repositoryTrackService.findByName(trackForm.getTrack_name());
        if (track != null)
        {
            LOGGER.debug(trackForm.getTrack_name() + ": Exists");
            response.sendError(HttpServletResponse.SC_FOUND, trackForm.getTrack_name() + " Exists");
            //response.setStatus(HttpServletResponse.SC_FOUND);
            return trackForm;
        }
        LOGGER.debug("Creating track: " + trackForm.getTrack_name());
        track = repositoryTrackService.create(trackForm);
        final String strResult = track != null ? "OK" : "Unknown error";
        LOGGER.debug("Creating track: " + strResult);
        response.setStatus(HttpServletResponse.SC_CREATED);

        return new TrackInfo(track);
    }

    @RequestMapping(value = "/edit", method = POST, produces = "application/json")
    public
    @ResponseBody
    TrackInfo edit(final HttpSession session, @RequestBody @Valid final TrackInfo trackForm,
                    final HttpServletResponse response, final BindingResult result, Locale locale) throws Exception
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, errorMessage);
            return trackForm;
        }
        Track track = repositoryTrackService.findByName(trackForm.getTrack_name());
        Track trackbyId = repositoryTrackService.findById(trackForm.getId());
        if (track != null && trackbyId != null)
        {
            if(trackbyId.getId() != track.getId())
            {
                LOGGER.debug(trackForm.getTrack_name() + ": Exists");
                response.sendError(HttpServletResponse.SC_FOUND, trackForm.getTrack_name() + ": Exists");
                return trackForm;
            }
        }
        LOGGER.debug("Updating track: " + trackForm.getTrack_name());
        track = repositoryTrackService.update(trackForm);
        final String strResult = track != null ? "OK" : "Unknown error";
        LOGGER.debug("Updating track result: " + strResult);
        response.setStatus(HttpServletResponse.SC_ACCEPTED);
        return new TrackInfo(track);
    }

    @RequestMapping(value = "/delete", method = DELETE, produces = "application/json")
    public
    @ResponseBody Integer delete(final HttpSession session, @RequestParam(value = "track_id", required = true) Integer id,final HttpServletResponse response) throws Exception
    {
        try
        {
            LOGGER.debug("Deleting track (" + id + ")");
            Track track = repositoryTrackService.delete(id);
            LOGGER.debug("Deleted track (" + id + "): " + track.getTrack_name());
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
        }
        catch (final TrackNotFoundException ex)
        {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return id;
        }
        return id;
    }

}
