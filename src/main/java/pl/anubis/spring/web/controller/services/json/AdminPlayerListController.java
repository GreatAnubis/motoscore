package pl.anubis.spring.web.controller.services.json;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.anubis.spring.data.repository.player.PlayerNotFoundException;
import pl.anubis.spring.data.repository.player.PlayerService;
import pl.anubis.spring.model.Player;
import pl.anubis.spring.mvc.container.json.PlayerData;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Anubis on 2014-09-19.
 */
@Controller
@RequestMapping("/admin/playerlist")
public class AdminPlayerListController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminPlayerListController.class);

    @Autowired
    private PlayerService repositoryPlayerService;

    @Autowired
    private ReloadableResourceBundleMessageSource messageSource;


    @RequestMapping("/list.json")
    public @ResponseBody List<PlayerData> list(final Model model)
    {
        final List<Player> players = repositoryPlayerService.findAll();
        List<PlayerData> playerList  = new ArrayList<PlayerData>();
        for(Player player : players)
        {
            playerList.add(new PlayerData(player));
        }
        return playerList;
    }

    @RequestMapping(value = "/add", method = POST, produces = "application/json")
    public @ResponseBody PlayerData add(final HttpSession session, @RequestBody @Valid final PlayerData playerForm,final HttpServletResponse response, final BindingResult result, Locale locale) throws Exception
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, errorMessage);
            return playerForm;
        }
        Player player = repositoryPlayerService.findByNick(playerForm.getPlayer_nick());
        if (player != null)
        {
            LOGGER.debug(playerForm.getPlayer_nick() + ": Exists");
            response.sendError(HttpServletResponse.SC_FOUND, playerForm.getPlayer_nick() + " Exists");
            //response.setStatus(HttpServletResponse.SC_FOUND);
            return playerForm;
        }
        LOGGER.debug("Creating player: " + playerForm.getPlayer_nick());
        player = repositoryPlayerService.create(playerForm.getPlayer_nick());
        final String strResult = player != null ? "OK" : "Unknown error";
        LOGGER.debug("Creating player: " + strResult);
        response.setStatus(HttpServletResponse.SC_CREATED);

        return new PlayerData(player);
    }

    @RequestMapping(value = "/edit", method = POST, produces = "application/json")
    public
    @ResponseBody
    PlayerData edit(final HttpSession session, @RequestBody @Valid final PlayerData playerForm,
                    final HttpServletResponse response, final BindingResult result, Locale locale) throws Exception
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, errorMessage);
            return playerForm;
        }
        Player player = repositoryPlayerService.findByNick(playerForm.getPlayer_nick());
        if (player != null)
        {
            LOGGER.debug(playerForm.getPlayer_nick() + ": Exists");
            response.sendError(HttpServletResponse.SC_FOUND,playerForm.getPlayer_nick() + ": Exists");
            return playerForm;
        }
        LOGGER.debug("Updating player: " + playerForm.getPlayer_nick());
        player = repositoryPlayerService.update(playerForm);
        final String strResult = player != null ? "OK" : "Unknown error";
        LOGGER.debug("Updating player result: " + strResult);
        response.setStatus(HttpServletResponse.SC_ACCEPTED);
        return new PlayerData(player);
    }

    @RequestMapping(value = "/delete", method = DELETE, produces = "application/json")
    public
    @ResponseBody Integer delete(final HttpSession session, @RequestParam(value = "player_id", required = true) Integer id,final HttpServletResponse response) throws Exception
    {
        try
        {
            LOGGER.debug("Deleting player (" + id + ")");
            Player player = repositoryPlayerService.delete(id);
            LOGGER.debug("Deleted player (" + id + "): " + player.getPlayer_nick());
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
        }
        catch (final PlayerNotFoundException ex)
        {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return id;
        }
        return id;
    }

}
