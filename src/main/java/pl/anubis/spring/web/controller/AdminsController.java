package pl.anubis.spring.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Anubis on 14.06.14.
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminsController
{

    @Autowired
    private MessageSource messageSource;


    @RequestMapping("/test")
    public String login()
    {
        return "login-form";
    }

    @RequestMapping("/")
    public String root()
    {
        return "redirect:/index";

    }


}
