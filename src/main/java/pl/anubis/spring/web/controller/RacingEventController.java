package pl.anubis.spring.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import pl.anubis.spring.data.repository.racingevent.RacingEventNotFoundException;
import pl.anubis.spring.data.repository.racingevent.RacingEventService;
import pl.anubis.spring.data.repository.track.TrackService;
import pl.anubis.spring.data.repository.trackinevent.TrackInEventService;
import pl.anubis.spring.model.RacingEvent;
import pl.anubis.spring.model.Track;
import pl.anubis.spring.model.TrackInEvent;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.util.List;
import java.util.Locale;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Anubis on 02.05.14.
 */
@Controller
@RequestMapping("/admin/racingevents")
public class RacingEventController
{
    //http://blog.fawnanddoug.com/2012/05/pagination-with-spring-mvc-spring-data.html
    private static final Logger LOGGER = LoggerFactory.getLogger(RacingEventController.class);

    @Autowired
    private RacingEventService racingEventService;

    @Autowired
    private ReloadableResourceBundleMessageSource messageSource;

    @Autowired
    private TrackService trackService;

    @Autowired
    private TrackInEventService trackInEventService;

    public List<Track> getAvailableTracks()
    {
        return availableTracks;
    }

    public void setAvailableTracks(List<Track> availableTracks)
    {
        this.availableTracks = availableTracks;
    }

    private List<Track> availableTracks;

    @ModelAttribute("availableTracks")
    public List<Track> prepareTrackListModel()
    {
        LOGGER.debug("new RacingEvent()");
        return trackService.findAll();
    }

    @ModelAttribute("trackInEvent")
    public TrackInEvent getTrackInEvent()
    {
        return new TrackInEvent();
    }

    @ModelAttribute("racingevent")
    public RacingEvent getRacingevent()
    {
        return new RacingEvent();
    }

    @RequestMapping("/")
    public String root()
    {
        return "redirect:/admin/racingevents/list";
    }

    @RequestMapping("")
    public String rootEmpty()
    {
        return "redirect:/admin/racingevents/list";
    }

    @RequestMapping("/list")
    public String list(final Model model)
    {
        final List<RacingEvent> racingevents = racingEventService.findAll();
        model.addAttribute("racingevents", racingevents);
        return "racingevents/list";
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public String prepareEditRacingEvent(@PathVariable("id") Integer racingEventId, Model model) throws RacingEventNotFoundException
    {
        availableTracks = trackService.findAll();
        RacingEvent racingEvent = racingEventService.findById(racingEventId);
        if (racingEvent == null)
        {
            throw new RacingEventNotFoundException("Not found!");
        }
        model.addAttribute("racingevent", racingEvent);
        return "racingevents/edit";
    }

    @RequestMapping(value = "/{id}/updateall", method = POST)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public String editRacingEvent(final HttpSession session, @PathVariable("id") Integer racingEventId, @Valid final RacingEvent racingEventForm, Model model, final BindingResult result, Locale locale) throws Exception
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            racingEventForm.setId(racingEventId);
            model.addAttribute("racingevent", racingEventForm);
            return "racingevents/edit";
        }

        racingEventForm.setId(racingEventId);
        LOGGER.debug("Updating racingEvent: " + racingEventForm.getTimestamp());
        RacingEvent racingEvent = racingEventService.updateAll(racingEventForm);
        final String strResult = racingEvent != null ? "OK" : "Unknown error";
        LOGGER.debug("Updating racingEvent: " + strResult);
        return "redirect:edit";
    }

    @RequestMapping(value = "/{id}/reorder", method = POST, produces = "text/plain; charset=utf-8")
    @ResponseStatus(HttpStatus.CREATED)
    public
    @ResponseBody
    String updateOrdering(final HttpSession session, @PathVariable("id") Integer racingEventId, @RequestParam(value = "id") String id, @RequestParam(value = "ordering") String ordering, Model model, final BindingResult result, Locale locale) throws Exception
    {
        final String strResult = "";//created != null ? "OK" : "Unknown error";
        LOGGER.debug("Updating ordering TrackInEvent: " + strResult);
        return strResult;
    }

    @RequestMapping(value = "/{racingEventId}/addtrack", method = POST, produces = "text/plain; charset=utf-8")
    @ResponseStatus(HttpStatus.CREATED)
    public
    @ResponseBody
    String addTrackToRacingEvent(final HttpSession session, @PathVariable("racingEventId") Integer racingEventId, @Valid final TrackInEvent trackInEvent, Model model, final BindingResult result, Locale locale) throws Exception
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            return errorMessage;
        }
        TrackInEvent created = trackInEventService.create(racingEventId, trackInEvent);
        final String strResult = created != null ? "OK" : "Unknown error";
        LOGGER.debug("Adding Track to trackInEvent: " + strResult);
        return strResult;
    }

    @RequestMapping(value = "/{id}/deletetrack", method = POST, produces = "text/plain; charset=utf-8")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    String deleteTrackInEvent(final HttpSession session, @PathVariable("id") Integer racingEventId, @RequestParam(value = "trackInEvent_id", required = true) Integer trackInEvent_id, Locale locale) throws Exception
    {
        TrackInEvent deleted = trackInEventService.delete(racingEventId,trackInEvent_id);
        final String strResult = deleted != null ? "OK" : "Unknown error";
        LOGGER.debug("Delete track in event: " + strResult);
        return strResult;
    }

    @RequestMapping(value = "/add", method = POST, produces = "text/plain; charset=utf-8")
    @ResponseStatus(HttpStatus.CREATED)
    public
    @ResponseBody
    String add(final HttpSession session, @Valid final RacingEvent racingEventForm, final BindingResult result, Locale locale) throws Exception
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            return errorMessage;
        }
        LOGGER.debug("Add racingEvent: " + racingEventForm.getTimestamp());
        //racingEventForm.setId(0);
        RacingEvent racingEvent = racingEventService.create(racingEventForm);
        final String strResult = racingEvent != null ? "OK" : "Unknown error";
        LOGGER.debug("Add racingEvent: " + strResult);
        return strResult;
    }

    @RequestMapping(value = "/delete", method = POST, produces = "text/plain; charset=utf-8")
    public
    @ResponseBody
    String delete(final HttpSession session, @RequestParam(value = "racing_event_id", required = true) Integer id) throws Exception
    {
        try
        {
            LOGGER.debug("Deleting racingEvent (" + id + ")");
            RacingEvent racingEvent = racingEventService.delete(id);
            LOGGER.debug("Deleted racingEvent (" + id + "): " + racingEvent.getTimestamp());
        }
        catch (final RacingEventNotFoundException ex)
        {
            return "NOTEXISTS";
        }
        return "OK";
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder)
    {
        binder.registerCustomEditor(RacingEvent.class, new PropertyEditorSupport()
        {
            @Override
            public void setAsText(String text)
            {
                RacingEvent custom = racingEventService.findById(Integer.parseInt(text));
                setValue(custom);
            }
        });

        binder.registerCustomEditor(Track.class, new PropertyEditorSupport()
        {
            @Override
            public void setAsText(String text)
            {
                Track custom = trackService.findById(Integer.parseInt(text));
                setValue(custom);
            }
        });
        binder.registerCustomEditor(TrackInEvent.class, new PropertyEditorSupport()
        {
            @Override
            public void setAsText(String text)
            {
                TrackInEvent custom = trackInEventService.findById(Integer.parseInt(text));
                setValue(custom);
            }
        });
        /*binder.registerCustomEditor(Track.class, "track", new CustomCollectionEditor(List.class)
        {
            @Override
            protected Object convertElement(Object element)
            {
                Integer id = null;

                if(element instanceof String && !((String)element).equals("")){
                    //From the JSP 'element' will be a String
                    try{
                        id = Integer.parseInt((String) element);
                    }
                    catch (NumberFormatException e) {
                        System.out.println("Element was " + ((String) element));
                        e.printStackTrace();
                    }
                }
                else if(element instanceof Long) {
                    //From the database 'element' will be a Long
                    id = (Integer) element;
                }

                return id != null ? trackService.findById(id) : null;
            }
        });*/
    }
}
