package pl.anubis.spring.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import pl.anubis.spring.data.repository.track.TrackNotFoundException;
import pl.anubis.spring.data.repository.track.TrackService;
import pl.anubis.spring.model.Track;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Anubis on 21.04.14.
 */
@Controller
@RequestMapping("/admin/tracks")
public class TracksController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TracksController.class);

    @Autowired
    private TrackService trackService;
    @Autowired
    private ReloadableResourceBundleMessageSource messageSource;

    @RequestMapping(value = "/")
    public String root()
    {
        return "redirect:/admin/tracks/list";
    }

    @RequestMapping(value = "")
    public String rootEmpty()
    {
        return "redirect:/admin/tracks/list";
    }

    @ModelAttribute("track")
    public Track prepareTrackModel()
    {
        LOGGER.debug("new TrackDTO()");
        return new Track();
    }

    @RequestMapping("/list")
    public String list(final Model model)
    {
        final List<Track> tracks = trackService.findAll();
        model.addAttribute("tracks", tracks);

        return "tracks/list";
    }


    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws ServletException
    {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    }

    @RequestMapping(value = "/add", method = POST, produces = "text/plain; charset=utf-8")
    @ResponseStatus(HttpStatus.CREATED)
    public
    @ResponseBody
    String add(final HttpSession session, @Valid final Track trackForm, final BindingResult result, Locale locale) throws Exception
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            return errorMessage;
        }
        Track track = trackService.findByName(trackForm.getTrack_name());
        if (track != null)
        {
            LOGGER.debug(trackForm.getTrack_name() + ": Exists");
            return "EXISTS";
        }
        LOGGER.debug("Creating track: " + trackForm.getTrack_name());
        track = trackService.create(trackForm);
        final String strResult = track != null ? "OK" : "Unknown error";
        LOGGER.debug("Creating track: " + strResult);
        return strResult;
    }

    @RequestMapping(value = "/edit", method = POST, produces = "text/plain; charset=utf-8")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    String edit(final HttpSession session, @Valid final Track trackForm, final BindingResult result, Locale locale) throws Exception
    {
        if (result.hasFieldErrors())
        {
            final String field = result.getFieldError().getField();
            final String field_code = result.getFieldError().getCode();
            LOGGER.debug("Error: " + field_code + "." + field);
            final String errorMessage = messageSource.getMessage(field_code + "." + field, result.getFieldError().getArguments(), locale);
            return errorMessage;
        }
        Track track = trackService.findByName(trackForm.getTrack_name());
        if (track != null)
        {
            LOGGER.debug(trackForm.getTrack_name() + ": Exists");
            return "EXISTS";
        }
        LOGGER.debug("Updating track: " + trackForm.getTrack_name());
        track = trackService.update(trackForm);
        final String strResult = track != null ? "OK" : "Unknown error";
        LOGGER.debug("Updating track: " + strResult);
        return strResult;
    }

    @RequestMapping(value = "/delete", method = POST, produces = "text/plain; charset=utf-8")
    public
    @ResponseBody
    String delete(final HttpSession session, @RequestParam(value = "track_id", required = true) Integer id) throws Exception
    {
        try
        {
            LOGGER.debug("Deleting track (" + id + ")");
            Track track = trackService.delete(id);
            LOGGER.debug("Deleted track (" + id + "): " + track.getTrack_name());
        }
        catch (final TrackNotFoundException ex)
        {
            return "NOTEXISTS";
        }
        return "OK";
    }

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Track track, BindingResult bindingResult, Model uiModel, @RequestParam("image") MultipartFile multipartFile, HttpServletRequest httpServletRequest)
    {
        if (bindingResult.hasErrors())
        {
            //populateEditForm(uiModel, logItem);
            return "tracks/create";
        }
        uiModel.asMap().clear();
        track.setContentType(multipartFile.getContentType());
        //logItem.persist();
        return "redirect:/admin/tracks/" + track.getId().toString();
    }
}
