package pl.anubis.spring.web.controller.services.json;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.anubis.spring.config.CustomStrings;
import pl.anubis.spring.data.repository.racingevent.RacingEventService;
import pl.anubis.spring.model.PlayerScoreInRun;
import pl.anubis.spring.model.RacingEvent;
import pl.anubis.spring.model.TrackInEvent;
import pl.anubis.spring.model.Vehicle;
import pl.anubis.spring.mvc.container.TableRacingEventRow;
import pl.anubis.spring.mvc.container.json.PlayerData;
import pl.anubis.spring.mvc.container.json.RacingEventInfo;
import pl.anubis.spring.mvc.container.json.TrackInfo;
import pl.anubis.spring.mvc.container.json.VehicleInfo;
import pl.anubis.spring.security.Sorters;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Anubis on 2014-07-14.
 */
@Controller
@RequestMapping(value = "/events")
public class EventScoresDataController
{
    @Autowired
    private RacingEventService racingEventService;

    @RequestMapping(value = "/racingevents.json", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<RacingEventInfo> indexJSONWithSorting(@RequestParam(value = "year", required = true) String stryear, @RequestParam(value = "month", required = false) String strmonth)
    {
        final List<RacingEventInfo> racingEventsInfo = new ArrayList<RacingEventInfo>();

        Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        Integer year = 0;
        Integer month = null;

        if (stryear == null || stryear.length() == 0)
        {
            year = date.get(Calendar.YEAR);
        }
        if (strmonth == null || strmonth.length() == 0)
        {
            strmonth = "all";
        }
        if (stryear != null)
        {
            try
            {
                year = Integer.parseInt(stryear);
            }
            catch (NumberFormatException ex)
            {
                year = date.get(Calendar.YEAR);
            }
        }
        if (strmonth != null && strmonth.length() != 0)
        {
            if (strmonth.compareToIgnoreCase("all") == 0)
            {
                month = null;
            }
            else
            {
                try
                {
                    month = Integer.parseInt(strmonth);
                }
                catch (NumberFormatException ex)
                {
                    month = date.get(Calendar.MONTH) + 1;
                }
            }
        }
        final List<RacingEvent> racingevents = racingEventService.findAllVisibleFromYearAndMonth(year, month);

        for (RacingEvent racingEvent : racingevents)
        {
            final Map<Integer, TableRacingEventRow> listRacingEvents = new HashMap<Integer, TableRacingEventRow>();
            final RacingEventInfo racingEventInfo = new RacingEventInfo();


            long timestamp = racingEvent.getTimestamp().getTime();
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(timestamp);
            racingEventInfo.setId(racingEvent.getId());
            racingEventInfo.setYear(cal.get(Calendar.YEAR));
            racingEventInfo.setMonth(cal.get(Calendar.MONTH)+1);
            racingEventInfo.setMovieLink(racingEvent.getMovieLink());
            racingEventInfo.setTimeStamp(new SimpleDateFormat(CustomStrings.DEFAULT_DATE_PATTERN).format(racingEvent.getTimestamp()));
            final List<TrackInEvent> tracks = racingEvent.getTracks();
            for (final TrackInEvent trackInEvent : tracks)
            {
                racingEventInfo.addTrack (new TrackInfo(trackInEvent));
                final Map<Integer, Set<Integer>> vehiclesForTrackInEvent = racingEventInfo.getVehiclePresenceInRun();
                if(!vehiclesForTrackInEvent.containsKey(trackInEvent.getId()))
                {
                    //final Set<VehicleInfo> vehicleInfo = new TreeSet<VehicleInfo>();
                    final Set<Integer> vehicleInfo = new TreeSet<Integer>();
                    if (trackInEvent.getVehiclesInRun() != null)
                    {
                        for (Vehicle veh : trackInEvent.getVehiclesInRun())
                        {
                            if(!racingEventInfo.getVehicles().containsKey(veh.getId()))
                            {
                                racingEventInfo.addVehicle(new VehicleInfo(veh));
                            }
                            vehicleInfo.add(veh.getId());
                        }
                    }
                    if(vehicleInfo.size() != 0)
                    {
                        vehiclesForTrackInEvent.put(trackInEvent.getId(), vehicleInfo);
                    }
                }

                for (final PlayerScoreInRun playerScoreInRun : trackInEvent.getPlayerScoresInRun())
                {
                    if (playerScoreInRun.getTrackInEvent().getId() == trackInEvent.getId())
                    {
                        if (!listRacingEvents.containsKey(playerScoreInRun.getPlayer().getId()))
                        {
                            racingEventInfo.addPlayer(new PlayerData(playerScoreInRun.getPlayer()));
                            final TableRacingEventRow tableRacingEventRow = new TableRacingEventRow();
                            tableRacingEventRow.setPlayer(playerScoreInRun.getPlayer());
                            tableRacingEventRow.setTotalScore(playerScoreInRun.getScore());
                            tableRacingEventRow.getPlayerScores().put(trackInEvent.getId(), playerScoreInRun.getScore());
                            listRacingEvents.put(playerScoreInRun.getPlayer().getId(), tableRacingEventRow);
                        }
                        else
                        {
                            final TableRacingEventRow tableRacingEventRow = listRacingEvents.get(playerScoreInRun.getPlayer().getId());
                            tableRacingEventRow.setTotalScore(tableRacingEventRow.getTotalScore() + playerScoreInRun.getScore());
                            tableRacingEventRow.getPlayerScores().put(trackInEvent.getId(), playerScoreInRun.getScore());
                            listRacingEvents.put(playerScoreInRun.getPlayer().getId(), tableRacingEventRow);
                        }
                    }
                }
                final List<TableRacingEventRow> playersRow = new ArrayList<TableRacingEventRow>();
                int TableRacingRowId = 0;
                for (Integer player : listRacingEvents.keySet())
                {
                    TableRacingEventRow row = listRacingEvents.get(player);
                    row.setId(TableRacingRowId++);
                    playersRow.add(row);

                }

                racingEventInfo.setTableRacingEventRows(Sorters.getSorters().sortByTotalScore(playersRow));

            }

            racingEventsInfo.add(racingEventInfo);

        }
        return racingEventsInfo;
    }
}
