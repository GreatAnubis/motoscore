package pl.anubis.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.site.SitePreference;
import org.springframework.ui.Model;

// @Controller
public class StartupController
{

    private static final Logger logger = LoggerFactory.getLogger(StartupController.class);

    // @RequestMapping(value = "/www", method = RequestMethod.GET)
    // @ResponseBody
    public String showIndex()
    {
        return "Hello world";
    }

    // @RequestMapping("/indexxxxx")
    public String home(final Device device, final SitePreference sitePreference, final Model model)
    {
        if (device.isMobile())
        {
            logger.info("Hello mobile user!");
        }
        else if (device.isTablet())
        {
            logger.info("Hello tablet user!");
        }
        else
        {
            logger.info("Hello desktop user!");
        }

        if (sitePreference == SitePreference.NORMAL)
        {
            logger.info("Site preference is normal");
            return "home";
        }
        else if (sitePreference == SitePreference.MOBILE)
        {
            logger.info("Site preference is mobile");
            return "home-mobile";
        }
        else if (sitePreference == SitePreference.TABLET)
        {
            logger.info("Site preference is tablet");
            return "home-tablet";
        }
        else
        {
            logger.info("no site preference");
            return "home";
        }
    }

}
